<% if $ShowPurchaseAccountBalance %>
	<p><%t EWalletWidget.ss.PURCHASE_BALANCE "E-Purchase Balance: {balance}" balance=$CurrentMember.PurchaseAccountBalance.Nice %></p>
<% end_if %>
<% if $ShowCashAccountBalance %>
	<p><%t EWalletWidget.ss.CASH_BALANCE "E-Cash Balance: {balance}" balance=$CurrentMember.CashAccountBalance.Nice %></p>
<% end_if %>
<% if $ShowBonusAccountBalance %>
	<p><%t EWalletWidget.ss.BONUS_BALANCE "E-Bonus Balance: {balance}" balance=$CurrentMember.BonusAccountBalance.Nice %></p>
<% end_if %>
<% if $ShowUpgradeAccountBalance %>
	<p><%t EWalletWidget.ss.UPGRADE_BALANCE "E-Upgrade Balance: {balance}" balance=$CurrentMember.UpgradeAccountBalance.Nice %></p>
<% end_if %>
<% if $ShowProductAccountBalance %>
	<p><%t EWalletWidget.ss.PRODUCT_BALANCE "E-Product Balance: {balance}" balance=$CurrentMember.ProductAccountBalance.Nice %></p>
<% end_if %>