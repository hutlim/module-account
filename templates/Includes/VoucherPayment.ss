<tr>
    <td rowspan="2" class="payment-method" data-title="Payment Method" valign="top">
        $PaymentMethod ($Status.Title)
    </td>
    <td class="payment-detail">
        <%t VoucherPayment.ss.PAID_AMOUNT "Paid Amount: {currency}{amount}" currency=$Currency amount=$TotalAmount %><br />
    </td>
</tr>
<tr>
    <td class="payment-detail">
    	<%t VoucherPayment.ss.VOUCHER_CODE "Voucher Code: {code}" code=$Voucher.Code %>
    </td>
</tr>
<% if $Message %>
<tr>
    <td class="payment-detail" colspan="2">$Message</td>
</tr>
<% end_if %>