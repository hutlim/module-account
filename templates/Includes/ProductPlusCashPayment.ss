<tr>
    <td rowspan="2" class="payment-method" data-title="Payment Method" valign="top">
        $PaymentMethod ($Status.Title)
    </td>
    <td class="payment-detail">
    	<%t ProductPlusCashPayment.ss.PRODUCT_PAID_AMOUNT "E-Product Paid Amount: {currency}{amount}" currency=$Currency amount=$ProductAmount %><br />
        <%t ProductPlusCashPayment.ss.CASH_PAID_AMOUNT "E-Cash Paid Amount: {currency}{amount}" currency=$Currency amount=$CashAmount %>
    </td>
</tr>
<tr>
    <td class="payment-detail">
    	<%t ProductPlusCashPayment.ss.PRODUCT_BEFORE_BALANCE "E-Product Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeProductBalance %><br />
    	<%t ProductPlusCashPayment.ss.PRODUCT_AFTER_BALANCE "E-Product After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterProductBalance %><br />
    	<%t ProductPlusCashPayment.ss.CASH_BEFORE_BALANCE "E-Cash Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeCashBalance %><br />
    	<%t ProductPlusCashPayment.ss.CASH_AFTER_BALANCE "E-Cash After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterCashBalance %>
    </td>
</tr>
<% if $Message %>
<tr>
    <td class="payment-detail" colspan="2">$Message</td>
</tr>
<% end_if %>