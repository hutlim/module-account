<tr>
    <td rowspan="2" class="payment-method" data-title="Payment Method" valign="top">
        $PaymentMethod ($Status.Title)
    </td>
    <td class="payment-detail">
        <%t PurchasePlusBonusPayment.ss.BONUS_PAID_AMOUNT "E-Bonus Paid Amount: {currency}{amount}" currency=$Currency amount=$BonusAmount %><br />
        <%t PurchasePlusBonusPayment.ss.PURCHASE_PAID_AMOUNT "E-Purchase Paid Amount: {currency}{amount}" currency=$Currency amount=$PurchaseAmount %>
    </td>
</tr>
<tr>
    <td class="payment-detail">
    	<%t PurchasePlusBonusPayment.ss.BONUS_BEFORE_BALANCE "E-Bonus Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeBonusBalance %><br />
    	<%t PurchasePlusBonusPayment.ss.BONUS_AFTER_BALANCE "E-Bonus After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterBonusBalance %><br />
    	<%t PurchasePlusBonusPayment.ss.PURCHASE_BEFORE_BALANCE "E-Purchase Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforePurchaseBalance %><br />
    	<%t PurchasePlusBonusPayment.ss.PURCHASE_AFTER_BALANCE "E-Purchase After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterPurchaseBalance %>
    </td>
</tr>
<% if $Message %>
<tr>
    <td class="payment-detail" colspan="2">$Message</td>
</tr>
<% end_if %>