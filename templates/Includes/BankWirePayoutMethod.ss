<% if $Format == 'PDF' %>
	<link rel="stylesheet" type="text/css" href="account/css/PayoutDetail.css" />
<% else %>
	<link rel="stylesheet" type="text/css" href="/account/css/PayoutDetail.css" />
<% end_if %>
<table id="payout-details">
    <tr>
        <th class="method"><%t BankWirePayoutMethod.ss.METHOD "Method" %></th>
        <th class="detail"><%t BankWirePayoutMethod.ss.DETAILS "Details" %></th>
    </tr>
    <tr>
        <td rowspan="2" class="method" data-title="Payment Method">
            $Title
        </td>
        <td class="detail">
            <%t BankWirePayoutMethod.ss.REFERENCE "Reference: {reference}" reference=$Reference %><br />
            <% if $ProcessingFee %>
            	<%t BankWirePayoutMethod.ss.PROCESSING_FEE "Processing Fee: {processing_fee}" processing_fee=$ProcessingFee.Nice %>
            <% end_if %>
        </td>
    </tr>
    <tr>
        <td class="detail">
        	<%t BankWirePayoutMethod.ss.ACCOUNT_NAME "Account Name: {account_name}" account_name=$AccountName %><br />
            <%t BankWirePayoutMethod.ss.ACCOUNT_NUMBER "Account Number: {account_number}" account_number=$AccountNumber %><br />
            <% if $OwnerContact %>
            	<%t BankWirePayoutMethod.ss.OWNER_CONTACT "Owner Contact: {owner_contact}" owner_contact=$OwnerContact %><br />
            <% end_if %>
            <% if $OwnerEmail %>
            	<%t BankWirePayoutMethod.ss.OWNER_EMAIL "Owner Email: {owner_email}" owner_email=$OwnerEmail %><br />
            <% end_if %>
            <strong><u><%t BankWirePayoutMethod.ss.BANK_DETAILS "Bank Details" %></u></strong><br />
            <%t BankWirePayoutMethod.ss.BANK_NAME "Bank Name: {bank_name}" bank_name=$BankNameByLang %><br />
            <% if $BranchName %>
            	<%t BankWirePayoutMethod.ss.BRANCH_NAME "Branch Name: {branch_name}" branch_name=$BranchName %><br />
            <% end_if %>
            <% if $SwiftCode %>
            	<%t BankWirePayoutMethod.ss.SWIFT_CODE "Swift Code / ABA: {swift_code}" swift_code=$SwiftCode %><br />
            <% end_if %>
            <% if $Currency %>
            	<%t BankWirePayoutMethod.ss.CURRENCY "Currency: {currency} ({rate})" currency=$Currency rate=$CurrencyRate.Nice %><br />
            <% end_if %>
            <% if $Address %>
            	<%t BankWirePayoutMethod.ss.ADDRESS "Address: {address}" address=$Address %><br />
            <% end_if %>
            <% if $Suburb %>
            	<%t BankWirePayoutMethod.ss.SUBURB "City / Town: {suburb}" suburb=$Suburb %><br />
            <% end_if %>
            <% if $State %>
            	<%t BankWirePayoutMethod.ss.STATE "State / Province: {state}" state=$State %><br />
            <% end_if %>
            <% if $Postal %>
            	<%t BankWirePayoutMethod.ss.POSTAL "Zip / Postal Code: {postal}" postal=$Postal %><br />
            <% end_if %>
            <% if $CountryName %>
            	<%t BankWirePayoutMethod.ss.COUNTRY "Country: {country}" country=$CountryName %><br />
            <% end_if %>
        </td>
    </tr>
    <% if $Message %>
    <tr>
        <td class="payment-detail">$Message</td>
    </tr>
    <% end_if %>
</table>