<% if $Format == 'PDF' %>
	<link rel="stylesheet" type="text/css" href="account/css/WithdrawStatementTemplate.css" />
<% else %>
	<link rel="stylesheet" type="text/css" href="/account/css/WithdrawStatementTemplate.css" />
<% end_if %>
<div id="withdraw-statement">
    <div id="header"><%t WithdrawStatementTemplate.ss.WITHDRAWAL_STATEMENT "{type} Withdrawal Statement" type=$Type %></div>
    <div id="identity">
        <div id="address">
            <p><strong>$SiteConfig.CompanyName</strong></p>
            <p>$SiteConfig.FullAddress</p>
            <% if $SiteConfig.CompanySupportEmail %>
            <p>$SiteConfig.CompanySupportEmail</p>
            <% end_if %>
            <% if $SiteConfig.CompanyPhone %>
            <p>$SiteConfig.CompanyPhone</p>
            <% end_if %>
        </div>
        <div id="logo">
            <% if $SiteConfig.CompanyLogo %>
                <img src="$SiteConfig.CompanyLogo.SetHeight(100).URL" alt="logo" />
            <% end_if %>
        </div>
    </div>
    <div style="clear:both"></div>
    <br />
    <div id="customer">
        <div id="customer-title">
            <p><strong><u><%t WithdrawStatementTemplate.ss.PAID_TO "Paid To" %></u></strong></p>
            <p>$Member.Name ($Member.Username)</p>
            <p>$Member.FullAddress</p>
            <% if $Member.Email %>
                <p>$Member.Email</p>
            <% end_if %>
            <% if $Member.Mobile %>
                <p>$Member.Mobile</p>
            <% end_if %>
        </div>
        <div id="meta">
	        <table>
	            <tr>
	                <td class="meta-head"><%t WithdrawStatementTemplate.ss.STATUS "Status" %></td>
	                <td>$Status.Title</td>
	            </tr>
	            <tr>
	                <td class="meta-head"><%t WithdrawStatementTemplate.ss.SUBMISSION_DATE "Submission Date" %></td>
	                <td>$Created.Nice</td>
	            </tr>
	            <tr>
	                <td class="meta-head"><%t WithdrawStatementTemplate.ss.RECEIVABLE_AMOUNT "Receivable Amount" %></td>
	                <td>$PayoutMethod.Amount.Nice</td>
	            </tr>
	            <tr>
	                <td class="meta-head"><%t WithdrawStatementTemplate.ss.PRINTED_ON "Printed on" %></td>
	                <td>$PrintTime</td>
	            </tr>
	        </table>
        </div>
    </div>
    <div style="clear:both"></div>
    <% if $Remark %>
    <div id="remark">
        <p><%t WithdrawStatementTemplate.ss.NOTE "Note: {remark}" remark=$Remark %></p>
    </div>
    <div style="clear:both"></div>
    <% end_if %>
    <br />
    $PayoutMethod.FullDetailHTML
    <div id="terms">
        <p><%t WithdrawStatementTemplate.ss.TERM "This is a computer generated statement, no company stamp or signature is required. We strongly encourages our customers to print this invoice out only when you need it. Save Trees, Save Earth." %></p>
    </div>
</div>