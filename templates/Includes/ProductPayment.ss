<tr>
    <td rowspan="2" class="payment-method" data-title="Payment Method" valign="top">
        $PaymentMethod ($Status.Title)
    </td>
    <td class="payment-detail">
        <%t ProductPayment.ss.PAID_AMOUNT "Paid Amount: {currency}{amount}" currency=$Currency amount=$TotalAmount %><br />
    </td>
</tr>
<tr>
    <td class="payment-detail">
    	<%t ProductPayment.ss.BEFORE_BALANCE "Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeBalance %><br />
    	<%t ProductPayment.ss.AFTER_BALANCE "After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterBalance %>
    </td>
</tr>
<% if $Message %>
<tr>
    <td class="payment-detail" colspan="2">$Message</td>
</tr>
<% end_if %>