<p><strong><%t DashboardLatestPendingDepositSubmissionPanel.ss.TOTAL_PENDING_DEPOSIT_SUBMISSION "Total {total} pending deposit submission" total=$TotalPendingDepositSubmission %></strong></p>
<div class="dashboard-latest-member">
    <ul>
        <% if PendingDepositSubmissions %>
            <% loop PendingDepositSubmissions %>
                <li><a href="$EditLink" class="cms-panel-link" target="_blank">$Title</a></li>
            <% end_loop %>
        <% else %>
            <li><i><%t DashboardLatestPendingDepositSubmissionPanel.ss.NO_PENDING_DEPOSIT_SUBMISSION_FOUND "Currently has no pending deposit submission found" %></i></li>
        <% end_if %>
    </ul>
</div>