<tr>
    <td rowspan="2" class="payment-method" data-title="Payment Method" valign="top">
        $PaymentMethod ($Status.Title)
    </td>
    <td class="payment-detail">
    	<% if $PurchaseAmount > 0 %>
    		<%t EWalletPayment.ss.PURCHASE_PAID_AMOUNT "E-Purchase Paid Amount: {currency}{amount}" currency=$Currency amount=$PurchaseAmount %><br />
    	<% end_if %>
    	<% if $CashAmount > 0 %>
    		<%t EWalletPayment.ss.CASH_PAID_AMOUNT "E-Cash Paid Amount: {currency}{amount}" currency=$Currency amount=$CashAmount %><br />
    	<% end_if %>
    	<% if $BonusAmount > 0 %>
    		<%t EWalletPayment.ss.BONUS_PAID_AMOUNT "E-Bonus Paid Amount: {currency}{amount}" currency=$Currency amount=$BonusAmount %><br />
    	<% end_if %>
    	<% if $UpgradeAmount > 0 %>
    		<%t EWalletPayment.ss.UPGRADE_PAID_AMOUNT "E-Upgrade Paid Amount: {currency}{amount}" currency=$Currency amount=$UpgradeAmount %><br />
    	<% end_if %>
    	<% if $ProductAmount > 0 %>
    		<%t EWalletPayment.ss.PRODUCT_PAID_AMOUNT "E-Product Paid Amount: {currency}{amount}" currency=$Currency amount=$ProductAmount %>
    	<% end_if %>
    </td>
</tr>
<tr>
    <td class="payment-detail">
    	<% if $PurchaseAmount > 0 %>
    		<%t EWalletPayment.ss.PURCHASE_BEFORE_BALANCE "E-Purchase Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforePurchaseBalance %><br />
    		<%t EWalletPayment.ss.PURCHASE_AFTER_BALANCE "E-Purchase After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterPurchaseBalance %><br />
    	<% end_if %>
    	<% if $CashAmount > 0 %>
    		<%t EWalletPayment.ss.CASH_BEFORE_BALANCE "E-Cash Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeCashBalance %><br />
    		<%t EWalletPayment.ss.CASH_AFTER_BALANCE "E-Cash After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterCashBalance %><br />
    	<% end_if %>
    	<% if $BonusAmount > 0 %>
    		<%t EWalletPayment.ss.BONUS_BEFORE_BALANCE "E-Bonus Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeBonusBalance %><br />
    		<%t EWalletPayment.ss.BONUS_AFTER_BALANCE "E-Bonus After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterBonusBalance %><br />
    	<% end_if %>
    	<% if $UpgradeAmount > 0 %>
    		<%t EWalletPayment.ss.UPGRADE_BEFORE_BALANCE "E-Upgrade Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeUpgradeBalance %><br />
    		<%t EWalletPayment.ss.UPGRADE_AFTER_BALANCE "E-Upgrade After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterUpgradeBalance %><br />
    	<% end_if %>
    	<% if $ProductAmount > 0 %>
    		<%t EWalletPayment.ss.PRODUCT_BEFORE_BALANCE "E-Product Before Balance: {currency}{before_balance}" currency=$Currency before_balance=$BeforeProductBalance %><br />
    		<%t EWalletPayment.ss.PRODUCT_AFTER_BALANCE "E-Product After Balance: {currency}{after_balance}" currency=$Currency after_balance=$AfterProductBalance %>
    	<% end_if %>
    </td>
</tr>
<% if $Message %>
<tr>
    <td class="payment-detail" colspan="2">$Message</td>
</tr>
<% end_if %>