<?php

class EVoucher extends Page_Controller {
    private static $allowed_actions = array(
        'process'
    );

    function process() {
		$payment = VoucherPayment::get()->byID($this->request->param('ID'));
		if($payment){
			$token = new SecurityToken(sprintf('TOKEN_%s', $payment->Receipt()->Reference));
	        if($token->checkRequest($this->request)){
	            try {
	            	DB::getConn()->transactionStart();
	                $payment->pendingPayment();
	                if($payment->checkVoucher()){
	                    $payment->completePayment();
						DB::getConn()->transactionEnd();
						return $this->redirect($payment->RedirectLink($this->request->getVar('locale')));
	                }
	                else{
	                    $payment->failurePayment(array('Message' => _t('EVoucher.INVALID_VOUCHER_CODE', 'Invalid voucher code')));
						$this->setMessage('error', _t('EVoucher.INVALID_VOUCHER_CODE', 'Invalid voucher code'));
	                }
	                DB::getConn()->transactionEnd();
					return $this->redirectBack();
	            }
	            catch(ValidationException $e) {
	                DB::getConn()->transactionRollback();
	                SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
					$this->setMessage('error', $e->getResult()->message());
					return $this->redirectBack();
	            }
	        }
        }
        
        return $this->httpError('404');
    }
}
