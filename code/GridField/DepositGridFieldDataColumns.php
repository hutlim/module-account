<?php
/**
 * @package account
 */
class DepositGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('account/javascript/lang');
		Requirements::javascript('account/javascript/DepositAction.min.js');
    	Requirements::css('account/css/DepositGridFieldDataColumns.css');
    	$content = '';
    	if($record->canApprove()){
    		$field = GridField_FormAction::create($gridField, 'Approve' . $record->ID, false, "approve", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-approve-deposit')->setAttribute('title', _t('DepositGridFieldDataColumns.BUTTONAPPROVE', 'Approve'))->setAttribute('data-icon', 'accept')->setDescription(_t('DepositGridFieldDataColumns.APPROVE_DEPOSIT', 'Approve Deposit'));
			$content .= $field->Field();
		}

		return $content;
    }

    public function getActions($gridField) {
        return array(
            'approve'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'approve') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
        	if(!$item->canApprove()) {
                throw new ValidationException(_t('DepositGridFieldDataColumns.APPROVE_PERMISSION', 'No permission to approve these item'), 0);
            }
			
			try {
	            DB::getConn()->transactionStart();
				if($item->Status == 'Fully Paid'){
					DepositSubmission::update_status($item->ID, 'Approved', isset($data['Remark']) ? $data['Remark'] : '');
				}
				else{
					Config::inst()->update('DepositSubmission', 'auto_approve', true);
					foreach($item->PaymentReceipt()->Payments() as $payment){
						if($payment->canApprove()){
							$payment->completePayment();
						}
					};
					
					if(isset($data['Remark']) && $data['Remark']){
						$item->Remark = $data['Remark'];
						$item->write();
					}
				}
	            DB::getConn()->transactionEnd();
	        }
	        catch(ValidationException $e) {
	            DB::getConn()->transactionRollback();
	            throw new ValidationException($e->getMessage(), 0);
	        }
        }
    }
}
