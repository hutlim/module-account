<?php
/**
 * @package account
 */
class VoucherGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::css('account/css/VoucherGridFieldDataColumns.css');
    	$content = '';
    	if($record->canCancel()){
    		$field = GridField_FormAction::create($gridField, 'Cancel' . $record->ID, false, "cancel", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-cancel-voucher')->setAttribute('title', _t('VoucherGridFieldDataColumns.BUTTONCANCEL', 'Cancel'))->setAttribute('data-icon', 'cross-circle')->setDescription(_t('VoucherGridFieldDataColumns.CANCEL_VOUCHER', 'Cancel Voucher'));
			$content .= $field->Field();
		}
		return $content;
    }

    public function getActions($gridField) {
        return array(
            'cancel'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'cancel') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }

        	if(!$item->canCancel()) {
                throw new ValidationException(_t('VoucherGridFieldDataColumns.CANCEL_PERMISSION', 'No permission to cancel these item'), 0);
            }
			
			try {
	            DB::getConn()->transactionStart();
	            $item->Status = 'Cancelled';
        		$item->write();
	            DB::getConn()->transactionEnd();
	        }
	        catch(ValidationException $e) {
	            DB::getConn()->transactionRollback();
	            throw new ValidationException($e->getMessage(), 0);
	        }
        }
    }
}
