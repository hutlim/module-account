<?php
/**
 * @package  account
 */

class DepositGridFieldDetailForm extends GridFieldDetailForm {
}

class DepositGridFieldDetailForm_ItemRequest extends GridFieldDetailForm_ItemRequest {
    private static $allowed_actions = array(
        'edit',
        'view',
        'ItemEditForm',
        'doPrint',
        'doViewPayment'
    );
    
    public function updateCMSActions() {
        $actions = FieldList::create();
		
		if($this->record->canPrint()){
			$printButton = 	FormAction::create('doPrint', _t('DepositGridFieldDetailForm.BUTTONPRINT', 'Print'))->setAttribute('data-icon', 'grid_print')->setAttribute('data-print-link', $this->Link('doPrint'))->setUseButtonTag(true);
			$actions->push($printButton);
		}
        
        if(Permission::check('CMS_ACCESS_PaymentAdmin') && $this->record->PaymentReceipt()->canView() && $this->record->PaymentReceipt()->exists()){
            $viewPaymentButton = FormAction::create('doViewPayment', _t('DepositGridFieldDetailForm.BUTTONVIEWRECEIPT', 'View Payment Receipt'))->setAttribute('data-icon', 'navigation')->setUseButtonTag(true);
            $actions->push($viewPaymentButton);
        }
        
        if($this->record->canApprove()){
            $approveButton = FormAction::create('doApproveDeposit', _t('DepositGridFieldDetailForm.BUTTONAPPROVE', 'Approve'))->setAttribute('data-icon', 'accept')->setUseButtonTag(true);
            $actions->push($approveButton);
        }

        return $actions;
    }

    public function ItemEditForm() {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('account/javascript/lang');
		Requirements::javascript('account/javascript/DepositAction.min.js');
		
        $form = parent::ItemEditForm();
		$fields = FieldList::create(
             LiteralField::create('Statement', $this->record->ViewHTML())
        );

		$form->setFields($fields);
        $form->setActions($this->updateCMSActions());
        return $form;
    }

    public function doApproveDeposit($data, $form) {
        $controller = $this->getToplevelController();

        try {
            DB::getConn()->transactionStart();

			if($this->record->Status == 'Fully Paid'){
				$id = DepositSubmission::update_status($this->record->ID, 'Approved', $data['Remark']);
			}
			else{
				Config::inst()->update('DepositSubmission', 'auto_approve', true);
				foreach($this->record->PaymentReceipt()->Payments() as $payment){
					if($payment->canApprove()){
						$payment->completePayment();
					}
				};
				
				if($data['Remark']){
					$this->record->Remark = $data['Remark'];
					$this->record->write();
				}
			}
            DB::getConn()->transactionEnd();
            $this->record = DataObject::get_by_id($this->record->ClassName, $this->record->ID);
            $this->gridField->getList()->add($this->record);
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            $form->sessionMessage($e->getResult()->message(), 'bad');
            $responseNegotiator = new PjaxResponseNegotiator( array(
                'CurrentForm' => function() use (&$form) {
                    return $form->forTemplate();
                },
                'default' => function() use (&$controller) {
                    return $controller->redirectBack();
                }

            ));
            if($controller->getRequest()->isAjax()) {
                $controller->getRequest()->addHeader('X-Pjax', 'CurrentForm');
            }
            return $responseNegotiator->respond($controller->getRequest());
        }

        $message = _t('DepositGridFieldDetailForm.SUCCESS_APPROVED_LOAD_DEPOSIT', 'Load deposit has been successfully approved.');
        //end custom code

        $form->sessionMessage($message, 'good');

        if($this->gridField->getList()->byId($this->record->ID)) {
            // Return new view, as we can't do a "virtual redirect" via the CMS Ajax
            // to the same URL (it assumes that its content is already current, and doesn't reload)
            return $this->edit($controller->getRequest());
        } else {
            // Changes to the record properties might've excluded the record from
            // a filtered list, so return back to the main view if it can't be found
            $noActionURL = $controller->removeAction($data['url']);
            $controller->getRequest()->addHeader('X-Pjax', 'Content');
            return $controller->redirect($noActionURL, 302);
        }
    }
    
    public function doPrint(){
        return $this->record->ViewPrint();
    }
    
    public function doViewPayment(){
        $link = Controller::join_links(PaymentAdmin::create()->Link(), 'Receipt', "EditForm", "field", 'Receipt', "item", $this->record->PaymentReceiptID, "edit");
        return Controller::curr()->redirect($link);
    }
}
