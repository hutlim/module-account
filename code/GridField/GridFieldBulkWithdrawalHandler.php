<?php
/**
 * Bulk action handler for approve/reject withdrawal
 * 
 * @package account
 */
class GridFieldBulkWithdrawalHandler extends GridFieldBulkHandler {	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('approve', 'reject');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'approve' => 'approve',
		'reject' => 'reject'
	);
	

	/**
	 * Approve the selected records passed from the approve bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of approved records ID
	 */
	public function approve(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canApprove()){
					array_push($ids, $record->ID);
					$record->Status = 'Approved';
					$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkWithdrawalHandler.SUCCESS_APPROVED', 'Total {count} withdrawal request has been approved', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
	
	/**
	 * Reject the selected records passed from the reject bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of rejected records ID
	 */
	public function reject(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canReject()){
					array_push($ids, $record->ID);
					$record->Status = 'Rejected';
					$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkWithdrawalHandler.SUCCESS_REJECTED', 'Total {count} withdrawal request has been rejected', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}