<?php
/**
 * Bulk action handler for revert convert
 * 
 * @package account
 */
class GridFieldBulkConvertHandler extends GridFieldBulkHandler {	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('revert');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'revert' => 'revert'
	);
	

	/**
	 * Approve the selected records passed from the approve bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of approved records ID
	 */
	public function revert(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canRevert()){
					array_push($ids, $record->ID);
					$record->IsRevert = 1;
					$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkConvertHandler.SUCCESS_REVERTED', 'Total {count} convert request has been reverted', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}