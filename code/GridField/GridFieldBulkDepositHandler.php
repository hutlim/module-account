<?php
/**
 * Bulk action handler for approve deposit submission
 * 
 * @package account
 */
class GridFieldBulkDepositHandler extends GridFieldBulkHandler {	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('approve');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'approve' => 'approve'
	);
	

	/**
	 * Approve the selected records passed from the approve bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of approved records ID
	 */
	public function approve(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canApprove()){
					array_push($ids, $record->ID);
					if($record->Status == 'Fully Paid'){
						DepositSubmission::update_status($record->ID, 'Approved');
					}
					else{
						Config::inst()->update('DepositSubmission', 'auto_approve', true);
						foreach($record->PaymentReceipt()->Payments() as $payment){
							if($payment->canApprove()){
								$payment->completePayment();
							}
						};
					}
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e){
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkDepositHandler.SUCCESS_APPROVED', 'Total {count} deposit submission has been approved', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}