<?php
/**
 * Bulk action handler for cancel voucher
 * 
 * @package account
 */
class GridFieldBulkVoucherHandler extends GridFieldBulkHandler {	
	/**
	 * RequestHandler allowed actions
	 * @var array
	 */
	private static $allowed_actions = array('cancel');


	/**
	 * RequestHandler url => action map
	 * @var array
	 */
	private static $url_handlers = array(
		'cancel' => 'cancel'
	);
	

	/**
	 * Approve the selected records passed from the approve bulk action
	 * 
	 * @param SS_HTTPRequest $request
	 * @return SS_HTTPResponse List of approved records ID
	 */
	public function cancel(SS_HTTPRequest $request){
		$ids = array();
		
		try {
            DB::getConn()->transactionStart();
			foreach ($this->getRecords() as $record){
				if($record->canCancel()){
					array_push($ids, $record->ID);
					$record->Status = 'Cancelled';
					$record->write();
				}
			}
            DB::getConn()->transactionEnd();
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            throw new ValidationException($e->getMessage(), 0);
        }

		$response = new SS_HTTPResponse(Convert::raw2json(array(
			'result' => 'success',
			'message' => _t('GridFieldBulkVoucherHandler.SUCCESS_CANCELLED', 'Total {count} voucher has been cancelled', '', array('count' => sizeof($ids)))
		)));
		$response->addHeader('Content-Type', 'text/json');
		return $response;	
	}
}