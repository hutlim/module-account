<?php
/**
 * @package account
 */
class WithdrawalGridFieldDataColumns implements GridField_ColumnProvider, GridField_ActionProvider {
    public function augmentColumns($gridField, &$columns) {
        if(!in_array('Actions', $columns)) {
            $columns[] = 'Actions';
        }
    }

    public function getColumnAttributes($gridField, $record, $columnName) {
        return array('class' => 'col-buttons');
    }

    public function getColumnMetadata($gridField, $columnName) {
        if($columnName == 'Actions') {
            return array('title' => '');
        }
    }

    public function getColumnsHandled($gridField) {
        return array('Actions');
    }

    public function getColumnContent($gridField, $record, $columnName) {
    	Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('account/javascript/lang');
		Requirements::javascript('account/javascript/WithdrawalAction.min.js');
    	Requirements::css('account/css/WithdrawalGridFieldDataColumns.css');
    	$content = '';
		
		if($record->canReject()){
        	$field = GridField_FormAction::create($gridField, 'Reject' . $record->ID, false, "reject", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-reject-widthdrawal')->setAttribute('title', _t('WithdrawalGridFieldDataColumns.BUTTONREJECT', 'Reject'))->setAttribute('data-icon', 'cross-circle')->setDescription(_t('WithdrawalGridFieldDataColumns.REJECT_WITHDRAWAL', 'Reject Withdrawal'));
			$content .= $field->Field();
		}
		
		if($record->canApprove()){
    		$field = GridField_FormAction::create($gridField, 'Approve' . $record->ID, false, "approve", array('RecordID' => $record->ID))->addExtraClass('gridfield-button-approve-widthdrawal')->setAttribute('title', _t('WithdrawalGridFieldDataColumns.BUTTONAPPROVE', 'Approve'))->setAttribute('data-icon', 'accept')->setDescription(_t('WithdrawalGridFieldDataColumns.APPROVE_WITHDRAWAL', 'Approve Withdrawal'));
			$content .= $field->Field();
		}

		return $content;
    }

    public function getActions($gridField) {
        return array(
            'approve',
            'reject'
        );
    }

    public function handleAction(GridField $gridField, $actionName, $arguments, $data) {
        if($actionName == 'approve' || $actionName == 'reject') {
            $item = $gridField->getList()->byID($arguments['RecordID']);
            if(!$item) {
                return;
            }
			
            if($actionName == 'approve') {
            	if(!$item->canApprove()) {
	                throw new ValidationException(_t('WithdrawalGridFieldDataColumns.APPROVE_PERMISSION', 'No permission to approve these item'), 0);
	            }
				
				try {
		            DB::getConn()->transactionStart();
					$item->Remark = isset($data['Remark']) ? $data['Remark'] : '';
	                $item->Status = 'Approved';
	            	$item->write();
		            DB::getConn()->transactionEnd();
		        }
		        catch(ValidationException $e) {
		            DB::getConn()->transactionRollback();
		            throw new ValidationException($e->getMessage(), 0);
		        }
            }
			
            if($actionName == 'reject') {
            	if(!$item->canReject()) {
	                throw new ValidationException(_t('WithdrawalGridFieldDataColumns.REJECT_PERMISSION', 'No permission to reject these item'), 0);
	            }
				elseif(!isset($data['Remark']) || $data['Remark'] == ''){
					throw new ValidationException(_t('WithdrawalGridFieldDataColumns.REMARK_REQUIRED', 'Remark is required'), 0);
				}
				
				try {
		            DB::getConn()->transactionStart();
		            $item->Remark = $data['Remark'];
	                $item->Status = 'Rejected';
	            	$item->write();
		            DB::getConn()->transactionEnd();
		        }
		        catch(ValidationException $e) {
		            DB::getConn()->transactionRollback();
		            throw new ValidationException($e->getMessage(), 0);
		        }
            }
        }
    }
}
