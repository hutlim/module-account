<?php

class DashboardLatestPendingDepositSubmissionPanel extends DashboardPanel {
    private static $db = array('Count' => 'Int');

    private static $defaults = array('Count' => 10);

    private static $icon = "account/images/ewallet-icon.png";

    public function getLabel() {
        return _t('DashboardLatestPendingDepositSubmissionPanel.LATEST_PENDING_DEPOSIT_SUBMISSION', 'Latest Pending Deposit Submission');
    }

    public function getDescription() {
        return _t('DashboardLatestPendingDepositSubmissionPanel.SHOW_LATEST_PENDING_DEPOSIT_SUBMISSION', 'Shows latest pending deposit submission.');
    }
    
    public function getSecondaryActions() {
        $actions = parent::getPrimaryActions();
        if(Permission::check('CMS_ACCESS_EWalletAdmin') && Permission::check('VIEW_DepositSubmission')){
            $link = Controller::join_links(EWalletAdmin::create()->Link('DepositSubmission'));
			$actions->push(DashboardPanelAjaxAction::create(
                Controller::join_links($link, sprintf('?q[Status]=Pending')), 
                _t('DashboardLatestPendingDepositSubmissionPanel.BUTTONVIEWALLPENDINGDEPOSITSUBMISSION', 'View all pending deposit submission')   
            ));
        }
        return $actions;
    }

    public function getConfiguration() {
        $fields = parent::getConfiguration();
        $fields->push(TextField::create("Count", _t('DashboardLatestPendingDepositSubmissionPanel.NUMBER_OF_PENDING_DEPOSIT_SUBMISSION_SHOW', 'Number of pending deposit submission to show')));
        return $fields;
    }

    protected function EditLink($id) {
        if(Permission::check('CMS_ACCESS_EWalletAdmin') && Permission::check('VIEW_DepositSubmission')){
            $link = Controller::join_links(EWalletAdmin::create()->Link('DepositSubmission'), 'EditForm', 'field', 'DepositSubmission', 'item', $id, 'edit', sprintf('?q[Status]=Pending'));
        }
        else{
            $link = 'javascript:void(0)';
        }
        return $link;
    }

    public function PendingDepositSubmissions() {
        $deposits = DepositSubmission::get()
		->filter('Status', 'Pending')
        ->sort('Created', 'DESC')
        ->limit($this->Count);
        $set = ArrayList::create(array());
        foreach($deposits as $r) {
            $set->push(ArrayData::create(array(
                'EditLink' => $this->EditLink($r->ID),
                'Title' => _t('DashboardLatestPendingDepositSubmissionPanel.PENDING_DEPOSIT_SUBMISSION', '{username} has request deposit {amount} on {created}', '', array('username' => $r->Member()->Username, 'amount' => $r->dbObject('Amount')->Nice(), 'created' => $r->dbObject('Created')->Nice()))
            )));
        }
        return $set;
    }

	public function TotalPendingDepositSubmission(){
		return $members = DepositSubmission::get()->filter('Status', 'Pending')->count();
	}
}
