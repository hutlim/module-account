<?php

class DashboardLatestPendingCashWithdrawalPanel extends DashboardPanel {
    private static $db = array('Count' => 'Int');

    private static $defaults = array('Count' => 10);

    private static $icon = "account/images/withdrawal-icon.png";

    public function getLabel() {
        return _t('DashboardLatestPendingCashWithdrawalPanel.LATEST_PENDING_WITHDRAWAL', 'Latest Pending Withdrawal');
    }

    public function getDescription() {
        return _t('DashboardLatestPendingCashWithdrawalPanel.SHOW_LATEST_PENDING_WITHDRAWAL', 'Shows latest pending E-Cash withdrawal.');
    }
    
    public function getSecondaryActions() {
        $actions = parent::getPrimaryActions();
        if(Permission::check('CMS_ACCESS_WithdrawalAdmin') && Permission::check('VIEW_CashAccountWithdrawal')){
            $link = Controller::join_links(WithdrawalAdmin::create()->Link('CashAccountWithdrawal'));
			$actions->push(DashboardPanelAjaxAction::create(
                Controller::join_links($link, sprintf('?q[Status]=Pending')), 
                _t('DashboardLatestPendingCashWithdrawalPanel.BUTTONVIEWALLPENDINGWITHDRAWAL', 'View all pending withdrawal')   
            ));
        }
        return $actions;
    }

    public function getConfiguration() {
        $fields = parent::getConfiguration();
        $fields->push(TextField::create("Count", _t('DashboardLatestPendingCashWithdrawalPanel.NUMBER_OF_PENDING_WITHDRAWAL_SHOW', 'Number of pending withdrawal to show')));
        return $fields;
    }

    protected function EditLink($id) {
        if(Permission::check('CMS_ACCESS_WithdrawalAdmin') && Permission::check('VIEW_CashAccountWithdrawal')){
            $link = Controller::join_links(WithdrawalAdmin::create()->Link('CashAccountWithdrawal'), 'EditForm', 'field', 'CashAccountWithdrawal', 'item', $id, 'edit', sprintf('?q[Status]=Pending'));
        }
        else{
            $link = 'javascript:void(0)';
        }
        return $link;
    }

    public function PendingWithdrawals() {
        $withdrawals = CashAccountWithdrawal::get()
		->filter('Status', 'Pending')
        ->sort('Created', 'DESC')
        ->limit($this->Count);
        $set = ArrayList::create(array());
        foreach($withdrawals as $r) {
            $set->push(ArrayData::create(array(
                'EditLink' => $this->EditLink($r->ID),
                'Title' => _t('DashboardLatestPendingCashWithdrawalPanel.PENDING_WITHDRAWAL', '{username} has withdraw {amount} on {created}', '', array('username' => $r->Member()->Username, 'amount' => $r->dbObject('Amount')->Nice(), 'created' => $r->dbObject('Created')->Nice()))
            )));
        }
        return $set;
    }

	public function TotalPendingWithdrawal(){
		return $members = CashAccountWithdrawal::get()->filter('Status', 'Pending')->count();
	}
}
