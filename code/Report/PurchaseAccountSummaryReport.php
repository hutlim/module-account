<?php

/**
 * @package account
 */

class PurchaseAccountSummaryReport extends GeneralReport {
	protected $sort = 40;
	
	public function title() {
		return _t('PurchaseAccountSummaryReport.TITLE', 'E-Purchase Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('PurchaseAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$ret = PurchaseAccount::get()->sort($sort)->limit($limit)->filter('Member.IsDistributor', 1);

        if(isset($params['StartDate']) && $params['StartDate']){
        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() .' 00:00:00');
        }

		if(isset($params['EndDate']) && $params['EndDate']){
			$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() .' 23:59:59');
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }

		$ret = $ret->alterDataQuery(function($query){
			$query->groupby('"PurchaseAccount"."MemberID"')->having('"PurchaseAccount"."MemberID" > 0');
		});

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('PurchaseAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('PurchaseAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('PurchaseAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('PurchaseAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('PurchaseAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('PurchaseAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('PurchaseAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
