<?php

/**
 * @package account
 */

class ProductAccountSummaryReport extends GeneralReport {
	protected $sort = 30;
	
	public function title() {
		return _t('ProductAccountSummaryReport.TITLE', 'E-Product Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('ProductAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$ret = ProductAccount::get()->sort($sort)->limit($limit)->filter('Member.IsDistributor', 1);

        if(isset($params['StartDate']) && $params['StartDate']){
        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() .' 00:00:00');
        }

		if(isset($params['EndDate']) && $params['EndDate']){
			$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() .' 23:59:59');
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }

		$ret = $ret->alterDataQuery(function($query){
			$query->groupby('"ProductAccount"."MemberID"')->having('"ProductAccount"."MemberID" > 0');
		});

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('ProductAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('ProductAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('ProductAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('ProductAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('ProductAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('ProductAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('ProductAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
