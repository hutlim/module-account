<?php

/**
 * @package account
 */

class ProductAccountBalanceReport extends GeneralReport {
	protected $sort = 30;
    protected $total_balance = 0;
	
	public function title() {
		return _t('ProductAccountBalanceReport.TITLE', 'E-Product Account Balance Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('ProductAccount');
		
        $fields->insertBefore(NumericField_Readonly::create('TotalBalance', _t('CashAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'), DBField::create_field('Currency', $this->total_balance)->Nice()), 'Report');
        
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
		$ret = Member::get()->sort($sort)->limit($limit)->filter('IsDistributor', 1)->innerJoin('ProductAccountBalance', '"Member"."ID" = "ProductAccountBalance"."MemberID"');

        if(isset($params['StartBalance']) && is_numeric($params['StartBalance'])){
        	$ret = $ret->where(sprintf('"ProductAccountBalance"."Balance" >= %s', (int)$params['StartBalance']));
        }

		if(isset($params['EndBalance']) && is_numeric($params['EndBalance'])){
			$ret = $ret->where(sprintf('"ProductAccountBalance"."Balance" <= %s', (int)$params['EndBalance']));
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Username:PartialMatch', $params['Username']);
        }
		
		$ret = $ret->alterDataQuery(function($query){
			$query->selectFromTable('ProductAccountBalance', array('TotalDebit', 'TotalCredit', 'Balance'));
		});

        $this->total_balance = $ret->sum('Balance');
        
		return $ret;
	}

	public function columns() {
		$fields = array(
			'Username' => array(
				'title' => _t('ProductAccountBalanceReport.USERNAME', 'Username')
			),
			'Name' => array(
				'title' => _t('ProductAccountBalanceReport.NAME', 'Name')
			),
			'TotalCredit' => array(
                'title' => _t('ProductAccountBalanceReport.TOTAL_IN', 'Total In'),
                'casting' => 'Decimal->Nice'
            ),
            'TotalDebit' => array(
                'title' => _t('ProductAccountBalanceReport.TOTAL_OUT', 'Total Out'),
                'casting' => 'Decimal->Nice'
            ),
            'Balance' => array(
                'title' => _t('ProductAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	NumericField::create('filters[StartBalance]', '')->addExtraClass('no-change-track'),
				NumericField::create('filters[EndBalance]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('ProductAccountBalanceReport.TOTAL_BALANCE', 'Total Balance')),
			UsernameField::create('Username', _t('ProductAccountBalanceReport.USERNAME', 'Username'))
		);
	}
}
