<?php

/**
 * @package account
 */

class CashAccountSummaryReport extends GeneralReport {
	protected $sort = 20;
	
	public function title() {
		return _t('CashAccountSummaryReport.TITLE', 'E-Cash Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('CashAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$ret = CashAccount::get()->sort($sort)->limit($limit)->filter('Member.IsDistributor', 1);

        if(isset($params['StartDate']) && $params['StartDate']){
        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() .' 00:00:00');
        }

		if(isset($params['EndDate']) && $params['EndDate']){
			$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() .' 23:59:59');
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }

		$ret = $ret->alterDataQuery(function($query){
			$query->groupby('"CashAccount"."MemberID"')->having('"CashAccount"."MemberID" > 0');
		});

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('CashAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('CashAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('CashAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('CashAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('CashAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('CashAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('CashAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
