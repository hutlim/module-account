<?php

/**
 * @package account
 */

class CashAccountBalanceReport extends GeneralReport {
	protected $sort = 20;
    protected $total_balance = 0;
	
	public function title() {
		return _t('CashAccountBalanceReport.TITLE', 'E-Cash Account Balance Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('CashAccount');
		
        $fields->insertBefore(NumericField_Readonly::create('TotalBalance', _t('CashAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'), DBField::create_field('Currency', $this->total_balance)->Nice()), 'Report');
        
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
		$ret = Member::get()->sort($sort)->limit($limit)->filter('IsDistributor', 1)->innerJoin('CashAccountBalance', '"Member"."ID" = "CashAccountBalance"."MemberID"');

        if(isset($params['StartBalance']) && is_numeric($params['StartBalance'])){
        	$ret = $ret->where(sprintf('"CashAccountBalance"."Balance" >= %s', (int)$params['StartBalance']));
        }

		if(isset($params['EndBalance']) && is_numeric($params['EndBalance'])){
			$ret = $ret->where(sprintf('"CashAccountBalance"."Balance" <= %s', (int)$params['EndBalance']));
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Username:PartialMatch', $params['Username']);
        }
		
		$ret = $ret->alterDataQuery(function($query){
			$query->selectFromTable('CashAccountBalance', array('TotalDebit', 'TotalCredit', 'Balance'));
		});

        $this->total_balance = $ret->sum('Balance');
        
		return $ret;
	}

	public function columns() {
		$fields = array(
			'Username' => array(
				'title' => _t('CashAccountBalanceReport.USERNAME', 'Username')
			),
			'Name' => array(
				'title' => _t('CashAccountBalanceReport.NAME', 'Name')
			),
			'TotalCredit' => array(
                'title' => _t('CashAccountBalanceReport.TOTAL_IN', 'Total In'),
                'casting' => 'Decimal->Nice'
            ),
            'TotalDebit' => array(
                'title' => _t('CashAccountBalanceReport.TOTAL_OUT', 'Total Out'),
                'casting' => 'Decimal->Nice'
            ),
            'Balance' => array(
                'title' => _t('CashAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	NumericField::create('filters[StartBalance]', '')->addExtraClass('no-change-track'),
				NumericField::create('filters[EndBalance]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('CashAccountBalanceReport.TOTAL_BALANCE', 'Total Balance')),
			UsernameField::create('Username', _t('CashAccountBalanceReport.USERNAME', 'Username'))
		);
	}
}
