<?php

/**
 * @package account
 */

class BonusAccountBalanceReport extends GeneralReport {
	protected $sort = 10;
    protected $total_balance = 0;
	
	public function title() {
		return _t('BonusAccountBalanceReport.TITLE', 'E-Bonus Account Balance Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('BonusAccount');
		                                                                                                                                                                  
        $fields->insertBefore(NumericField_Readonly::create('TotalBalance', _t('BonusAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'), DBField::create_field('Currency', $this->total_balance)->Nice()), 'Report');
        
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
		$ret = Member::get()->sort($sort)->limit($limit)->filter('IsDistributor', 1)->innerJoin('BonusAccountBalance', '"Member"."ID" = "BonusAccountBalance"."MemberID"');

        if(isset($params['StartBalance']) && is_numeric($params['StartBalance'])){
        	$ret = $ret->where(sprintf('"BonusAccountBalance"."Balance" >= %s', (int)$params['StartBalance']));
        }

		if(isset($params['EndBalance']) && is_numeric($params['EndBalance'])){
			$ret = $ret->where(sprintf('"BonusAccountBalance"."Balance" <= %s', (int)$params['EndBalance']));
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Username:PartialMatch', $params['Username']);
        }
		
		$ret = $ret->alterDataQuery(function($query){
			$query->selectFromTable('BonusAccountBalance', array('TotalDebit', 'TotalCredit', 'Balance'));
		});
        
        $this->total_balance = $ret->sum('Balance');

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Username' => array(
				'title' => _t('BonusAccountBalanceReport.USERNAME', 'Username')
			),
			'Name' => array(
				'title' => _t('BonusAccountBalanceReport.NAME', 'Name')
			),
			'TotalCredit' => array(
                'title' => _t('BonusAccountBalanceReport.TOTAL_IN', 'Total In'),
                'casting' => 'Decimal->Nice'
            ),
            'TotalDebit' => array(
                'title' => _t('BonusAccountBalanceReport.TOTAL_OUT', 'Total Out'),
                'casting' => 'Decimal->Nice'
            ),
            'Balance' => array(
                'title' => _t('BonusAccountBalanceReport.TOTAL_BALANCE', 'Total Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	NumericField::create('filters[StartBalance]', '')->addExtraClass('no-change-track'),
				NumericField::create('filters[EndBalance]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('BonusAccountBalanceReport.TOTAL_BALANCE', 'Total Balance')),
			UsernameField::create('Username', _t('BonusAccountBalanceReport.USERNAME', 'Username'))
		);
	}
}
