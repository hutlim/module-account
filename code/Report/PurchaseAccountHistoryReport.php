<?php

/**
 * @package account
 */

class PurchaseAccountHistoryReport extends GeneralReport {
	protected $sort = 40;
	
	public function title() {
		return _t('PurchaseAccountHistoryReport.TITLE', 'E-Purchase Transaction History Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('PurchaseAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
        
        $ret = PurchaseAccount::get()->sort($sort)->limit($limit);

        if(isset($params['Type']) && $params['Type']){
            $ret = $ret->filter('Type', $params['Type']);
        }

		if(isset($params['Submit']) && $params['Submit']){
	        if(isset($params['StartDate']) && $params['StartDate']){
	        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() . '00:00:00');
	        }
		}
		else{
			$ret = $ret->filter('Date:GreaterThanOrEqual', date('Y-m-01 00:00:00'));
		}
		
		if(isset($params['Submit']) && $params['Submit']){
			if(isset($params['EndDate']) && $params['EndDate']){
	        	$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() . '23:59:59');
	        }
		}
		else{
			$ret = $ret->filter('Date:LessThanOrEqual', date('Y-m-t 23:59:59'));
		}
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }
        
        if(isset($params['Reference']) && $params['Reference']){
        	$ret = $ret->filter('Reference:PartialMatch', $params['Reference']);
        }

		return $ret;
	}

	public function columns() {
		$fields = array(
            'Date' => array(
                'title' => _t('PurchaseAccountHistoryReport.DATE', 'Date'),
                'casting' => 'SS_Datetime->Nice'
            ),
            'TypeByLang' => array(
                'title' => _t('PurchaseAccountHistoryReport.TYPE', 'Type')
            ),
			'Member.Username' => array(
				'title' => _t('PurchaseAccountHistoryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('PurchaseAccountHistoryReport.NAME', 'Name')
			),
			'Reference' => array(
				'title' => _t('PurchaseAccountHistoryReport.REFERENCE', 'Reference')
			),
			'Description' => array(
				'title' => _t('PurchaseAccountHistoryReport.DESCRIPTION', 'Description')
			),
			'Credit' => array(
                'title' => _t('PurchaseAccountHistoryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('PurchaseAccountHistoryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('PurchaseAccountHistoryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
			singleton('PurchaseAccountType')->getDropdownField('Type', _t('PurchaseAccountHistoryReport.TYPE', 'Type'))->setEmptyString(_t('PurchaseAccountHistoryReport.ALL', 'All')),
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '', date('Y-m-01'))->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '', date('Y-m-t'))->addExtraClass('no-change-track')
			)->setTitle(_t('PurchaseAccountHistoryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('PurchaseAccountHistoryReport.USERNAME', 'Username')),
			TextField::create('Reference', _t('PurchaseAccountHistoryReport.REFERENCE', 'Reference')),
			HiddenField::create('Submit', 'Submit', 1)
		);
	}
}
