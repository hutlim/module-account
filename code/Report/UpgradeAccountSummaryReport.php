<?php

/**
 * @package account
 */

class UpgradeAccountSummaryReport extends GeneralReport {
	protected $sort = 50;
	
	public function title() {
		return _t('UpgradeAccountSummaryReport.TITLE', 'E-Purchase Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('UpgradeAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}

		$ret = UpgradeAccount::get()->sort($sort)->limit($limit)->filter('Member.IsDistributor', 1);

        if(isset($params['StartDate']) && $params['StartDate']){
        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() .' 00:00:00');
        }

		if(isset($params['EndDate']) && $params['EndDate']){
			$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() .' 23:59:59');
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }

		$ret = $ret->alterDataQuery(function($query){
			$query->groupby('"UpgradeAccount"."MemberID"')->having('"UpgradeAccount"."MemberID" > 0');
		});

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('UpgradeAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('UpgradeAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('UpgradeAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('UpgradeAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('UpgradeAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('UpgradeAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('UpgradeAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
