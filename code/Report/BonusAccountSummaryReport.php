<?php

/**
 * @package account
 */

class BonusAccountSummaryReport extends GeneralReport {
	protected $sort = 10;
	
	public function title() {
		return _t('BonusAccountSummaryReport.TITLE', 'E-Bonus Transaction Summary Report');
	}
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();

		$fields->dataFieldByName('Report')->setModelClass('BonusAccount');
		
		return $fields;
	}
	
	public function sourceRecords($params, $sort, $limit) {
		if($sort) {
			$parts = explode(' ', $sort);
			$field = $parts[0];
			$direction = $parts[1];
			
			if($field == 'Name') {
			    $sort = 'FirstName ' . $direction;
			}
		}
		
		$ret = BonusAccount::get()->sort($sort)->limit($limit)->filter('Member.IsDistributor', 1);

        if(isset($params['StartDate']) && $params['StartDate']){
        	$ret = $ret->filter('Date:GreaterThanOrEqual', DBField::create_field('Date', $params['StartDate'])->URLDate() .' 00:00:00');
        }

		if(isset($params['EndDate']) && $params['EndDate']){
			$ret = $ret->filter('Date:LessThanOrEqual', DBField::create_field('Date', $params['EndDate'])->URLDate() .' 23:59:59');
        }
        
        if(isset($params['Username']) && $params['Username']){
        	$ret = $ret->filter('Member.Username:PartialMatch', $params['Username']);
        }

		$ret = $ret->alterDataQuery(function($query){
			$query->groupby('"BonusAccount"."MemberID"')->having('"BonusAccount"."MemberID" > 0');
		});

		return $ret;
	}

	public function columns() {
		$fields = array(
			'Member.Username' => array(
				'title' => _t('BonusAccountSummaryReport.USERNAME', 'Username')
			),
			'Member.Name' => array(
				'title' => _t('BonusAccountSummaryReport.NAME', 'Name')
			),
			'Credit' => array(
                'title' => _t('BonusAccountSummaryReport.IN', 'In'),
                'casting' => 'Decimal->Nice'
            ),
            'Debit' => array(
                'title' => _t('BonusAccountSummaryReport.OUT', 'Out'),
                'casting' => 'Decimal->Nice'
            ),
            'ForwardBalance' => array(
                'title' => _t('BonusAccountSummaryReport.BALANCE', 'Balance'),
                'casting' => 'Decimal->Nice'
            )
		);
		
		return $fields;
	}
	
	public function parameterFields() {
		return new FieldList(
            FieldGroup::create(
            	DateField::create('filters[StartDate]', '')->addExtraClass('no-change-track'),
				DateField::create('filters[EndDate]', '')->addExtraClass('no-change-track')
			)->setTitle(_t('BonusAccountSummaryReport.DATE', 'Date')),
			UsernameField::create('Username', _t('BonusAccountSummaryReport.USERNAME', 'Username'))
		);
	}
}
