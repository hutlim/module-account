<?php

class WithdrawalAmountDropdownField extends DropdownField {
	protected $minimum_withdraw = 100;
	protected $maximum_withdraw = 10000;
	protected $withdraw_range = 100;
	
	function getSource(){
		$source = parent::getSource();
		if(!sizeof($source)){
	    	for($i = $this->minimum_withdraw; $i <= $this->maximum_withdraw; $i+=$this->withdraw_range){
	    		$source[$i] = DBField::create_field('Currency', $i)->Nice();
	    	}
		}
		
		return $source;
	}
	
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'withdrawalamount'
            )
        );
    }
	
	function Type() {
		return 'withdrawalamountdropdown dropdown';
	}

	function setMinimumWithdrawal($minimum_withdraw){
		$this->minimum_withdraw = $minimum_withdraw;
		return $this;
	}
	
	function getMinimumWithdrawal(){
		return $this->minimum_withdraw;
	}
	
	function setMaximumWithdrawal($maximum_withdraw){
		$this->maximum_withdraw = $maximum_withdraw;
		return $this;
	}
	
	function getMaximumWithdrawal(){
		return $this->maximum_withdraw;
	}
	
	function setWithdrawalRange($withdraw_range){
		$this->withdraw_range = $withdraw_range;
		return $this;
	}
	
	function getWithdrawalRange(){
		return $this->withdraw_range;
	}
}
