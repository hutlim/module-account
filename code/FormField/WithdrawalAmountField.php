<?php

class WithdrawalAmountField extends NumericField {
	protected $minimum_withdraw = 0;
	protected $maximum_withdraw = 0;
	
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'withdrawalamount',
                'data-inputmask-allowminus' => 'false'
            )
        );
    }
	
	function Type() {
		return parent::Type() . ' withdrawalamount';
	}
	
    function validate($validator) {
    	$result = parent::validate($validator);
        $amount = $this->dataValue();
		
        if($amount <= 0) {
            $validator->validationError(
            	$this->getName(), 
            	_t('WithdrawalAmountField.INVALID_AMOUNT', 'Invalid withdrawal amount'), 
            	'warning'
			);
            $result = false;
        }
        else if($this->getMinimumWithdrawal() && $this->getMinimumWithdrawal() > $amount){
            $validator->validationError(
            	$this->getName(), 
            	_t('WithdrawalAmountField.MINIMUM_WITHDRAWAL_AMOUNT', 'Minimum withdrawal amount is {minimum_withdrawal_amount}', 'Display minimum withdrawal amount', array('minimum_withdrawal_amount' => DBField::create_field('Currency', $this->minimum_withdraw)->Nice())), 
            	'warning'
			);
            $result = false;
        }
		else if($this->getMaximumWithdrawal() && $amount > $this->getMaximumWithdrawal()){
            $validator->validationError(
            	$this->getName(), 
            	_t('WithdrawalAmountField.MAXIMUM_WITHDRAWAL_AMOUNT', 'Maximum withdrawal amount is {maximum_withdrawal_amount}', 'Display maximum withdrawal amount', array('maximum_withdrawal_amount' => DBField::create_field('Currency', $this->maximum_withdraw)->Nice())), 
            	'warning'
			);
            $result = false;
        }

        return $result;
    }

	function setMinimumWithdrawal($minimum_withdraw){
		$this->minimum_withdraw = $minimum_withdraw;
		return $this;
	}
	
	function getMinimumWithdrawal(){
		return $this->minimum_withdraw;
	}
	
	function setMaximumWithdrawal($maximum_withdraw){
		$this->maximum_withdraw = $maximum_withdraw;
		return $this;
	}
	
	function getMaximumWithdrawal(){
		return $this->maximum_withdraw;
	}
}
