<?php

class DepositAmountField extends NumericField {
	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'depositamount',
                'data-inputmask-allowminus' => 'false'
            )
        );
    }
	
	function Type() {
		return parent::Type() . ' depositamount';
	}
	
    function validate($validator) {
        $result = parent::validate($validator);
		
        if($this->dataValue() <= 0) {
            $validator->validationError(
            	$this->getName(), 
            	_t('DepositAmountField.INVALID_AMOUNT', 'Invalid deposit amount'), 
            	'warning'
			);
            $result = false;
        }

        return $result;
    }
}
