<?php

class TransferAmountField extends NumericField {
	protected $converted = null;
	protected $rate = null;
	/**
	 * @var AmountField
	 */
	protected $amountField = null;
	
	/**
	 * @var RateField
	 */
	protected $rateField = null;
	
	/**
	 * @var ConvertedField
	 */
	protected $convertedField = null;
	
	function __construct($name, $title = null, $value = ""){
		Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
        Requirements::javascript('account/javascript/TransferAmountField.min.js');
		
		$this->amountField = NumericField::create($name, _t('TransferAmountField.AMOUNT', 'Amount'));
		$this->rateField = NumericField::create($name . 'Rate', _t('TransferAmountField.RATE', 'Rate'));
		$this->convertedField = NumericField::create($name . 'Converted', _t('TransferAmountField.RECEIVABLE', 'Receivable'));

		parent::__construct($name, $title, $value);
	}
	
	function Field($properties = array()) {
		$this->amountField->setAttribute('rel', 'transfer-amount');
		$this->amountField->setAttribute('data-inputmask-allowminus', 'false');
		if ($this->Required()) {
			$this->amountField->setAttribute('required', 'required');
			$this->amountField->setAttribute('aria-required', 'true');
		}
		$this->rateField->setAttribute('rel', 'transfer-rate');
		$this->rateField->setAttribute('readonly', 'readonly');
		$this->rateField->setAttribute('autocomplete', 'off');
		$this->rateField->setAttribute('tabindex', '-1');
		$this->rateField->setAttribute('data-inputmask-allowminus', 'false');
		$this->convertedField->setAttribute('rel', 'transfer-converted');
		$this->convertedField->setAttribute('readonly', 'readonly');
		$this->convertedField->setAttribute('autocomplete', 'off');
		$this->convertedField->setAttribute('tabindex', '-1');
		$this->convertedField->setAttribute('data-inputmask-allowminus', 'false');
        return $this->amountField->Field() . $this->rateField->Field() . $this->convertedField->Field();
    }

	function FieldHolder($properties = array()) {
		$this->amountField->setAttribute('rel', 'transfer-amount');
		$this->amountField->setAttribute('data-inputmask-allowminus', 'false');
		if ($this->Required()) {
			$this->amountField->setAttribute('required', 'required');
			$this->amountField->setAttribute('aria-required', 'true');
		}
		$this->rateField->setAttribute('rel', 'transfer-rate');
		$this->rateField->setAttribute('readonly', 'readonly');
		$this->rateField->setAttribute('autocomplete', 'off');
		$this->rateField->setAttribute('tabindex', '-1');
		$this->rateField->setAttribute('data-inputmask-allowminus', 'false');
		$this->convertedField->setAttribute('rel', 'transfer-converted');
		$this->convertedField->setAttribute('readonly', 'readonly');
		$this->convertedField->setAttribute('autocomplete', 'off');
		$this->convertedField->setAttribute('tabindex', '-1');
		$this->convertedField->setAttribute('data-inputmask-allowminus', 'false');
        return $this->amountField->FieldHolder() . $this->rateField->FieldHolder() . $this->convertedField->FieldHolder();
    }

	function getAttributes() {
        return array_merge(
            parent::getAttributes(), array(
            	'rel' => 'transferamount',
                'data-inputmask-allowminus' => 'false'
            )
        );
    }
	
	function Type() {
		return parent::Type() . ' transferamount';
	}
	
	/**
	 * Set the field value.
	 * 
	 * @param mixed $value
	 * @return FormField Self reference
	 */
	function setValue($value, $data = array()) {
		if(is_array($value)) {
			$value = $value['amount'];
			$rate = $value['rate'];
			$converted = $value['converted'];
		} else {
			parent::setValue($value, $data);
			$value = $this->dataValue();
			$rate = $this->rate;
			$converted = $value * $rate;
		}

		$this->amountField->setValue($value);
		$this->setRate($rate);
		$this->setConverted($converted);

		return $this;
	}
	
	function setRate($rate) {
		$this->rate = $rate;
		$this->rateField->setValue($rate);
		return $this;
	}

	function getRate() {
		return $this->rate;
	}
	
	function setConverted($converted) {
		$this->converted = $converted;
		$this->convertedField->setValue($converted);
		return $this;
	}

	function getConverted() {
		return $this->converted;
	}
    
    function getAmountField(){
        return $this->amountField;
    }
    
    function setRateField($field){
        $this->rateField = $this->rateField->castedCopy($field);
        return $this;
    }
    
    function getRateField(){
        return $this->rateField;
    }
    
    function setConvertedField($field){
        $this->convertedField = $this->convertedField->castedCopy($field);
        return $this;
    }
    
    function getConvertedField(){
        return $this->convertedField;
    }
	
    function validate($validator) {
    	$result = parent::validate($validator);
		
        if($this->dataValue() <= 0) {
            $validator->validationError(
            	$this->getName(), 
            	_t('TransferAmountField.INVALID_AMOUNT', 'Invalid transfer amount'), 
            	'warning'
			);
            $result = false;
        }

        return $result;
    }
}
