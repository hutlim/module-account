<?php

class WithdrawalCurrencyField extends CurrencyDropdownField {
	function getSource(){
		$source = parent::getSource();
		if(is_array($source)){
			foreach($source as $code => $title){
				$obj = CurrencyRate::get()->filter('FromCurrency', SiteCurrencyConfig::current_site_currency())->filter('ToCurrency', $code)->sort('LastEdited', 'DESC')->first();
				if($obj && $obj->Rate > 0){
					$source[$code] = sprintf('%s (%s)', $title, _t('WithdrawalCurrencyField.RATE', 'Rate: {rate}', '', array('rate' => $obj->Rate)));
				}
			}
		}
		
		return $source;
	}
	
	public function performReadonlyTransformation() {
		$field = $this->castedCopy('ReadonlyWithdrawalCurrencyField');
		$field->setReadonly(true);
		
		return $field;
	}
}	

class ReadonlyWithdrawalCurrencyField extends ReadonlyCurrencyDropdownField {
	function getSource(){
		$source = parent::getSource();
		if(is_array($source)){
			foreach($source as $code => $title){
				$obj = CurrencyRate::get()->filter('FromCurrency', SiteCurrencyConfig::current_site_currency())->filter('ToCurrency', $code)->sort('LastEdited', 'DESC')->first();
				if($obj && $obj->Rate > 0){
					$source[$code] = sprintf('%s (%s)', $title, _t('ReadonlyWithdrawalCurrencyField.RATE', 'Rate: {rate}', '', array('rate' => $obj->Rate)));
				}
			}
		}
		
		return $source;
	}
}