<?php

class BonusAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Adjustment";
    private static $plural_name = "E-Bonus Adjustments";
    
    private static $extensions = array("AccountAdjustment");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
        return BonusAccountAdjustment::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }
    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_BonusAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusAccountAdjustment' => array(
                'name' => _t('BonusAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Bonus Account Adjustment')
            ),
            'CREATE_BonusAccountAdjustment' => array(
                'name' => _t('BonusAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('BonusAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Bonus Account Adjustment')
            )
        );
    }
}
?>