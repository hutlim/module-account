<?php

class PurchaseAccount extends DataObject {
    private static $singular_name = "E-Purchase Account";
    private static $plural_name = "E-Purchase Accounts";
    
    private static $extensions = array("Account");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return PurchaseAccount::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}
?>