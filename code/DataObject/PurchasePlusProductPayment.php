<?php

Class PurchasePlusProductPayment extends Payment {
    private static $singular_name = "E-Purchase + E-Product Payment";
    private static $plural_name = "E-Purchase + E-Product Payments";
	// manual | auto
    private static $type = 'manual';
	private static $purchase_min_percentage = 0.5;
	private static $purchase_max_percentage = 1;
	private static $priority_payment = 'PurchaseAccount';
    private static $db = array(
        'BeforePurchaseBalance' => 'Currency',
        'AfterPurchaseBalance' => 'Currency',
        'PurchaseAmount' => 'Currency',
        'BeforeProductBalance' => 'Currency',
        'AfterProductBalance' => 'Currency',
        'ProductAmount'  => 'Currency'
    );
	
	private static $many_many = array(
		'PurchaseAccounts' => 'PurchaseAccount',
		'ProductAccounts' => 'ProductAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforePurchaseBalance'] = _t('PurchasePlusProductPayment.PURCHASE_BEFORE_BALANCE', 'E-Purchase Before Balance');
		$labels['AfterPurchaseBalance'] = _t('PurchasePlusProductPayment.PURCHASE_AFTER_BALANCE', 'E-Purchase After Balance');
		$labels['PurchaseAmount'] = _t('PurchasePlusProductPayment.PURCHASE_PAID_AMOUNT', 'E-Purchase Paid Amount');
		$labels['BeforeProductBalance'] = _t('PurchasePlusProductPayment.PRODUCT_BEFORE_BALANCE', 'E-Product Before Balance');
		$labels['AfterProductBalance'] = _t('PurchasePlusProductPayment.PRODUCT_AFTER_BALANCE', 'E-Product After Balance');
		$labels['ProductAmount'] = _t('PurchasePlusProductPayment.PRODUCT_PAID_AMOUNT', 'E-Product Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'PurchasePlusProductPayment_Amount' => $this->Amount,
				'PurchasePlusProductPayment_PurchaseAmount' => $this->PurchaseAmount,
				'PurchasePlusProductPayment_ProductAmount' => $this->ProductAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforePurchaseBalance');
        $fields->makeFieldReadonly('AfterPurchaseBalance');
		$fields->makeFieldReadonly('BeforeProductBalance');
        $fields->makeFieldReadonly('AfterProductBalance');
		$fields->removeByName('PurchaseAccounts');
		$fields->removeByName('ProductAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$purchase_payment = $this->Amount * $this->config()->get('purchase_min_percentage');
    	$product_payment = $this->Amount * (1 - $this->config()->get('purchase_min_percentage'));
    	if($this->config()->get('priority_payment') == 'PurchaseAccount'){
			$purchase_balance = Account::get('PurchaseAccount', $memberid)->Balance;
			if($purchase_balance > $purchase_payment){
		    	if($this->Amount * $this->config()->get('purchase_max_percentage') < $purchase_balance){
					$purchase_payment = $this->Amount * $this->config()->get('purchase_max_percentage');
				}
				else{
					$purchase_payment = $purchase_balance;
				}
			}

			$product_payment = $this->Amount - $purchase_payment;
		}
		else{
			$product_balance = Account::get('ProductAccount', $memberid)->Balance;
	    	if($product_balance < $product_payment){
	    		if($product_balance < $this->Amount * (1 - $this->config()->get('purchase_max_percentage'))){
	    			$product_payment = $this->Amount * (1 - $this->config()->get('purchase_max_percentage'));
				}
				else{
					$product_payment = $product_balance;
				}
			}

			$purchase_payment = $this->Amount - $product_payment;
		}
		
		$fields = FieldList::create(
            HtmlEditorField_Readonly::create('PurchasePlusProductPayment_Note', _t('PurchasePlusProductPayment.NOTE', 'Note'), _t('PurchasePlusProductPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice()))),
            $purchaseAccountBalanceField = ReadonlyField::create('PurchasePlusProductPayment_ShowPlusPurchaseAccountBalance', _t('PurchasePlusProductPayment.CURRENT_PURCHASE_BALANCE', 'Current E-Purchase Balance'), Account::get('PurchaseAccount', $memberid)->obj('Balance')->Nice()),
            $purchaseAccountAmountField = NumericField::create('PurchasePlusProductPayment_PurchaseAmount', _t('PurchasePlusProductPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Purchase Payment Amount'), $purchase_payment)->setAttribute('data-inputmask-allowminus', 'false'),
            $productAccountBalanceField = ReadonlyField::create('PurchasePlusProductPayment_ShowPlusProductAccountBalance', _t('PurchasePlusProductPayment.CURRENT_PRODUCT_BALANCE', 'Current E-Product Balance'), Account::get('ProductAccount', $memberid)->obj('Balance')->Nice()),
            $productAccountAmountField = NumericField::create('PurchasePlusProductPayment_ProductAmount', _t('PurchasePlusProductPayment.PRODUCT_PAYMENT_AMOUNT', 'E-Product Payment Amount'), $product_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
	        $purchaseAccountAmountField->setAttribute('readonly', 'readonly');
			$productAccountAmountField->setAttribute('readonly', 'readonly');
		}
		
		$purchaseAccountBalanceField->setIncludeHiddenField(true);
		$productAccountBalanceField->setIncludeHiddenField(true);
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusProductPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['PurchasePlusProductPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusProductPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusProductPayment_PurchaseAmount'], $data['PurchasePlusProductPayment_Amount'] * $this->config()->purchase_min_percentage) < 0 || bccomp($data['PurchasePlusProductPayment_PurchaseAmount'], $data['PurchasePlusProductPayment_Amount'] * $this->config()->purchase_max_percentage) > 0){
            $validator->validationError(
                'PurchasePlusProductPayment_PurchaseAmount',
                _t('PurchasePlusProductPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusProductPayment_PurchaseAmount'] + $data['PurchasePlusProductPayment_ProductAmount'], $data['PurchasePlusProductPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusProductPayment.INVALID_PAYMENT_AMOUNT', 'E-Purchase + E-Product payment amount is not tally with total payment amount'),
                'warning'
            );
        }
		else{
			if(bccomp(Account::get('PurchaseAccount', $data['MemberID'])->Balance, $data['PurchasePlusProductPayment_PurchaseAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusProductPayment_PurchaseAmount',
	                _t('PurchasePlusProductPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('PurchaseAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
			if(bccomp(Account::get('ProductAccount', $data['MemberID'])->Balance, $data['PurchasePlusProductPayment_ProductAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusProductPayment_ProductAmount',
	                _t('PurchasePlusProductPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('ProductAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->PurchaseAmount > 0 && !$this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforePurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->PurchaseAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterPurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$this->PurchaseAccounts()->add($id);
			}

			if($this->ProductAmount > 0 && !$this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->ProductAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$this->ProductAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count())){			
			foreach($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $statement->MemberID);
				$this->PurchaseAccounts()->add($id);
			}
			
			foreach($this->ProductAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $statement->MemberID);
				$this->ProductAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance >= $this->PurchaseAmount) && (Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance >= $this->ProductAmount);
    }
}

?>