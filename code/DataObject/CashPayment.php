<?php

Class CashPayment extends Payment {
    private static $singular_name = "E-Cash Payment";
    private static $plural_name = "E-Cash Payments";
    
    private static $db = array(
        'BeforeBalance' => 'Currency',
        'AfterBalance' => 'Currency'
    );
	
	private static $many_many = array(
		'CashAccounts' => 'CashAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforeBalance'] = _t('CashPayment.BEFORE_BALANCE', 'Before Balance');
		$labels['AfterBalance'] = _t('CashPayment.AFTER_BALANCE', 'After Balance');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'CashPayment_Amount' => $this->Amount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

        $fields->makeFieldReadonly('BeforeBalance');
        $fields->makeFieldReadonly('AfterBalance');
		$fields->removeByName('CashAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
        $fields = FieldList::create(
            $cashAccountBalanceField = ReadonlyField::create('CashPayment_ShowCashAccountBalance', _t('CashPayment.CURRENT_BALANCE', 'Current Balance'), Account::get('CashAccount', $memberid)->obj('Balance')->Nice())
        );
		
		$cashAccountBalanceField->setIncludeHiddenField(true);
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('CashPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['CashPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('CashPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
        else if(bccomp(Account::get('CashAccount', $data['MemberID'])->Balance, $data['CashPayment_Amount']) < 0){
            $validator->validationError(
                'CashPayment_ShowCashAccountBalance',
                _t('CashPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient cash account balance', array('balance' => Account::get('CashAccount', $data['MemberID'])->obj('Balance')->Nice())),
                'warning'
            );
        }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();
		
		if($this->Amount > 0 && $this->isChanged('Status') && $this->Status == 'Success' && !$this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
            $this->setField('BeforeBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
            $statement_data = array(
                'Type' => 'Purchase',
                'Debit' => $this->Amount,
                'Reference' => $this->Receipt()->Reference,
                'Description' => $this->Receipt()->TransactionType()->Description
            );
            $id = CashAccount::create_statement($statement_data, $this->Receipt()->MemberID);
            $this->setField('AfterBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
			$this->CashAccounts()->add($id);
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && $this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
			foreach($this->CashAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $statement->MemberID);
				$this->CashAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
	function checkBalance(){
		return Account::get('CashAccount', $this->Receipt()->MemberID)->Balance >= $this->Amount;
	}
}

?>