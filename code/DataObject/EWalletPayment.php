<?php

Class EWalletPayment extends Payment {
    private static $singular_name = "EWallet Payment";
    private static $plural_name = "EWallet Payments";
    // manual | auto
    private static $type = 'manual';
	private static $purchase_percentage_range = 0.1;
	private static $purchase_min_percentage = 0.5;
	private static $purchase_max_percentage = 1;
	private static $cash_percentage_range = 0.1;
	private static $cash_min_percentage = 0;
	private static $cash_max_percentage = 0.5;
	private static $bonus_percentage_range = 0.1;
	private static $bonus_min_percentage = 0;
	private static $bonus_max_percentage = 0.5;
	private static $upgrade_percentage_range = 0.1;
	private static $upgrade_min_percentage = 0;
	private static $upgrade_max_percentage = 0.5;
	private static $product_percentage_range = 0.1;
	private static $product_min_percentage = 0;
	private static $product_max_percentage = 0.5;
    private static $db = array(
    	'BeforePurchaseBalance' => 'Currency',
        'AfterPurchaseBalance' => 'Currency',
        'PurchaseAmount'  => 'Currency',
        'BeforeCashBalance' => 'Currency',
        'AfterCashBalance' => 'Currency',
        'CashAmount'  => 'Currency',
        'BeforeBonusBalance' => 'Currency',
        'AfterBonusBalance' => 'Currency',
        'BonusAmount'  => 'Currency',
        'BeforeUpgradeBalance' => 'Currency',
        'AfterUpgradeBalance' => 'Currency',
        'UpgradeAmount'  => 'Currency',
        'BeforeProductBalance' => 'Currency',
        'AfterProductBalance' => 'Currency',
        'ProductAmount' => 'Currency'
    );
	
	private static $many_many = array(
		'PurchaseAccounts' => 'PurchaseAccount',
		'CashAccounts' => 'CashAccount',
		'BonusAccounts' => 'BonusAccount',
		'UpgradeAccounts' => 'UpgradeAccount',
		'ProductAccounts' => 'ProductAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforePurchaseBalance'] = _t('EWalletPayment.PURCHASE_BEFORE_BALANCE', 'E-Purchase Before Balance');
		$labels['AfterPurchaseBalance'] = _t('EWalletPayment.PURCHASE_AFTER_BALANCE', 'E-Purchase After Balance');
		$labels['PurchaseAmount'] = _t('EWalletPayment.PURCHASE_PAID_AMOUNT', 'E-Purchase Paid Amount');
		$labels['BeforeCashBalance'] = _t('EWalletPayment.CASH_BEFORE_BALANCE', 'E-Cash Before Balance');
		$labels['AfterCashBalance'] = _t('EWalletPayment.CASH_AFTER_BALANCE', 'E-Cash After Balance');
		$labels['CashAmount'] = _t('EWalletPayment.CASH_PAID_AMOUNT', 'E-Cash Paid Amount');
		$labels['BeforeBonusBalance'] = _t('EWalletPayment.BONUS_BEFORE_BALANCE', 'E-Bonus Before Balance');
		$labels['BeforeBonusBalance'] = _t('EWalletPayment.BONUS_AFTER_BALANCE', 'E-Bonus After Balance');
		$labels['BonusAmount'] = _t('EWalletPayment.BONUS_PAID_AMOUNT', 'E-Bonus Paid Amount');
		$labels['BeforeBonusBalance'] = _t('EWalletPayment.UPGRADE_BEFORE_BALANCE', 'E-Upgrade Before Balance');
		$labels['BeforeBonusBalance'] = _t('EWalletPayment.UPGRADE_AFTER_BALANCE', 'E-Upgrade After Balance');
		$labels['BonusAmount'] = _t('EWalletPayment.UPGRADE_PAID_AMOUNT', 'E-Upgrade Paid Amount');
		$labels['BeforeProductBalance'] = _t('EWalletPayment.PRODUCT_BEFORE_BALANCE', 'E-Product Before Balance');
		$labels['AfterProductBalance'] = _t('EWalletPayment.PRODUCT_AFTER_BALANCE', 'E-Product After Balance');
		$labels['ProductAmount'] = _t('EWalletPayment.PRODUCT_PAID_AMOUNT', 'E-Product Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'EWalletPayment_Amount' => $this->Amount,
				'EWalletPayment_PurchaseAmount' => $this->PurchaseAmount,
				'EWalletPayment_CashAmount' => $this->CashAmount,
				'EWalletPayment_BonusAmount' => $this->BonusAmount,
				'EWalletPayment_UpgradeAmount' => $this->UpgradeAmount,
				'EWalletPayment_ProductAmount' => $this->ProductAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforePurchaseBalance');
        $fields->makeFieldReadonly('AfterPurchaseBalance');
        $fields->makeFieldReadonly('BeforeCashBalance');
        $fields->makeFieldReadonly('AfterCashBalance');
		$fields->makeFieldReadonly('BeforeBonusBalance');
        $fields->makeFieldReadonly('AfterBonusBalance');
		$fields->makeFieldReadonly('BeforeUpgradeBalance');
        $fields->makeFieldReadonly('AfterUpgradeBalance');
		$fields->makeFieldReadonly('BeforeProductBalance');
        $fields->makeFieldReadonly('AfterProductBalance');
		$fields->removeByName('PurchaseAccounts');
		$fields->removeByName('CashAccounts');
		$fields->removeByName('BonusAccounts');
		$fields->removeByName('UpgradeAccounts');
		$fields->removeByName('ProductAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	if($this->config()->get('type') == 'auto'){
    		Requirements::javascript('account/javascript/EWalletPayment.min.js');
			
    		$fields = FieldList::create();
			
			if($this->config()->purchase_min_percentage >= 0 && $this->config()->purchase_max_percentage > 0 && $this->config()->purchase_percentage_range > 0){
				$source = array();
				$amount = null;
				for($i = $this->config()->purchase_min_percentage, $j = $this->config()->purchase_percentage_range; $i <= $this->config()->purchase_max_percentage; $i += $j){
					if($amount === null){
						$amount = $this->Amount * $i;
					}
					$source[str_replace('.', '_', $i)] = DBField::create_field('Percentage', $i)->Nice();
				}
				$fields->push(DropdownField::create('EWalletPayment_PurchasePercentage', _t('EWalletPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Purchase Payment Amount'), $source)->setAttribute('rel', 'purchase-percentage'));
				$fields->push(HiddenField::create('EWalletPayment_PurchaseAmount', null, $amount)->setAttribute('rel', 'purchase-amount'));
				
			}
			
			if($this->config()->cash_min_percentage >= 0 && $this->config()->cash_max_percentage > 0 && $this->config()->cash_percentage_range > 0){
				$source = array();
				$amount = null;
				for($i = $this->config()->cash_min_percentage, $j = $this->config()->cash_percentage_range; $i <= $this->config()->cash_max_percentage; $i += $j){
					if($amount === null){
						$amount = $this->Amount * $i;
					}
					$source[str_replace('.', '_', $i)] = DBField::create_field('Percentage', $i)->Nice();
				}
				$fields->push(DropdownField::create('EWalletPayment_CashPercentage', _t('EWalletPayment.CASH_PAYMENT_AMOUNT', 'E-Cash Payment Amount'), $source)->setAttribute('rel', 'cash-percentage'));
				$fields->push(HiddenField::create('EWalletPayment_CashAmount', null, $amount)->setAttribute('rel', 'cash-amount'));
			}
			
			if($this->config()->bonus_min_percentage >= 0 && $this->config()->bonus_max_percentage > 0 && $this->config()->bonus_percentage_range > 0){
				$source = array();
				$amount = null;
				for($i = $this->config()->bonus_min_percentage, $j = $this->config()->bonus_percentage_range; $i <= $this->config()->bonus_max_percentage; $i += $j){
					if($amount === null){
						$amount = $this->Amount * $i;
					}
					$source[str_replace('.', '_', $i)] = DBField::create_field('Percentage', $i)->Nice();
				}
				$fields->push(DropdownField::create('EWalletPayment_BonusPercentage', _t('EWalletPayment.BONUS_PAYMENT_AMOUNT', 'E-Bonus Payment Amount'), $source)->setAttribute('rel', 'bonus-percentage'));
				$fields->push(HiddenField::create('EWalletPayment_BonusAmount', null, $amount)->setAttribute('rel', 'bonus-amount'));
			}
			
			if($this->config()->upgrade_min_percentage >= 0 && $this->config()->upgrade_max_percentage > 0 && $this->config()->upgrade_percentage_range > 0){
				$source = array();
				$amount = null;
				for($i = $this->config()->upgrade_min_percentage, $j = $this->config()->upgrade_percentage_range; $i <= $this->config()->upgrade_max_percentage; $i += $j){
					if($amount === null){
						$amount = $this->Amount * $i;
					}
					$source[str_replace('.', '_', $i)] = DBField::create_field('Percentage', $i)->Nice();
				}
				$fields->push(DropdownField::create('EWalletPayment_UpgradePercentage', _t('EWalletPayment.UPGRADE_PAYMENT_AMOUNT', 'E-Upgrade Payment Amount'), $source)->setAttribute('rel', 'upgrade-percentage'));
				$fields->push(HiddenField::create('EWalletPayment_UpgradeAmount', null, $amount)->setAttribute('rel', 'upgrade-amount'));
			}
			
			if($this->config()->product_min_percentage >= 0 && $this->config()->product_max_percentage > 0 && $this->config()->product_percentage_range > 0){
				$source = array();
				$amount = null;
				for($i = $this->config()->product_min_percentage, $j = $this->config()->product_percentage_range; $i <= $this->config()->product_max_percentage; $i += $j){
					if($amount === null){
						$amount = $this->Amount * $i;
					}
					$source[str_replace('.', '_', $i)] = DBField::create_field('Percentage', $i)->Nice();
				}
				$fields->push(DropdownField::create('EWalletPayment_ProductPercentage', _t('EWalletPayment.PRODUCT_PAYMENT_AMOUNT', 'E-Product Payment Amount'), $source)->setAttribute('rel', 'product-percentage'));
				$fields->push(HiddenField::create('EWalletPayment_ProductAmount', null, $amount)->setAttribute('rel', 'product-amount'));
			}
		}
		else{
			$fields = FieldList::create();
			
			if($this->config()->purchase_max_percentage > 0){
				$fields->push(NumericField::create('EWalletPayment_PurchaseAmount', _t('EWalletPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Purchase Payment Amount'))->setAttribute('data-inputmask-allowminus', 'false'));
			}

			if($this->config()->cash_max_percentage > 0){
				$fields->push(NumericField::create('EWalletPayment_CashAmount', _t('EWalletPayment.CASH_PAYMENT_AMOUNT', 'E-Cash Payment Amount'))->setAttribute('data-inputmask-allowminus', 'false'));
			}
			
			if($this->config()->bonus_max_percentage > 0){
				$fields->push(NumericField::create('EWalletPayment_BonusAmount', _t('EWalletPayment.BONUS_PAYMENT_AMOUNT', 'E-Bonus Payment Amount'))->setAttribute('data-inputmask-allowminus', 'false'));
			}
			
			if($this->config()->upgrade_max_percentage > 0){
				$fields->push(NumericField::create('EWalletPayment_UpgradeAmount', _t('EWalletPayment.UPGRADE_PAYMENT_AMOUNT', 'E-Upgrade Payment Amount'))->setAttribute('data-inputmask-allowminus', 'false'));
			}
			
			if($this->config()->product_max_percentage > 0){
				$fields->push(NumericField::create('EWalletPayment_ProductAmount', _t('EWalletPayment.PRODUCT_PAYMENT_AMOUNT', 'E-Product Payment Amount'))->setAttribute('data-inputmask-allowminus', 'false'));
			}
        }
			
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
    	$purchase = isset($data['EWalletPayment_PurchaseAmount']) ? $data['EWalletPayment_PurchaseAmount'] : 0;
		$cash = isset($data['EWalletPayment_CashAmount']) ? $data['EWalletPayment_CashAmount'] : 0;
		$bonus = isset($data['EWalletPayment_BonusAmount']) ? $data['EWalletPayment_BonusAmount'] : 0;
		$upgrade = isset($data['EWalletPayment_UpgradeAmount']) ? $data['EWalletPayment_UpgradeAmount'] : 0;
		$product = isset($data['EWalletPayment_ProductAmount']) ? $data['EWalletPayment_ProductAmount'] : 0;
		
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('EWalletPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['EWalletPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('EWalletPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($purchase + $cash + $bonus + $upgrade + $product, $data['EWalletPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('EWalletPayment.INVALID_PAYMENT_AMOUNT', 'EWallet payment amount is not tally with total payment amount'),
                'warning'
            );
        }
        else {
        	if($purchase > 0){
	            if(bccomp($purchase, $data['EWalletPayment_Amount'] * $this->config()->purchase_min_percentage) < 0 || bccomp($purchase, $data['EWalletPayment_Amount'] * $this->config()->purchase_max_percentage) > 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_PurchasePercentage' : 'EWalletPayment_PurchaseAmount',
		                _t('EWalletPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('purchase_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('purchase_max_percentage'))->Nice())),
		                'warning'
		            );
		        }
				else if(bccomp(Account::get('PurchaseAccount', $data['MemberID'])->Balance, $purchase) < 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_PurchasePercentage' : 'EWalletPayment_PurchaseAmount',
		                _t('EWalletPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('PurchaseAccount', $data['MemberID'])->obj('Balance')->Nice())),
		                'warning'
		            );
		        }
	        }

			if($cash > 0){
				if(bccomp($cash, $data['EWalletPayment_Amount'] * $this->config()->cash_min_percentage) < 0 || bccomp($cash, $data['EWalletPayment_Amount'] * $this->config()->cash_max_percentage) > 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_CashPercentage' : 'EWalletPayment_CashAmount',
		                _t('EWalletPayment.INVALID_CASH_AMOUNT', 'E-Cash payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_max_percentage'))->Nice())),
		                'warning'
		            );
		        }
				else if(bccomp(Account::get('CashAccount', $data['MemberID'])->Balance, $cash) < 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_CashPercentage' : 'EWalletPayment_CashAmount',
		                _t('EWalletPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('CashAccount', $data['MemberID'])->obj('Balance')->Nice())),
		                'warning'
		            );
		        }
	        }

			if($bonus > 0){
				if(bccomp($bonus, $data['EWalletPayment_Amount'] * $this->config()->bonus_min_percentage) < 0 || bccomp($bonus, $data['EWalletPayment_Amount'] * $this->config()->bonus_max_percentage) > 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_BonusPercentage' : 'EWalletPayment_BonusAmount',
		                _t('EWalletPayment.INVALID_BONUS_AMOUNT', 'E-Bonus payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('bonus_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('bonus_max_percentage'))->Nice())),
		                'warning'
		            );
		        }
				else if(bccomp(Account::get('BonusAccount', $data['MemberID'])->Balance, $bonus) < 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_BonusPercentage' : 'EWalletPayment_BonusAmount',
		                _t('EWalletPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('BonusAccount', $data['MemberID'])->obj('Balance')->Nice())),
		                'warning'
		            );
		        }
	        }

			if($upgrade > 0){
				if(bccomp($upgrade, $data['EWalletPayment_Amount'] * $this->config()->upgrade_min_percentage) < 0 || bccomp($upgrade, $data['EWalletPayment_Amount'] * $this->config()->upgrade_max_percentage) > 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_UpgradePercentage' : 'EWalletPayment_UpgradeAmount',
		                _t('EWalletPayment.INVALID_UPGRADE_AMOUNT', 'E-Upgrade payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('upgrade_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('upgrade_max_percentage'))->Nice())),
		                'warning'
		            );
		        }
				else if(bccomp(Account::get('UpgradeAccount', $data['MemberID'])->Balance, $upgrade) < 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_UpgradePercentage' : 'EWalletPayment_UpgradeAmount',
		                _t('EWalletPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('UpgradeAccount', $data['MemberID'])->obj('Balance')->Nice())),
		                'warning'
		            );
		        }
			}

			if($product > 0){
				if(bccomp($product, $data['EWalletPayment_Amount'] * $this->config()->product_min_percentage) < 0 || bccomp($product, $data['EWalletPayment_Amount'] * $this->config()->product_max_percentage) > 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_ProductPercentage' : 'EWalletPayment_ProductAmount',
		                _t('EWalletPayment.INVALID_PRODUCT_AMOUNT', 'E-Product payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('product_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('product_max_percentage'))->Nice())),
		                'warning'
		            );
		        }
				else if(bccomp(Account::get('ProductAccount', $data['MemberID'])->Balance, $product) < 0){
		            $validator->validationError(
		                $this->config()->get('type') == 'auto' ? 'EWalletPayment_ProductPercentage' : 'EWalletPayment_ProductAmount',
		                _t('EWalletPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('ProductAccount', $data['MemberID'])->obj('Balance')->Nice())),
		                'warning'
		            );
		        }
	        }
	    }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->PurchaseAmount > 0 && !$this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforePurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->PurchaseAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterPurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$this->PurchaseAccounts()->add($id);
			}
			
			if($this->CashAmount > 0 && !$this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->CashAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
				$this->CashAccounts()->add($id);
			}
			
			if($this->BonusAmount > 0 && !$this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->BonusAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
				$this->BonusAccounts()->add($id);
			}
			
			if($this->UpgradeAmount > 0 && !$this->UpgradeAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeUpgradeBalance', Account::get('UpgradeAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->UpgradeAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = UpgradeAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterUpgradeBalance', Account::get('UpgradeAccount', $this->Receipt()->MemberID)->Balance);
				$this->UpgradeAccounts()->add($id);
			}
			
			if($this->ProductAmount > 0 && !$this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforeProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->ProductAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$this->ProductAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->CashAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->UpgradeAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count())){
			foreach($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $statement->MemberID);
				$this->PurchaseAccounts()->add($id);
			}
			
			foreach($this->CashAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $statement->MemberID);
				$this->CashAccounts()->add($id);
			}
			
			foreach($this->BonusAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $statement->MemberID);
				$this->BonusAccounts()->add($id);
			}
			
			foreach($this->UpgradeAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = UpgradeAccount::create_statement($statement_data, $statement->MemberID);
				$this->UpgradeAccounts()->add($id);
			}
			
			foreach($this->ProductAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $statement->MemberID);
				$this->ProductAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance >= $this->PurchaseAmount) && (Account::get('CashAccount', $this->Receipt()->MemberID)->Balance >= $this->CashAmount) && (Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance >= $this->BonusAmount) && (Account::get('UpgradeAccount', $this->Receipt()->MemberID)->Balance >= $this->UpgradeAmount) && (Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance >= $this->ProductAmount);
    }
}

?>