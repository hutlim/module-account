<?php

class BonusConvertUpgrade extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Convert E-Upgrade";
    private static $plural_name = "E-Bonus Convert E-Upgrade";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return BonusConvertUpgrade::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusConvertUpgrade');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusConvertUpgrade' => array(
                'name' => _t('BonusConvertUpgrade.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusConvertUpgrade.PERMISSIONS_CATEGORY', 'E-Bonus Convert E-Upgrade')
            )
        );
    }
}
?>