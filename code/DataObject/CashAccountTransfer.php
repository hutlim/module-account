<?php

class CashAccountTransfer extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Transfer";
    private static $plural_name = "E-Cash Transfers";
    
    private static $extensions = array("AccountTransfer");

	static function create_statement($data, $memberid, $transferid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        } else if(!$transferid) {
            throw new Exception("Empty transferid");
        }
		
        return CashAccountTransfer::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->setField('TransferID', $transferid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashAccountTransfer');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_CashAccountTransfer' => array(
                'name' => _t('CashAccountTransfer.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashAccountTransfer.PERMISSIONS_CATEGORY', 'E-Cash Transfer')
            )
        );
    }
}
?>