<?php

class ProductAccount extends DataObject {
    private static $singular_name = "E-Product Account";
    private static $plural_name = "E-Product Accounts";
    
    private static $extensions = array("Account");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return ProductAccount::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}
?>