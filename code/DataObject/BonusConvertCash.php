<?php

class BonusConvertCash extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Convert E-Cash";
    private static $plural_name = "E-Bonus Convert E-Cash";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return BonusConvertCash::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusConvertCash');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusConvertCash' => array(
                'name' => _t('BonusConvertCash.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusConvertCash.PERMISSIONS_CATEGORY', 'E-Bonus Convert E-Cash')
            )
        );
    }
}
?>