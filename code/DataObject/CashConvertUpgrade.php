<?php

class CashConvertUpgrade extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Convert E-Upgrade";
    private static $plural_name = "E-Cash Convert E-Upgrade";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return CashConvertUpgrade::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashConvertUpgrade');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_CashConvertUpgrade' => array(
                'name' => _t('CashConvertUpgrade.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashConvertUpgrade.PERMISSIONS_CATEGORY', 'E-Cash Convert E-Upgrade')
            )
        );
    }
}
?>