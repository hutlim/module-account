<?php
class AccountType extends DataObject {
    private static $singular_name = "Account Type";
    private static $plural_name = "Account Type";
    
    private static $db = array(
        'Code' => 'Varchar',
        'Title' => 'Varchar(250)',
        'Active' => 'Boolean',
        'Sort' => 'Int',
        'Locale' => 'Varchar'
    );

    private static $searchable_fields = array(
        'Code',
        'Title',
        'Active',
        'Locale'
    );

    private static $summary_fields = array(
        'Code',
        'Title',
        'Active.Nice',
        'Locale'
    );

    private static $default_sort = 'ClassName, Code, Sort';

    private static $defaults = array('Active' => true);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['Code'] = _t('AccountType.CODE', 'Code');
		$labels['Title'] = _t('AccountType.TITLE', 'Title');
		$labels['Active'] = _t('AccountType.IS_ACTIVE', 'Is Active?');
		$labels['Active.Nice'] = _t('AccountType.IS_ACTIVE', 'Is Active?');
		$labels['Locale'] = _t('AccountType.LOCALE', 'Locale');
		
		return $labels;	
	}

    /**
     * Set custom validation for dropdown list form
     *
     * @return Validator
     */
    function getCMSValidator() {
        return AccountType_Validator::create();
    }

    /**
     * Set not allow to edit default code & title
     *
     * @return FieldSet
     */
    function getCMSFields() {
        $fields = parent::getCMSFields();

        if($this->exists()) {
            $fields->makeFieldReadonly('Code');
			$fields->dataFieldByName('Code')->setIncludeHiddenField(true);
        }
		
		if(class_exists('Translatable')){
			$locale_list = array();
			$allowed_locale = Translatable::get_allowed_locales();
			$locale = i18n::get_common_locales();
			foreach($allowed_locale as $code){
				if(isset($locale[$code])){
					$locale_list[$code] = $locale[$code];
				}
			}
		}
		else {
			$locale_list = i18n::get_common_locales();
		}
		$fields->replaceField('Locale', DropdownField::create('Locale', $this->fieldLabel('Locale'), $locale_list));
		
		$fields->removeByName('Sort');

        return $fields;
    }

    function map($locale = null) {
    	$sourceClass = $this->class;
		if(!$locale) $locale = i18n::get_locale();
        return $sourceClass::get()->filter('Locale', $locale)->filter('Active', 1)->map('Code', 'Title');
    }

    function getDropdownField($name, $title) {
        return DropdownField::create($name, $title)->setSource($this->map());
    }

	function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check(sprintf('VIEW_%s', $this->class));
    }

    function canEdit($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check(sprintf('EDIT_%s', $this->class));
    }

    function canDelete($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check(sprintf('DELETE_%s', $this->class));
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check(sprintf('CREATE_%s', $this->class));
    }

    public function providePermissions() {
    	if($this->class == 'AccountType') return array();
		
        return array(
            sprintf('VIEW_%s', $this->class) => array(
                'name' => _t('AccountType.PERMISSION_VIEW', 'Allow view access right'),
                'category' => $this->i18n_singular_name()
            ),
            sprintf('EDIT_%s', $this->class) => array(
                'name' => _t('AccountType.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => $this->i18n_singular_name()
            ),
            sprintf('DELETE_%s', $this->class) => array(
                'name' => _t('AccountType.PERMISSION_DELETE', 'Allow delete access right'),
                'category' => $this->i18n_singular_name()
            ),
            sprintf('CREATE_%s', $this->class) => array(
                'name' => _t('AccountType.PERMISSION_CREATE', 'Allow create access right'),
                'category' => $this->i18n_singular_name()
            )
        );
    }
}

class AccountType_Validator extends RequiredFields {
    protected $customRequired = array(
        'Code',
        'Title'
    );

    /**
     * Constructor
     */
    public function __construct() {
        $required = func_get_args();
        if(isset($required[0]) && is_array($required[0])) {
            $required = $required[0];
        }
        $required = array_merge($required, $this->customRequired);

        parent::__construct($required);
    }

    /**
     * Check if the submitted list data is valid (server-side)
     *
     * @param array $data Submitted data
     * @return bool Returns TRUE if the submitted data is valid, otherwise
     *              FALSE.
     */
    function php($data) {
        $valid = parent::php($data);

		$record = $this->form->getRecord();

        if(!$record->exists()) {
            $sourceClass = $record->ClassName;
			$obj = $sourceClass::get()->filter('Code', $data['Code'])->filter('Locale', $data['Locale']);
	
	        if($obj->count()) {
	            $this->validationError('Code', _t('AccountType.DUPLICATE_CODE', 'Sorry, this code already exists'), 'validation');
	            $valid = false;
	        }
		}

        // Execute the validators on the extensions
        if($this->extension_instances) {
            foreach($this->extension_instances as $extension) {
                if(method_exists($extension, 'hasMethod') && $extension->hasMethod('updatePHP')) {
                    $valid &= $extension->updatePHP($data, $this->form);
                }
            }
        }

        return $valid;
    }
}

class BonusAccountType extends AccountType {
    private static $singular_name = "E-Bonus Account Type";
    private static $plural_name = "E-Bonus Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Purchase',
            'Title' => 'Purchase',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => 'Bonus',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Withdrawal',
            'Title' => 'Withdrawal',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => 'Transfer',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Withdrawal Fee',
            'Title' => 'Withdrawal Fee',
            'Sort' => 70,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Purchase',
            'Title' => '购买',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => '佣金',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Withdrawal',
            'Title' => '余额兑现',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => '余额转账',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 130,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Withdrawal Fee',
            'Title' => '余额兑现处理费',
            'Sort' => 140,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = BonusAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}

class CashAccountType extends AccountType {
    private static $singular_name = "E-Cash Account Type";
    private static $plural_name = "E-Cash Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Purchase',
            'Title' => 'Purchase',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => 'Bonus',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Withdrawal',
            'Title' => 'Withdrawal',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => 'Transfer',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Withdrawal Fee',
            'Title' => 'Withdrawal Fee',
            'Sort' => 70,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Purchase',
            'Title' => '购买',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => '佣金',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Withdrawal',
            'Title' => '余额兑现',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => '余额转账',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 130,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Withdrawal Fee',
            'Title' => '余额兑现处理费',
            'Sort' => 140,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = CashAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}

class ProductAccountType extends AccountType {
    private static $singular_name = "E-Product Account Type";
    private static $plural_name = "E-Product Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Purchase',
            'Title' => 'Purchase',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => 'Transfer',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Purchase',
            'Title' => '购买',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => '余额转账',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = ProductAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}

class PurchaseAccountType extends AccountType {
    private static $singular_name = "E-Purchase Account Type";
    private static $plural_name = "E-Purchase Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Purchase',
            'Title' => 'Purchase',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => 'Bonus',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Deposit',
            'Title' => 'Deposit',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => 'Transfer',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Purchase',
            'Title' => '购买',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => '佣金',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Deposit',
            'Title' => '在线充值',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Transfer',
            'Title' => '余额转账',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = PurchaseAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}

class UpgradeAccountType extends AccountType {
    private static $singular_name = "E-Upgrade Account Type";
    private static $plural_name = "E-Upgrade Account Type";
    
    private static $default_records = array(
        array(
            'Code' => 'Purchase',
            'Title' => 'Purchase',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => 'Bonus',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => 'Adjustment',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Convert',
            'Title' => 'Convert',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Purchase',
            'Title' => '购买',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Bonus',
            'Title' => '佣金',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Adjustment',
            'Title' => '余额调整',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Convert',
            'Title' => '余额兑换',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($type = UpgradeAccountType::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $type->Title;
        }
		return $code;
    }
}
?>
