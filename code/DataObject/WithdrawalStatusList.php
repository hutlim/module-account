<?php
class WithdrawalStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Withdrawal Status List";
    private static $plural_name = "Withdrawal Status Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Pending',
            'Title' => 'Pending',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Approved',
            'Title' => 'Approved',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Rejected',
            'Title' => 'Rejected',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Canceled',
            'Title' => 'Canceled',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Pending',
            'Title' => '等待处理',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Approved',
            'Title' => '已批准',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Rejected',
            'Title' => '已拒绝',
            'Sort' => 70,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Canceled',
            'Title' => '已取消',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($obj = WithdrawalStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $obj->Title;
        }
		return $code;
    }
}
?>
