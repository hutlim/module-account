<?php

class CashAccountTransferPurchaseAccount extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Transfer E-Purchase";
    private static $plural_name = "E-Cash Transfer E-Purchase";
    
    private static $extensions = array("AccountTransfer");

	static function create_statement($data, $memberid, $transferid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        } else if(!$transferid) {
            throw new Exception("Empty transferid");
        }
		
        return CashAccountTransferPurchaseAccount::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->setField('TransferID', $transferid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashAccountTransferPurchaseAccount');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_CashAccountTransferPurchaseAccount' => array(
                'name' => _t('CashAccountTransferPurchaseAccount.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashAccountTransferPurchaseAccount.PERMISSIONS_CATEGORY', 'E-Cash Transfer E-Purchase')
            )
        );
    }
}
?>