<?php

class UpgradeAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Upgrade Adjustment";
    private static $plural_name = "E-Upgrade Adjustments";
    
    private static $extensions = array("AccountAdjustment");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
        return UpgradeAccountAdjustment::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }
    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_UpgradeAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_UpgradeAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_UpgradeAccountAdjustment' => array(
                'name' => _t('UpgradeAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('UpgradeAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Upgrade Adjustment')
            ),
            'CREATE_UpgradeAccountAdjustment' => array(
                'name' => _t('UpgradeAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('UpgradeAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Upgrade Adjustment')
            )
        );
    }
}
?>