<?php

class Voucher extends DataObject implements PermissionProvider {
    private static $singular_name = "Voucher";
    private static $plural_name = "Vouchers";

    private static $db = array(
        'Code' => 'Varchar',
        'Amount' => 'Currency',
        'Status' => "Dropdown('VoucherStatusList')",
        'Remark' => 'Varchar(250)'
    );
	
	private static $defaults = array(
		'Status' => 'Available'
	);
	
	private static $has_one = array(
		'Member' => 'Member',
		'IssuedBy' => 'Member',
		'UsedBy' => 'Member'
	);
	
	private static $default_sort = 'Created DESC';

    private static $searchable_fields = array(
    	'Created' => array(
			'field' => 'DateField',
			'filter' => 'DateMatchFilter'
		),
		'Member.Username',
        'Code',
        'Amount' => array(
            'filter' => 'GreaterThanOrEqualFilter'
        ),
        'Status',
        'UsedBy.Username',
        'IssuedBy.Username'
    );

    private static $summary_fields = array(
    	'Created.Nice',
    	'Member.Username',
        'Code',
        'Amount.Nice',
        'Status.Title',
        'Remark',
        'UsedBy.Username',
        'IssuedBy.Username'
    );

    static function code_generator() {
        return str_pad(uniqid(rand()), 25, rand(), STR_PAD_LEFT);
    }

    public function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Created'] = _t('Voucher.CREATED_ON', 'Created On');
        $labels['Created.Nice'] = _t('Voucher.CREATED_ON', 'Created On');
        $labels['Code'] = _t('Voucher.CODE', 'Code');
		$labels['Amount'] = _t('Voucher.AMOUNT', 'Amount');
		$labels['Amount.Nice'] = _t('Voucher.AMOUNT', 'Amount');
        $labels['Status'] = _t('Voucher.STATUS', 'Status');
        $labels['Status.Title'] = _t('Voucher.STATUS', 'Status');
		$labels['Remark'] = _t('Voucher.REMARK', 'Remark');
        $labels['Member.Username'] = _t('Voucher.USERNAME', 'Username');
        $labels['UsedBy.Username'] = _t('Voucher.USED_BY', 'Used By');
		$labels['IssuedBy.Username'] = _t('Voucher.ISSUED_BY', 'Issued By');
        return $labels;
    }

	function validate() {
		$validationResult = parent::validate();
		
		if($this->SetUsername && !$this->MemberID){
			$this->MemberID = Distributor::get_id_by_username($this->SetUsername);
		}
		
		if(!$this->MemberID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('Voucher.INVALID_MEMBER_ID', 'Invalid member id'), 'INVALID_MEMBER_ID');
            $validationResult->combineAnd($subvalid);
        }
        
        if(!$this->ID && $this->Amount <= 0){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('Voucher.INVALID_VOUCHER_AMOUNT', 'Invalid voucher amount'), "INVALID_VOUCHER_AMOUNT");
            $validationResult->combineAnd($subvalid);
		}

        return $validationResult;
    }

	function getCMSFields() {
		$fields = parent::getCMSFields();

		if($this->exists()) {
			$fields->makeFieldReadonly('Code');
            $fields->makeFieldReadonly('Amount');
			$fields->makeFieldReadonly('Status');
            $fields->makeFieldReadonly('Remark');
            $fields->makeFieldReadonly('MemberID');
			$fields->makeFieldReadonly('UsedByID');
			$fields->makeFieldReadonly('IssuedByID');
        } else {
            $fields->insertBefore(UsernameField::create('SetUsername', _t('Voucher.USERNAME', 'Username')), 'Amount');
			$fields->removeByName('Code');
			$fields->removeByName('Status');
            $fields->removeByName('MemberID');
            $fields->removeByName('UsedByID');
            $fields->removeByName('IssuedByID');
        }

		return $fields;
	}
	
	function onBeforeWrite(){
		parent::onBeforeWrite();
		
		if(!$this->Code){
			$this->Code = self::code_generator();
		}
		
		if($this->MemberID == '' && $this->SetUsername != '') {
            $this->MemberID = Distributor::get_id_by_username($this->SetUsername);
        }
		
		if(!$this->exists()){
			$this->IssuedByID = Member::currentUserID();
		}
	}
	
	function getTitle(){
		return sprintf('%s (%s)', $this->Code, $this->dbObject('Status')->Title());
	}

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_Voucher');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('EDIT_Voucher');
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_Voucher');
    }
	
	function canCancel($member = null) {
        return Permission::check('CANCEL_Voucher') && $this->Status == 'Available';
    }

    public function providePermissions() {
        return array(
            'VIEW_Voucher' => array(
                'name' => _t('Voucher.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('Voucher.PERMISSIONS_CATEGORY', 'Voucher')
            ),
            'EDIT_Voucher' => array(
                'name' => _t('Voucher.PERMISSION_EDIT', 'Allow edit access right'),
                'category' => _t('Voucher.PERMISSIONS_CATEGORY', 'Voucher')
            ),
            'CREATE_Voucher' => array(
                'name' => _t('Voucher.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('Voucher.PERMISSIONS_CATEGORY', 'Voucher')
            ),
            'CANCEL_Voucher' => array(
                'name' => _t('Voucher.PERMISSION_CANCEL', 'Allow cancel access right'),
                'category' => _t('Voucher.PERMISSIONS_CATEGORY', 'Voucher')
            )
        );
    }

}
?>