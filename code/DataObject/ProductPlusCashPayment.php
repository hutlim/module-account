<?php

Class ProductPlusCashPayment extends Payment {
    private static $singular_name = "E-Product + E-Cash Payment";
    private static $plural_name = "E-Product + E-Cash Payments";
    // manual | auto
    private static $type = 'manual';
	private static $product_min_percentage = 0.5;
	private static $product_max_percentage = 1;
	private static $priority_payment = 'ProductAccount';
    private static $db = array(
        'BeforeProductBalance' => 'Currency',
        'AfterProductBalance' => 'Currency',
        'ProductAmount' => 'Currency',
        'BeforeCashBalance' => 'Currency',
        'AfterCashBalance' => 'Currency',
        'CashAmount'  => 'Currency'
    );
	
	private static $many_many = array(
		'ProductAccounts' => 'ProductAccount',
		'CashAccounts' => 'CashAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforeProductBalance'] = _t('ProductPlusCashPayment.PRODUCT_BEFORE_BALANCE', 'E-Product Before Balance');
		$labels['AfterProductBalance'] = _t('ProductPlusCashPayment.PRODUCT_AFTER_BALANCE', 'E-Product After Balance');
		$labels['ProductAmount'] = _t('ProductPlusCashPayment.PRODUCT_PAID_AMOUNT', 'E-Product Paid Amount');
		$labels['BeforeCashBalance'] = _t('ProductPlusCashPayment.CASH_BEFORE_BALANCE', 'E-Cash Before Balance');
		$labels['BeforeCashBalance'] = _t('ProductPlusCashPayment.CASH_AFTER_BALANCE', 'E-Cash After Balance');
		$labels['CashAmount'] = _t('ProductPlusCashPayment.CASH_PAID_AMOUNT', 'E-Cash Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'ProductPlusCashPayment_Amount' => $this->Amount,
				'ProductPlusCashPayment_ProductAmount' => $this->ProductAmount,
				'ProductPlusCashPayment_CashAmount' => $this->CashAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforeProductBalance');
        $fields->makeFieldReadonly('AfterProductBalance');
        $fields->makeFieldReadonly('BeforeCashBalance');
        $fields->makeFieldReadonly('AfterCashBalance');
		$fields->removeByName('ProductAccounts');
		$fields->removeByName('CashAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$product_payment = $this->Amount * $this->config()->get('product_min_percentage');
    	$cash_payment = $this->Amount * (1 - $this->config()->get('product_min_percentage'));
    	if($this->config()->get('priority_payment') == 'ProductAccount'){
			$product_balance = Account::get('ProductAccount', $memberid)->Balance;
			if($product_balance > $product_payment){
		    	if($this->Amount * $this->config()->get('product_max_percentage') < $product_balance){
					$product_payment = $this->Amount * $this->config()->get('product_max_percentage');
				}
				else{
					$product_payment = $product_balance;
				}
			}

			$cash_payment = $this->Amount - $product_payment;
		}
		else{
			$cash_balance = Account::get('CashAccount', $memberid)->Balance;
	    	if($cash_balance < $cash_payment){
	    		if($cash_balance < $this->Amount * (1 - $this->config()->get('product_max_percentage'))){
	    			$cash_payment = $this->Amount * (1 - $this->config()->get('product_max_percentage'));
				}
				else{
					$cash_payment = $cash_balance;
				}
			}

			$product_payment = $this->Amount - $cash_payment;
		}
		
		$fields = FieldList::create(
        	HtmlEditorField_Readonly::create('ProductPlusCashPayment_Note', _t('ProductPlusCashPayment.NOTE', 'Note'), _t('ProductPlusCashPayment.INVALID_PRODUCT_AMOUNT', 'E-Product payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->product_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->product_max_percentage)->Nice()))),
        	$productAccountBalanceField = ReadonlyField::create('ProductPlusCashPayment_ShowPlusProductAccountBalance', _t('ProductPlusCashPayment.CURRENT_PRODUCT_BALANCE', 'Current E-Product Balance'), Account::get('ProductAccount', $memberid)->obj('Balance')->Nice()),
			$productAccountAmountField = NumericField::create('ProductPlusCashPayment_ProductAmount', _t('ProductPlusCashPayment.PRODUCT_PAYMENT_AMOUNT', 'E-Product Payment Amount'), $product_payment)->setAttribute('data-inputmask-allowminus', 'false'),
			$cashAccountBalanceField = ReadonlyField::create('ProductPlusCashPayment_ShowPlusCashAccountBalance', _t('ProductPlusCashPayment.CURRENT_CASH_BALANCE', 'Current E-Cash Balance'), Account::get('CashAccount', $memberid)->obj('Balance')->Nice()),
			$cashAccountAmountField = NumericField::create('ProductPlusCashPayment_CashAmount', _t('ProductPlusCashPayment.CASH_PAYMENT_AMOUNT', 'E-Cash Payment Amount'), $cash_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
	        $productAccountAmountField->setAttribute('readonly', 'readonly');
			$cashAccountAmountField->setAttribute('readonly', 'readonly');
		}
			
		$cashAccountBalanceField->setIncludeHiddenField(true);
		$productAccountBalanceField->setIncludeHiddenField(true);
			
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('ProductPlusCashPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['ProductPlusCashPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('ProductPlusCashPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['ProductPlusCashPayment_ProductAmount'], $data['ProductPlusCashPayment_Amount'] * $this->config()->product_min_percentage) < 0 || bccomp($data['ProductPlusCashPayment_ProductAmount'], $data['ProductPlusCashPayment_Amount'] * $this->config()->product_max_percentage) > 0){
            $validator->validationError(
                'ProductPlusCashPayment_ProductAmount',
                _t('ProductPlusCashPayment.INVALID_PRODUCT_AMOUNT', 'E-Product payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->product_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->product_max_percentage)->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['ProductPlusCashPayment_ProductAmount'] + $data['ProductPlusCashPayment_CashAmount'], $data['ProductPlusCashPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('ProductPlusCashPayment.INVALID_PAYMENT_AMOUNT', 'E-Product + E-Cash payment amount is not tally with total payment amount'),
                'warning'
            );
        }
		else{
			if(bccomp(Account::get('ProductAccount', $data['MemberID'])->Balance, $data['ProductPlusCashPayment_ProductAmount']) < 0){
	            $validator->validationError(
	                'ProductPlusCashPayment_ProductAmount',
	                _t('ProductPlusCashPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('ProductAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
	        if(bccomp(Account::get('CashAccount', $data['MemberID'])->Balance, $data['ProductPlusCashPayment_CashAmount']) < 0){
	            $validator->validationError(
	                'ProductPlusCashPayment_CashAmount',
	                _t('ProductPlusCashPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('CashAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->ProductAmount > 0 && !$this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforeProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->ProductAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$this->ProductAccounts()->add($id);
			}
			
			if($this->CashAmount > 0 && !$this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->CashAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
				$this->CashAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->CashAccounts()->filter('Debit:GreaterThan', 0)->count())){			
			foreach($this->ProductAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $statement->MemberID);
				$this->ProductAccounts()->add($id);
			}
			
			foreach($this->CashAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $statement->MemberID);
				$this->CashAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance >= $this->ProductAmount) && (Account::get('CashAccount', $this->Receipt()->MemberID)->Balance >= $this->CashAmount);
    }
}

?>