<?php
class BonusAccountWithdrawal extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Withdrawal";
    private static $plural_name = "E-Bonus Withdrawal";
    
    private static $extensions = array("WithdrawalSubmission");
	
	static function create_withdrawal_submission($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        $payout_data = $data;
        if(ClassInfo::exists($payout_data['PayoutMethod'])){
        	foreach($payout_data as $key => $val){
	    		if(preg_match(sprintf('/%s_/i', $payout_data['PayoutMethod']), $key)){
	    			$payout_data[str_replace(sprintf('%s_', $payout_data['PayoutMethod']), '', $key)] = $val;
	    		}
	    	}
            $payoutid = singleton($payout_data['PayoutMethod'])->castedUpdate($payout_data)->write();
            return BonusAccountWithdrawal::create()
            ->castedUpdate($data)
            ->setField('MemberID', $memberid)
            ->setField('PayoutMethodID', $payoutid)
            ->write();
        }
    }
	
	function getAction(){
        $view_link = Controller::join_links(BonusWithdrawalStatementPage::get()->first()->Link('view_statement'), $this->ID);
        $action = sprintf('<a class="btn btn-primary btn-xs" title="%s" data-title="%s" rel="popup tooltip" href="%s">%s</a>', _t('BonusAccountWithdrawal.CLICK_WITHDRAWAL_DETAILS', 'Click here to view withdrawal details'), _t('BonusAccountWithdrawal.WITHDRAWAL_DETAILS', 'Withdrawal Details'), $view_link, _t('BonusAccountWithdrawal.BUTTONVIEW', 'View'));

        return $action;
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusAccountWithdrawal');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
	
	function canPrint($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('PRINT_BonusAccountWithdrawal');
    }

	function canReject($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('REJECT_BonusAccountWithdrawal') && $this->Status == 'Pending';
    }
	
	function canApprove($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('APPROVE_BonusAccountWithdrawal') && $this->Status == 'Pending';
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusAccountWithdrawal' => array(
                'name' => _t('BonusAccountWithdrawal.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Bonus Withdrawal')
            ),
            'PRINT_BonusAccountWithdrawal' => array(
                'name' => _t('BonusAccountWithdrawal.PERMISSION_PRINT', 'Allow print access right'),
                'category' => _t('BonusAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Bonus Withdrawal')
            ),
            'REJECT_BonusAccountWithdrawal' => array(
                'name' => _t('BonusAccountWithdrawal.PERMISSION_REJECT', 'Allow reject access right'),
                'category' => _t('BonusAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Bonus Withdrawal')
            ),
            'APPROVE_BonusAccountWithdrawal' => array(
                'name' => _t('BonusAccountWithdrawal.PERMISSION_APPROVE', 'Allow approve access right'),
                'category' => _t('BonusAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Bonus Withdrawal')
            )
        );
    }
}
?>