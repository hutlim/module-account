<?php

class CashAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Adjustment";
    private static $plural_name = "E-Cash Adjustments";
    
    private static $extensions = array("AccountAdjustment");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
        return CashAccountAdjustment::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }
    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_CashAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_CashAccountAdjustment' => array(
                'name' => _t('CashAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Cash Adjustment')
            ),
            'CREATE_CashAccountAdjustment' => array(
                'name' => _t('CashAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('CashAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Cash Adjustment')
            )
        );
    }
}
?>