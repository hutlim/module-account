<?php

Class ProductPlusBonusPayment extends Payment {
    private static $singular_name = "E-Product + E-Bonus Payment";
    private static $plural_name = "E-Product + E-Bonus Payments";
    // manual | auto
    private static $type = 'manual';
	private static $product_min_percentage = 0.5;
	private static $product_max_percentage = 1;
	private static $priority_payment = 'ProductAccount';
    private static $db = array(
    	'BeforeProductBalance' => 'Currency',
        'AfterProductBalance' => 'Currency',
        'ProductAmount' => 'Currency',
        'BeforeBonusBalance' => 'Currency',
        'AfterBonusBalance' => 'Currency',
        'BonusAmount'  => 'Currency'
    );
	
	private static $many_many = array(
		'ProductAccounts' => 'ProductAccount',
		'BonusAccounts' => 'BonusAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforeProductBalance'] = _t('ProductPlusBonusPayment.PURCHASE_BEFORE_BALANCE', 'E-Product Before Balance');
		$labels['AfterProductBalance'] = _t('ProductPlusBonusPayment.PURCHASE_AFTER_BALANCE', 'E-Product After Balance');
		$labels['ProductAmount'] = _t('ProductPlusBonusPayment.PURCHASE_PAID_AMOUNT', 'E-Product Paid Amount');
		$labels['BeforeBonusBalance'] = _t('ProductPlusBonusPayment.BONUS_BEFORE_BALANCE', 'E-Bonus Before Balance');
		$labels['AfterBonusBalance'] = _t('ProductPlusBonusPayment.BONUS_AFTER_BALANCE', 'E-Bonus After Balance');
		$labels['BonusAmount'] = _t('ProductPlusBonusPayment.BONUS_PAID_AMOUNT', 'E-Bonus Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'ProductPlusBonusPayment_Amount' => $this->Amount,
				'ProductPlusBonusPayment_ProductAmount' => $this->ProductAmount,
				'ProductPlusBonusPayment_BonusAmount' => $this->BonusAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforeProductBalance');
        $fields->makeFieldReadonly('AfterProductBalance');
        $fields->makeFieldReadonly('BeforeBonusBalance');
        $fields->makeFieldReadonly('AfterBonusBalance');
		$fields->removeByName('ProductAccounts');
		$fields->removeByName('BonusAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$product_payment = $this->Amount * $this->config()->get('product_min_percentage');
    	$cash_payment = $this->Amount * (1 - $this->config()->get('product_min_percentage'));
    	if($this->config()->get('priority_payment') == 'ProductAccount'){
			$product_balance = Account::get('ProductAccount', $memberid)->Balance;
			if($product_balance > $product_payment){
		    	if($this->Amount * $this->config()->get('product_max_percentage') < $product_balance){
					$product_payment = $this->Amount * $this->config()->get('product_max_percentage');
				}
				else{
					$product_payment = $product_balance;
				}
			}

			$cash_payment = $this->Amount - $product_payment;
		}
		else{
			$cash_balance = Account::get('BonusAccount', $memberid)->Balance;
	    	if($cash_balance < $cash_payment){
	    		if($cash_balance < $this->Amount * (1 - $this->config()->get('product_max_percentage'))){
	    			$cash_payment = $this->Amount * (1 - $this->config()->get('product_max_percentage'));
				}
				else{
					$cash_payment = $cash_balance;
				}
			}

			$product_payment = $this->Amount - $cash_payment;
		}
		
		$fields = FieldList::create(
            HtmlEditorField_Readonly::create('ProductPlusBonusPayment_Note', _t('ProductPlusBonusPayment.NOTE', 'Note'), _t('ProductPlusBonusPayment.INVALID_PURCHASE_AMOUNT', 'E-Product payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->product_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->product_max_percentage)->Nice()))),
            $productAccountBalanceField = ReadonlyField::create('ProductPlusBonusPayment_ShowPlusProductAccountBalance', _t('ProductPlusBonusPayment.CURRENT_PURCHASE_BALANCE', 'Current E-Product Balance'), Account::get('ProductAccount', $memberid)->obj('Balance')->Nice()),
            $productAccountAmountField = NumericField::create('ProductPlusBonusPayment_ProductAmount', _t('ProductPlusBonusPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Product Payment Amount'), $product_payment)->setAttribute('data-inputmask-allowminus', 'false'),
            $cashAccountBalanceField = ReadonlyField::create('ProductPlusBonusPayment_ShowPlusBonusAccountBalance', _t('ProductPlusBonusPayment.CURRENT_BONUS_BALANCE', 'Current E-Bonus Balance'), Account::get('BonusAccount', $memberid)->obj('Balance')->Nice()),
            $cashAccountAmountField = NumericField::create('ProductPlusBonusPayment_BonusAmount', _t('ProductPlusBonusPayment.BONUS_PAYMENT_AMOUNT', 'E-Bonus Payment Amount'), $cash_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
    		$productAccountAmountField->setAttribute('readonly', 'readonly');
			$cashAccountAmountField->setAttribute('readonly', 'readonly');
		}
		
		$productAccountBalanceField->setIncludeHiddenField(true);
		$cashAccountBalanceField->setIncludeHiddenField(true);
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('ProductPlusBonusPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['ProductPlusBonusPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('ProductPlusBonusPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['ProductPlusBonusPayment_ProductAmount'], $data['ProductPlusBonusPayment_Amount'] * $this->config()->product_min_percentage) < 0 || bccomp($data['ProductPlusBonusPayment_ProductAmount'], $data['ProductPlusBonusPayment_Amount'] * $this->config()->product_max_percentage) > 0){
            $validator->validationError(
                'ProductPlusBonusPayment_ProductAmount',
                _t('ProductPlusBonusPayment.INVALID_PURCHASE_AMOUNT', 'E-Product payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->product_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->product_max_percentage)->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['ProductPlusBonusPayment_ProductAmount'] + $data['ProductPlusBonusPayment_BonusAmount'], $data['ProductPlusBonusPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('ProductPlusBonusPayment.INVALID_PAYMENT_AMOUNT', 'E-Product + E-Bonus payment amount is not tally with total payment amount'),
                'warning'
            );
        }
		else{
			if(bccomp(Account::get('ProductAccount', $data['MemberID'])->Balance, $data['ProductPlusBonusPayment_ProductAmount']) < 0){
	            $validator->validationError(
	                'ProductPlusBonusPayment_ProductAmount',
	                _t('ProductPlusBonusPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('ProductAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
	        if(bccomp(Account::get('BonusAccount', $data['MemberID'])->Balance, $data['ProductPlusBonusPayment_BonusAmount']) < 0){
	            $validator->validationError(
	                'ProductPlusBonusPayment_BonusAmount',
	                _t('ProductPlusBonusPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('BonusAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }

		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->ProductAmount > 0 && !$this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforeProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->ProductAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$this->ProductAccounts()->add($id);
			}

			if($this->BonusAmount > 0 && !$this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->BonusAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
				$this->BonusAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count())){			
			foreach($this->ProductAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $statement->MemberID);
				$this->ProductAccounts()->add($id);
			}
			
			foreach($this->BonusAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $statement->MemberID);
				$this->BonusAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance >= $this->ProductAmount) && (Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance >= $this->BonusAmount);
    }
}

?>