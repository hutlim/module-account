<?php

class ProductAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Product Adjustment";
    private static $plural_name = "E-Product Adjustments";
    
    private static $extensions = array("AccountAdjustment");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
        return ProductAccountAdjustment::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    
    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_ProductAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_ProductAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_ProductAccountAdjustment' => array(
                'name' => _t('ProductAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('ProductAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Product Adjustment')
            ),
            'CREATE_ProductAccountAdjustment' => array(
                'name' => _t('ProductAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('ProductAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Product Adjustment')
            )
        );
    }
}
?>