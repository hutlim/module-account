<?php

class UpgradeAccount extends DataObject {
    private static $singular_name = "E-Upgrade Account";
    private static $plural_name = "E-Upgrade Accounts";
    
    private static $extensions = array("Account");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return UpgradeAccount::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        return true;
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
}
?>