<?php

class PurchaseAccountAdjustment extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Purchase Adjustment";
    private static $plural_name = "E-Purchase Adjustments";
    
    private static $extensions = array("AccountAdjustment");

    static function create_statement($data, $memberid) {
        if(!$memberid) {
            throw new Exception("Empty memberid");
        }
        return PurchaseAccountAdjustment::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_PurchaseAccountAdjustment');
    }

    function canEdit($member = false) {
    	if(!$this->exists() && $this->canCreate($member)){
    		return true;
    	}
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('CREATE_PurchaseAccountAdjustment');
    }

    public function providePermissions() {
        return array(
            'VIEW_PurchaseAccountAdjustment' => array(
                'name' => _t('PurchaseAccountAdjustment.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('PurchaseAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Purchase Adjustment')
            ),
            'CREATE_PurchaseAccountAdjustment' => array(
                'name' => _t('PurchaseAccountAdjustment.PERMISSION_CREATE', 'Allow create access right'),
                'category' => _t('PurchaseAccountAdjustment.PERMISSIONS_CATEGORY', 'E-Purchase Adjustment')
            )
        );
    }
}
?>