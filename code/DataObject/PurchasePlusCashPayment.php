<?php

Class PurchasePlusCashPayment extends Payment {
    private static $singular_name = "E-Purchase + E-Cash Payment";
    private static $plural_name = "E-Purchase + E-Cash Payments";
    // manual | auto
    private static $type = 'manual';
	private static $purchase_min_percentage = 0.5;
	private static $purchase_max_percentage = 1;
	private static $priority_payment = 'PurchaseAccount';
    private static $db = array(
    	'BeforePurchaseBalance' => 'Currency',
        'AfterPurchaseBalance' => 'Currency',
        'PurchaseAmount' => 'Currency',
        'BeforeCashBalance' => 'Currency',
        'AfterCashBalance' => 'Currency',
        'CashAmount'  => 'Currency'
    );
	
	private static $many_many = array(
		'PurchaseAccounts' => 'PurchaseAccount',
		'CashAccounts' => 'CashAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforePurchaseBalance'] = _t('PurchasePlusCashPayment.PURCHASE_BEFORE_BALANCE', 'E-Purchase Before Balance');
		$labels['AfterPurchaseBalance'] = _t('PurchasePlusCashPayment.PURCHASE_AFTER_BALANCE', 'E-Purchase After Balance');
		$labels['PurchaseAmount'] = _t('PurchasePlusCashPayment.PURCHASE_PAID_AMOUNT', 'E-Purchase Paid Amount');
		$labels['BeforeCashBalance'] = _t('PurchasePlusCashPayment.CASH_BEFORE_BALANCE', 'E-Cash Before Balance');
		$labels['AfterCashBalance'] = _t('PurchasePlusCashPayment.CASH_AFTER_BALANCE', 'E-Cash After Balance');
		$labels['CashAmount'] = _t('PurchasePlusCashPayment.CASH_PAID_AMOUNT', 'E-Cash Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'PurchasePlusCashPayment_Amount' => $this->Amount,
				'PurchasePlusCashPayment_PurchaseAmount' => $this->PurchaseAmount,
				'PurchasePlusCashPayment_CashAmount' => $this->CashAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforePurchaseBalance');
        $fields->makeFieldReadonly('AfterPurchaseBalance');
        $fields->makeFieldReadonly('BeforeCashBalance');
        $fields->makeFieldReadonly('AfterCashBalance');
		$fields->removeByName('PurchaseAccounts');
		$fields->removeByName('CashAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$purchase_payment = $this->Amount * $this->config()->get('purchase_min_percentage');
    	$cash_payment = $this->Amount * (1 - $this->config()->get('purchase_min_percentage'));
    	if($this->config()->get('priority_payment') == 'PurchaseAccount'){
			$purchase_balance = Account::get('PurchaseAccount', $memberid)->Balance;
			if($purchase_balance > $purchase_payment){
		    	if($this->Amount * $this->config()->get('purchase_max_percentage') < $purchase_balance){
					$purchase_payment = $this->Amount * $this->config()->get('purchase_max_percentage');
				}
				else{
					$purchase_payment = $purchase_balance;
				}
			}

			$cash_payment = $this->Amount - $purchase_payment;
		}
		else{
			$cash_balance = Account::get('CashAccount', $memberid)->Balance;
	    	if($cash_balance < $cash_payment){
	    		if($cash_balance < $this->Amount * (1 - $this->config()->get('purchase_max_percentage'))){
	    			$cash_payment = $this->Amount * (1 - $this->config()->get('purchase_max_percentage'));
				}
				else{
					$cash_payment = $cash_balance;
				}
			}

			$purchase_payment = $this->Amount - $cash_payment;
		}
		
		$fields = FieldList::create(
            HtmlEditorField_Readonly::create('PurchasePlusCashPayment_Note', _t('PurchasePlusCashPayment.NOTE', 'Note'), _t('PurchasePlusCashPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice()))),
            $purchaseAccountBalanceField = ReadonlyField::create('PurchasePlusCashPayment_ShowPlusPurchaseAccountBalance', _t('PurchasePlusCashPayment.CURRENT_PURCHASE_BALANCE', 'Current E-Purchase Balance'), Account::get('PurchaseAccount', $memberid)->obj('Balance')->Nice()),
            $purchaseAccountAmountField = NumericField::create('PurchasePlusCashPayment_PurchaseAmount', _t('PurchasePlusCashPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Purchase Payment Amount'), $purchase_payment)->setAttribute('data-inputmask-allowminus', 'false'),
            $cashAccountBalanceField = ReadonlyField::create('PurchasePlusCashPayment_ShowPlusCashAccountBalance', _t('PurchasePlusCashPayment.CURRENT_CASH_BALANCE', 'Current E-Cash Balance'), Account::get('CashAccount', $memberid)->obj('Balance')->Nice()),
            $cashAccountAmountField = NumericField::create('PurchasePlusCashPayment_CashAmount', _t('PurchasePlusCashPayment.CASH_PAYMENT_AMOUNT', 'E-Cash Payment Amount'), $cash_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
    		$purchaseAccountAmountField->setAttribute('readonly', 'readonly');
			$cashAccountAmountField->setAttribute('readonly', 'readonly');
		}
		
		$purchaseAccountBalanceField->setIncludeHiddenField(true);
		$cashAccountBalanceField->setIncludeHiddenField(true);
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusCashPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['PurchasePlusCashPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusCashPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusCashPayment_PurchaseAmount'], $data['PurchasePlusCashPayment_Amount'] * $this->config()->purchase_min_percentage) < 0 || bccomp($data['PurchasePlusCashPayment_PurchaseAmount'], $data['PurchasePlusCashPayment_Amount'] * $this->config()->purchase_max_percentage) > 0){
            $validator->validationError(
                'PurchasePlusCashPayment_PurchaseAmount',
                _t('PurchasePlusCashPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusCashPayment_PurchaseAmount'] + $data['PurchasePlusCashPayment_CashAmount'], $data['PurchasePlusCashPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusCashPayment.INVALID_PAYMENT_AMOUNT', 'E-Purchase + E-Cash payment amount is not tally with total payment amount'),
                'warning'
            );
        }
		else{
			if(bccomp(Account::get('PurchaseAccount', $data['MemberID'])->Balance, $data['PurchasePlusCashPayment_PurchaseAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusCashPayment_PurchaseAmount',
	                _t('PurchasePlusCashPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('PurchaseAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
	        if(bccomp(Account::get('CashAccount', $data['MemberID'])->Balance, $data['PurchasePlusCashPayment_CashAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusCashPayment_CashAmount',
	                _t('PurchasePlusCashPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('CashAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }

		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->PurchaseAmount > 0 && !$this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforePurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->PurchaseAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterPurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$this->PurchaseAccounts()->add($id);
			}

			if($this->CashAmount > 0 && !$this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->CashAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
				$this->CashAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->CashAccounts()->filter('Debit:GreaterThan', 0)->count())){			
			foreach($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $statement->MemberID);
				$this->PurchaseAccounts()->add($id);
			}
			
			foreach($this->CashAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $statement->MemberID);
				$this->CashAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance >= $this->PurchaseAmount) && (Account::get('CashAccount', $this->Receipt()->MemberID)->Balance >= $this->CashAmount);
    }
}

?>