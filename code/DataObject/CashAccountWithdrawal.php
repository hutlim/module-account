<?php
class CashAccountWithdrawal extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Withdrawal";
    private static $plural_name = "E-Cash Withdrawal";
    
    private static $extensions = array("WithdrawalSubmission");
	
	static function create_withdrawal_submission($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
		$payout_data = $data;
        if(ClassInfo::exists($payout_data['PayoutMethod'])){
        	foreach($payout_data as $key => $val){
	    		if(preg_match(sprintf('/%s_/i', $payout_data['PayoutMethod']), $key)){
	    			$payout_data[str_replace(sprintf('%s_', $payout_data['PayoutMethod']), '', $key)] = $val;
	    		}
	    	}
            $payoutid = singleton($payout_data['PayoutMethod'])->castedUpdate($payout_data)->write();
            return CashAccountWithdrawal::create()
            ->castedUpdate($data)
            ->setField('MemberID', $memberid)
            ->setField('PayoutMethodID', $payoutid)
            ->write();
        }
    }
	
	function getAction(){
        $view_link = Controller::join_links(CashWithdrawalStatementPage::get()->first()->Link('view_statement'), $this->ID);
        $action = sprintf('<a class="btn btn-primary btn-xs" title="%s" data-title="%s" rel="popup tooltip" href="%s">%s</a>', _t('CashAccountWithdrawal.CLICK_WITHDRAWAL_DETAILS', 'Click here to view withdrawal details'), _t('CashAccountWithdrawal.WITHDRAWAL_DETAILS', 'Withdrawal Details'), $view_link, _t('CashAccountWithdrawal.BUTTONVIEW', 'View'));

        return $action;
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashAccountWithdrawal');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }
	
	function canPrint($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('PRINT_CashAccountWithdrawal');
    }

	function canReject($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('REJECT_CashAccountWithdrawal') && $this->Status == 'Pending';
    }
	
	function canApprove($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('APPROVE_CashAccountWithdrawal') && $this->Status == 'Pending';
    }

    public function providePermissions() {
        return array(
            'VIEW_CashAccountWithdrawal' => array(
                'name' => _t('CashAccountWithdrawal.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Cash Withdrawal')
            ),
            'PRINT_CashAccountWithdrawal' => array(
                'name' => _t('CashAccountWithdrawal.PERMISSION_PRINT', 'Allow print access right'),
                'category' => _t('CashAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Cash Withdrawal')
            ),
            'REJECT_CashAccountWithdrawal' => array(
                'name' => _t('CashAccountWithdrawal.PERMISSION_REJECT', 'Allow reject access right'),
                'category' => _t('CashAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Cash Withdrawal')
            ),
            'APPROVE_CashAccountWithdrawal' => array(
                'name' => _t('CashAccountWithdrawal.PERMISSION_APPROVE', 'Allow approve access right'),
                'category' => _t('CashAccountWithdrawal.PERMISSIONS_CATEGORY', 'E-Cash Withdrawal')
            )
        );
    }
}
?>