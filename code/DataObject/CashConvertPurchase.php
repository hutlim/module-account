<?php

class CashConvertPurchase extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Cash Convert E-Purchase";
    private static $plural_name = "E-Cash Convert E-Purchase";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return CashConvertPurchase::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_CashConvertPurchase');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_CashConvertPurchase' => array(
                'name' => _t('CashConvertPurchase.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('CashConvertPurchase.PERMISSIONS_CATEGORY', 'E-Cash Convert E-Purchase')
            )
        );
    }
}
?>