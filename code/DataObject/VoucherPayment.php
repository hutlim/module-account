<?php

Class VoucherPayment extends Payment {
    private static $singular_name = "Voucher Payment";
    private static $plural_name = "Voucher Payments";
	
	private static $has_one = array(
		'Voucher' => 'Voucher'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['Voucher'] = _t('VoucherPayment.VOUCHER', 'Voucher');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$voucher_code = $this->VoucherCode ? $this->VoucherCode : $this->Voucher()->Code;
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'VoucherPayment_Amount' => $this->Amount,
				'VoucherPayment_VoucherCode' => $voucher_code
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();
			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

        $fields->makeFieldReadonly('VoucherID');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
        $fields = FieldList::create(
            TextField::create('VoucherPayment_VoucherCode', _t('VoucherPayment.VOUCHER_CODE', 'Voucher Code'))
        );
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
    	$voucher = Voucher::get()->filter('Status', 'Available')->find('Code', $data['VoucherPayment_VoucherCode']);
        if($data['VoucherPayment_VoucherCode'] == '' || !$voucher){
            $validator->validationError(
                'VoucherPayment_VoucherCode',
                _t('VoucherPayment.INVALID_VOUCHER_CODE', 'Invalid voucher code'),
                'warning'
            );
        }
        else if($voucher->Amount <= 0 || bccomp($voucher->Amount, $data['VoucherPayment_Amount'], 2) != 0){
            $validator->validationError(
                'VoucherPayment_VoucherCode',
                _t('VoucherPayment.INVALID_VOUCHER_AMOUNT', 'Invalid voucher amount'),
                'warning'
            );
        }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();
		
		if($this->VoucherCode){
			$voucher = Voucher::get()->find('Code', $this->VoucherCode);
			$this->VoucherID = $voucher ? $voucher->ID : 0;
		}
		
		if($this->Amount > 0 && $this->isChanged('Status') && $this->Status == 'Success' && $this->VoucherID){
			$this->Voucher()->Status = 'Used';
			$this->Voucher()->UsedByID = $this->Receipt()->MemberID;
			$this->Voucher()->write();
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && $this->VoucherID){
			$this->Voucher()->Status = 'Available';
			$this->Voucher()->write();
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('evoucher', 'process', $this->ID)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
	function checkVoucher(){
		return $this->Voucher()->Status == 'Available' && bccomp($this->Voucher()->Amount, $this->Amount, 2) == 0;
	}
}

?>