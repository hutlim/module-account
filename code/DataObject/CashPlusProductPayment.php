<?php

Class CashPlusProductPayment extends Payment {
    private static $singular_name = "E-Cash + E-Product Payment";
    private static $plural_name = "E-Cash + E-Product Payments";
    // manual | auto
    private static $type = 'manual';
	private static $cash_min_percentage = 0.5;
	private static $cash_max_percentage = 1;
	private static $priority_payment = 'CashAccount';
    private static $db = array(
        'BeforeCashBalance' => 'Currency',
        'AfterCashBalance' => 'Currency',
        'CashAmount'  => 'Currency',
        'BeforeProductBalance' => 'Currency',
        'AfterProductBalance' => 'Currency',
        'ProductAmount' => 'Currency'
    );
	
	private static $many_many = array(
		'CashAccounts' => 'CashAccount',
		'ProductAccounts' => 'ProductAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforeCashBalance'] = _t('CashPlusProductPayment.CASH_BEFORE_BALANCE', 'E-Cash Before Balance');
		$labels['AfterCashBalance'] = _t('CashPlusProductPayment.CASH_AFTER_BALANCE', 'E-Cash After Balance');
		$labels['CashAmount'] = _t('CashPlusProductPayment.CASH_PAID_AMOUNT', 'E-Cash Paid Amount');
		$labels['BeforeProductBalance'] = _t('CashPlusProductPayment.PRODUCT_BEFORE_BALANCE', 'E-Product Before Balance');
		$labels['AfterProductBalance'] = _t('CashPlusProductPayment.PRODUCT_AFTER_BALANCE', 'E-Product After Balance');
		$labels['ProductAmount'] = _t('CashPlusProductPayment.PRODUCT_PAID_AMOUNT', 'E-Product Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'CashPlusProductPayment_Amount' => $this->Amount,
				'CashPlusProductPayment_CashAmount' => $this->CashAmount,
				'CashPlusProductPayment_ProductAmount' => $this->ProductAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

        $fields->makeFieldReadonly('BeforeCashBalance');
        $fields->makeFieldReadonly('AfterCashBalance');
		$fields->makeFieldReadonly('BeforeProductBalance');
        $fields->makeFieldReadonly('AfterProductBalance');
		$fields->removeByName('CashAccounts');
		$fields->removeByName('ProductAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$cash_payment = $this->Amount * $this->config()->get('cash_min_percentage');
    	$product_payment = $this->Amount * (1 - $this->config()->get('cash_min_percentage'));
    	if($this->config()->get('priority_payment') == 'CashAccount'){
			$cash_balance = Account::get('CashAccount', $memberid)->Balance;
			if($cash_balance > $cash_payment){
		    	if($this->Amount * $this->config()->get('cash_max_percentage') < $cash_balance){
					$cash_payment = $this->Amount * $this->config()->get('cash_max_percentage');
				}
				else{
					$cash_payment = $cash_balance;
				}
			}

			$product_payment = $this->Amount - $cash_payment;
		}
		else{
			$product_balance = Account::get('ProductAccount', $memberid)->Balance;
	    	if($product_balance < $product_payment){
	    		if($product_balance < $this->Amount * (1 - $this->config()->get('cash_max_percentage'))){
	    			$product_payment = $this->Amount * (1 - $this->config()->get('cash_max_percentage'));
				}
				else{
					$product_payment = $product_balance;
				}
			}

			$cash_payment = $this->Amount - $product_payment;
		}
		
		$fields = FieldList::create(
        	HtmlEditorField_Readonly::create('CashPlusProductPayment_Note', _t('CashPlusProductPayment.NOTE', 'Note'), _t('CashPlusProductPayment.INVALID_CASH_AMOUNT', 'E-Cash payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_max_percentage'))->Nice()))),
        	$cashAccountBalanceField = ReadonlyField::create('CashPlusProductPayment_ShowPlusCashAccountBalance', _t('CashPlusProductPayment.CURRENT_CASH_BALANCE', 'Current E-Cash Balance'), Account::get('CashAccount', $memberid)->obj('Balance')->Nice()),
			$cashAccountAmountField = NumericField::create('CashPlusProductPayment_CashAmount', _t('CashPlusProductPayment.CASH_PAYMENT_AMOUNT', 'E-Cash Payment Amount'), $cash_payment)->setAttribute('data-inputmask-allowminus', 'false'),
			$productAccountBalanceField = ReadonlyField::create('CashPlusProductPayment_ShowPlusCashAccountBalance', _t('CashPlusProductPayment.CURRENT_PRODUCT_BALANCE', 'Current E-Product Balance'), Account::get('ProductAccount', $memberid)->obj('Balance')->Nice()),
			$productAccountAmountField = NumericField::create('CashPlusProductPayment_ProductAmount', _t('CashPlusProductPayment.PRODUCT_PAYMENT_AMOUNT', 'E-Product Payment Amount'), $product_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
			$cashAccountAmountField->setAttribute('readonly', 'readonly');
			$productAccountAmountField->setAttribute('readonly', 'readonly');
		}
			
		$cashAccountBalanceField->setIncludeHiddenField(true);
		$productAccountBalanceField->setIncludeHiddenField(true);
			
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('CashPlusProductPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['CashPlusProductPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('CashPlusProductPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['CashPlusProductPayment_CashAmount'], $data['CashPlusProductPayment_Amount'] * $this->config()->cash_min_percentage) < 0 || bccomp($data['CashPlusProductPayment_CashAmount'], $data['CashPlusProductPayment_Amount'] * $this->config()->cash_max_percentage) > 0){
            $validator->validationError(
                'CashPlusProductPayment_CashAmount',
                _t('CashPlusProductPayment.INVALID_CASH_AMOUNT', 'E-Cash payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_min_percentage'))->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->get('cash_max_percentage'))->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['CashPlusProductPayment_CashAmount'] + $data['CashPlusProductPayment_ProductAmount'], $data['CashPlusProductPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('CashPlusProductPayment.INVALID_PAYMENT_AMOUNT', 'E-Cash + E-Product payment amount is not tally with total payment amount'),
                'warning'
            );
        }
        else {
	        if(bccomp(Account::get('CashAccount', $data['MemberID'])->Balance, $data['CashPlusProductPayment_CashAmount']) < 0){
	            $validator->validationError(
	                'CashPlusProductPayment_CashAmount',
	                _t('CashPlusProductPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('CashAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
			if(bccomp(Account::get('ProductAccount', $data['MemberID'])->Balance, $data['CashPlusProductPayment_ProductAmount']) < 0){
	            $validator->validationError(
	                'CashPlusProductPayment_ProductAmount',
	                _t('CashPlusProductPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('ProductAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }
		
		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->CashAmount > 0 && !$this->CashAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->CashAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterCashBalance', Account::get('CashAccount', $this->Receipt()->MemberID)->Balance);
				$this->CashAccounts()->add($id);
			}
			
			if($this->ProductAmount > 0 && !$this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforeProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->ProductAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterProductBalance', Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance);
				$this->ProductAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->CashAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->ProductAccounts()->filter('Debit:GreaterThan', 0)->count())){
			foreach($this->CashAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = CashAccount::create_statement($statement_data, $statement->MemberID);
				$this->CashAccounts()->add($id);
			}
			
			foreach($this->ProductAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = ProductAccount::create_statement($statement_data, $statement->MemberID);
				$this->ProductAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('CashAccount', $this->Receipt()->MemberID)->Balance >= $this->CashAmount) && (Account::get('ProductAccount', $this->Receipt()->MemberID)->Balance >= $this->ProductAmount);
    }
}

?>