<?php
class VoucherStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Voucher Status List";
    private static $plural_name = "Voucher Status Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Available',
            'Title' => 'Available',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Used',
            'Title' => 'Used',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Cancelled',
            'Title' => 'Cancelled',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Available',
            'Title' => '可使用',
            'Sort' => 40,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Used',
            'Title' => '已使用',
            'Sort' => 50,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Cancelled',
            'Title' => '已取消',
            'Sort' => 60,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = VoucherStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
