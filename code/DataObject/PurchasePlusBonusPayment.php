<?php

Class PurchasePlusBonusPayment extends Payment {
    private static $singular_name = "E-Purchase + E-Bonus Payment";
    private static $plural_name = "E-Purchase + E-Bonus Payments";
    // manual | auto
    private static $type = 'manual';
	private static $purchase_min_percentage = 0.5;
	private static $purchase_max_percentage = 1;
	private static $priority_payment = 'PurchaseAccount';
    private static $db = array(
    	'BeforePurchaseBalance' => 'Currency',
        'AfterPurchaseBalance' => 'Currency',
        'PurchaseAmount' => 'Currency',
        'BeforeBonusBalance' => 'Currency',
        'AfterBonusBalance' => 'Currency',
        'BonusAmount'  => 'Currency'
    );
	
	private static $many_many = array(
		'PurchaseAccounts' => 'PurchaseAccount',
		'BonusAccounts' => 'BonusAccount'
	);
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BeforePurchaseBalance'] = _t('PurchasePlusBonusPayment.PURCHASE_BEFORE_BALANCE', 'E-Purchase Before Balance');
		$labels['AfterPurchaseBalance'] = _t('PurchasePlusBonusPayment.PURCHASE_AFTER_BALANCE', 'E-Purchase After Balance');
		$labels['PurchaseAmount'] = _t('PurchasePlusBonusPayment.PURCHASE_PAID_AMOUNT', 'E-Purchase Paid Amount');
		$labels['BeforeBonusBalance'] = _t('PurchasePlusBonusPayment.BONUS_BEFORE_BALANCE', 'E-Bonus Before Balance');
		$labels['AfterBonusBalance'] = _t('PurchasePlusBonusPayment.BONUS_AFTER_BALANCE', 'E-Bonus After Balance');
		$labels['BonusAmount'] = _t('PurchasePlusBonusPayment.BONUS_PAID_AMOUNT', 'E-Bonus Paid Amount');
		
		return $labels;	
	}
	
	function validate() {
        $validationResult = parent::validate();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			$data = array(
				'MemberID' => $this->Receipt()->MemberID,
				'PurchasePlusBonusPayment_Amount' => $this->Amount,
				'PurchasePlusBonusPayment_PurchaseAmount' => $this->PurchaseAmount,
				'PurchasePlusBonusPayment_BonusAmount' => $this->BonusAmount
			);
	        $errors = $this->getPaymentFormRequirements(RequiredFields::create(), $data)->getErrors();

			if($errors){
	        	foreach($errors as $error){
            		$validationResult->error($error['message']);
				}
            }
		}

        return $validationResult;
    }
    
    function getCMSFields(){
        $fields = parent::getCMSFields();

		$fields->makeFieldReadonly('BeforePurchaseBalance');
        $fields->makeFieldReadonly('AfterPurchaseBalance');
        $fields->makeFieldReadonly('BeforeBonusBalance');
        $fields->makeFieldReadonly('AfterBonusBalance');
		$fields->removeByName('PurchaseAccounts');
		$fields->removeByName('BonusAccounts');
        
        return $fields;
    }
    
    function getPaymentFormFields($memberid){
    	$purchase_payment = $this->Amount * $this->config()->get('purchase_min_percentage');
    	$cash_payment = $this->Amount * (1 - $this->config()->get('purchase_min_percentage'));
    	if($this->config()->get('priority_payment') == 'PurchaseAccount'){
			$purchase_balance = Account::get('PurchaseAccount', $memberid)->Balance;
			if($purchase_balance > $purchase_payment){
		    	if($this->Amount * $this->config()->get('purchase_max_percentage') < $purchase_balance){
					$purchase_payment = $this->Amount * $this->config()->get('purchase_max_percentage');
				}
				else{
					$purchase_payment = $purchase_balance;
				}
			}

			$cash_payment = $this->Amount - $purchase_payment;
		}
		else{
			$cash_balance = Account::get('BonusAccount', $memberid)->Balance;
	    	if($cash_balance < $cash_payment){
	    		if($cash_balance < $this->Amount * (1 - $this->config()->get('purchase_max_percentage'))){
	    			$cash_payment = $this->Amount * (1 - $this->config()->get('purchase_max_percentage'));
				}
				else{
					$cash_payment = $cash_balance;
				}
			}

			$purchase_payment = $this->Amount - $cash_payment;
		}
		
		$fields = FieldList::create(
            HtmlEditorField_Readonly::create('PurchasePlusBonusPayment_Note', _t('PurchasePlusBonusPayment.NOTE', 'Note'), _t('PurchasePlusBonusPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice()))),
            $purchaseAccountBalanceField = ReadonlyField::create('PurchasePlusBonusPayment_ShowPlusPurchaseAccountBalance', _t('PurchasePlusBonusPayment.CURRENT_PURCHASE_BALANCE', 'Current E-Purchase Balance'), Account::get('PurchaseAccount', $memberid)->obj('Balance')->Nice()),
            $purchaseAccountAmountField = NumericField::create('PurchasePlusBonusPayment_PurchaseAmount', _t('PurchasePlusBonusPayment.PURCHASE_PAYMENT_AMOUNT', 'E-Purchase Payment Amount'), $purchase_payment)->setAttribute('data-inputmask-allowminus', 'false'),
            $cashAccountBalanceField = ReadonlyField::create('PurchasePlusBonusPayment_ShowPlusBonusAccountBalance', _t('PurchasePlusBonusPayment.CURRENT_BONUS_BALANCE', 'Current E-Bonus Balance'), Account::get('BonusAccount', $memberid)->obj('Balance')->Nice()),
            $cashAccountAmountField = NumericField::create('PurchasePlusBonusPayment_BonusAmount', _t('PurchasePlusBonusPayment.BONUS_PAYMENT_AMOUNT', 'E-Bonus Payment Amount'), $cash_payment)->setAttribute('data-inputmask-allowminus', 'false')
        );
		
    	if($this->config()->get('type') == 'auto'){
    		$purchaseAccountAmountField->setAttribute('readonly', 'readonly');
			$cashAccountAmountField->setAttribute('readonly', 'readonly');
		}
		
		$purchaseAccountBalanceField->setIncludeHiddenField(true);
		$cashAccountBalanceField->setIncludeHiddenField(true);
		
		$this->extend('updatePaymentFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPaymentFormRequirements($validator, $data){
        if($data['MemberID'] == '' || !$member = DataObject::get_by_id('Member', (int)$data['MemberID'])){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusBonusPayment.INVALID_MEMBER_ID', 'Invalid Member ID'),
                'warning'
            );
        }
        else if($data['PurchasePlusBonusPayment_Amount'] <= 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusBonusPayment.INVALID_AMOUNT', 'Invalid Amount'),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusBonusPayment_PurchaseAmount'], $data['PurchasePlusBonusPayment_Amount'] * $this->config()->purchase_min_percentage) < 0 || bccomp($data['PurchasePlusBonusPayment_PurchaseAmount'], $data['PurchasePlusBonusPayment_Amount'] * $this->config()->purchase_max_percentage) > 0){
            $validator->validationError(
                'PurchasePlusBonusPayment_PurchaseAmount',
                _t('PurchasePlusBonusPayment.INVALID_PURCHASE_AMOUNT', 'E-Purchase payment amount cannot less than {min_percentage} or exceed {max_percentage} of total payment amount', '', array('min_percentage' => DBField::create_field('Percentage', $this->config()->purchase_min_percentage)->Nice(), 'max_percentage' => DBField::create_field('Percentage', $this->config()->purchase_max_percentage)->Nice())),
                'warning'
            );
        }
		else if(bccomp($data['PurchasePlusBonusPayment_PurchaseAmount'] + $data['PurchasePlusBonusPayment_BonusAmount'], $data['PurchasePlusBonusPayment_Amount']) != 0){
            $validator->validationError(
                'Payment',
                _t('PurchasePlusBonusPayment.INVALID_PAYMENT_AMOUNT', 'E-Purchase + E-Bonus payment amount is not tally with total payment amount'),
                'warning'
            );
        }
		else{
			if(bccomp(Account::get('PurchaseAccount', $data['MemberID'])->Balance, $data['PurchasePlusBonusPayment_PurchaseAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusBonusPayment_PurchaseAmount',
	                _t('PurchasePlusBonusPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('PurchaseAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
			
	        if(bccomp(Account::get('BonusAccount', $data['MemberID'])->Balance, $data['PurchasePlusBonusPayment_BonusAmount']) < 0){
	            $validator->validationError(
	                'PurchasePlusBonusPayment_BonusAmount',
	                _t('PurchasePlusBonusPayment.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get('BonusAccount', $data['MemberID'])->obj('Balance')->Nice())),
	                'warning'
	            );
	        }
	    }

		$this->extend('updatePaymentFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();

		if($this->isChanged('Status') && $this->Status == 'Success'){
			if($this->PurchaseAmount > 0 && !$this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count()){
				$this->setField('BeforePurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->PurchaseAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $this->Receipt()->MemberID);
				$this->setField('AfterPurchaseBalance', Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance);
				$this->PurchaseAccounts()->add($id);
			}

			if($this->BonusAmount > 0 && !$this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count()){
	            $this->setField('BeforeBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Debit' => $this->BonusAmount,
	                'Reference' => $this->Receipt()->Reference,
	                'Description' => $this->Receipt()->TransactionType()->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $this->Receipt()->MemberID);
	            $this->setField('AfterBonusBalance', Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance);
				$this->BonusAccounts()->add($id);
			}
		}
	}
    
    function pendingPayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Pending')->write();
        return $this;
    }
    
    function failurePayment($data = array()){
        $this->castedUpdate($data)->setField('Status', 'Failure')->write();
        return $this;
    }
    
    function completePayment($data = array()){
        if($this->Status == 'Incomplete' || $this->Status == 'Pending'){
            $this->castedUpdate($data)->setField('Status', 'Success')->write();
        }
        
        return $this;
    }
	
	function refundPayment($data = array()){
		if($this->Status == 'Success' && $this->Amount > 0 && ($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0)->count() || $this->BonusAccounts()->filter('Debit:GreaterThan', 0)->count())){			
			foreach($this->PurchaseAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = PurchaseAccount::create_statement($statement_data, $statement->MemberID);
				$this->PurchaseAccounts()->add($id);
			}
			
			foreach($this->BonusAccounts()->filter('Debit:GreaterThan', 0) as $statement){
	            $statement_data = array(
	                'Type' => 'Purchase',
	                'Credit' => $statement->Debit,
	                'Reference' => $statement->Reference,
	                'Description' => 'Refund - ' . $statement->Description
	            );
	            $id = BonusAccount::create_statement($statement_data, $statement->MemberID);
				$this->BonusAccounts()->add($id);
			}
			$this->castedUpdate($data)->setField('Status', 'Refunded')->write();
		}
		return $this;
	}
    
    function ProcessLink(){
        $token = new SecurityToken(sprintf('TOKEN_%s', $this->Receipt()->Reference));
        return $token->addToUrl(Controller::join_links('ewallet', 'process', $this->ID, $this->ClassName)) . sprintf('&locale=%s', Controller::curr()->Locale);
    }
	
    function checkBalance(){
    	return (Account::get('PurchaseAccount', $this->Receipt()->MemberID)->Balance >= $this->PurchaseAmount) && (Account::get('BonusAccount', $this->Receipt()->MemberID)->Balance >= $this->BonusAmount);
    }
}

?>