<?php

Class BankWirePayoutMethod extends PayoutMethod {
    private static $singular_name = "Bank Wire Payout";
    private static $plural_name = "Bank Wire Payouts";
	
	private static $processing_fee = 20;
    private static $processing_fee_percentage = 0;
    
    private static $db = array(
    	'Country' => 'Varchar(2)',
        'BankName' => 'Varchar(100)',
        'BranchName' => 'Varchar(100)',
        'SwiftCode' => 'Varchar',
        'Currency' => 'Varchar',
        'CurrencyRate' => 'Decimal(15,6)',
        'Address' => 'Varchar(250)',
        'Suburb' => 'Varchar(64)',
        'State' => 'Varchar(64)',
        'Postal' => 'Varchar(10)',
        'AccountName' => 'Varchar',
        'AccountNumber' => 'Varchar',
        'OwnerContact' => 'Varchar',
        'OwnerEmail' => 'Varchar'
    );
	
	private static $casting = array(
		'ConvertedAmount' => 'Currency'
	);
	
	function populateDefaults() {
        parent::populateDefaults();
        $this->Currency = SiteCurrencyConfig::current_site_currency();
        $this->CurrencyRate = 1.000000;
		return $this;
    }
	
	public function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);
		
		$labels['BankName'] = _t('BankWirePayoutMethod.BANK_NAME', 'Bank Name');
		$labels['BranchName'] = _t('BankWirePayoutMethod.BRANCH_NAME', 'Branch Name');
		$labels['Currency'] = _t('BankWirePayoutMethod.CURRENCY', 'Currency');
		$labels['CurrencyRate'] = _t('BankWirePayoutMethod.CURRENCY_RATE', 'Currency Rate');
		$labels['SwiftCode'] = _t('BankWirePayoutMethod.SWIFT_CODE', 'Swift Code / ABA');
		$labels['Address'] = _t('BankWirePayoutMethod.ADDRESS', 'Address');
		$labels['Suburb'] = _t('BankWirePayoutMethod.SUBURB', 'Suburb');
		$labels['State'] = _t('BankWirePayoutMethod.STATE', 'State');
		$labels['Postal'] = _t('BankWirePayoutMethod.POSTAL', 'Postal');
		$labels['Country'] = _t('BankWirePayoutMethod.COUNTRY', 'Country');
		$labels['AccountName'] = _t('BankWirePayoutMethod.ACCOUNT_NAME', 'Account Name');
		$labels['AccountNumber'] = _t('BankWirePayoutMethod.ACCOUNT_NUMBER', 'Account No.');
		$labels['OwnerContact'] = _t('BankWirePayoutMethod.OWNER_CONTACT', 'Owner Contact');
		$labels['OwnerEmail'] = _t('BankWirePayoutMethod.OWNER_EMAIL', 'Owner Email');
		$labels['ConvertedAmount'] = _t('BankWirePayoutMethod.CONVERTED_AMOUNT', 'Converted Amount');
		
		return $labels;	
	}
    
    function getPayoutFormFields($memberid){
        $fields = $this->getFrontendFields();
		$fields->removeByName('CurrencyRate');
        $fields->replaceField('Country', CountryDropdownField::create('Country', $this->fieldLabel('Country')));
		$fields->replaceField('BankName', BankNameField::create('BankName', $this->fieldLabel('BankName'))->setCountryField('BankWirePayoutMethod_Country'));
        $fields->replaceField('Currency', WithdrawalCurrencyField::create('Currency', $this->fieldLabel('Currency'))->setCountryField('BankWirePayoutMethod_Country'));
		$bank = Bank::get()->find('MemberID', (int)$memberid);
		foreach($fields as $field){
            $name = $field->getName();
            if($bank && $bank->hasField($name)){
                $value = $bank->getField($name);
                $field->setValue($value);
				if($value){
					if($field->class == 'LiteralField'){
						continue;
					}
					
					$field = $field->performReadonlyTransformation();
					if($field->hasMethod('setIncludeHiddenField')){
						$field->setIncludeHiddenField(true);
					}
					$fields->replaceField($name, $field);
				}
            }
			$field->setName(sprintf('%s_%s', 'BankWirePayoutMethod', $name));
        }

		$this->extend('updatePayoutFormFields', $fields, $memberid);
		
        return $fields;
    }

    function getPayoutFormRequirements($validator, $data){
        if($data['BankWirePayoutMethod_BankName'] == ''){
            $validator->validationError(
                'BankWirePayoutMethod_BankName',
                _t('BankWirePayoutMethod.BANK_NAME_REQUIRED', 'Bank Name is required'),
                'required'
            );
        }
		
		if($data['BankWirePayoutMethod_Currency'] == ''){
            $validator->validationError(
                'BankWirePayoutMethod_Currency',
                _t('BankWirePayoutMethod.CURRENCY_REQUIRED', 'Currency is required'),
                'required'
            );
        }
		
		if($data['BankWirePayoutMethod_Country'] == ''){
            $validator->validationError(
                'BankWirePayoutMethod_Country',
                _t('BankWirePayoutMethod.COUNTRY_REQUIRED', 'Country is required'),
                'required'
            );
        }
        
        if($data['BankWirePayoutMethod_AccountName'] == ''){
            $validator->validationError(
                'BankWirePayoutMethod_AccountName',
                _t('BankWirePayoutMethod.ACCOUNT_NAME_REQUIRED', 'Account Name is required'),
                'required'
            );
        }
        
        if($data['BankWirePayoutMethod_AccountNumber'] == ''){
            $validator->validationError(
                'BankWirePayoutMethod_AccountNumber',
                _t('BankWirePayoutMethod.ACCOUNT_NUMBER_REQUIRED', 'Account Number is required'),
                'required'
            );
        }
		
		$this->extend('updatePayoutFormRequirements', $validator, $data);

        return $validator;
    }

	function onBeforeWrite(){
		parent::onBeforeWrite();
		
		if(!$this->exists()){
			$obj = CurrencyRate::get()->filter('FromCurrency', SiteCurrencyConfig::current_site_currency())->filter('ToCurrency', $this->Currency)->sort('LastEdited', 'DESC')->first();
			if($obj && $obj->Rate > 0){
				$this->CurrencyRate = $obj->Rate;
			}
		}
	}
	
	function getConvertedAmount(){
		if($this->CurrencyRate > 0){
			return $this->Amount * $this->CurrencyRate;
		}
		
		return $this->Amount;
	}
	
	function getBankNameByLang(){
		if(class_exists('BankList')){
			if($bank = BankList::get()->filter('Code', $this->getField('BankName'))->find('Locale', i18n::get_locale())){
				return $bank->Title;
			}
		}

		return $this->getField('BankName');
	}
	
	function getCountryName() {
        $locale = i18n::get_locale();
        return Zend_Locale::getTranslation($this->Country, "country", $locale);
    }
}

?>