<?php
class DepositSubmissionStatusList extends DropdownList implements PermissionProvider {
    private static $singular_name = "Deposit Submission Status List";
    private static $plural_name = "Deposit Submission Status Lists";
    
    private static $default_records = array(
        array(
            'Code' => 'Pending',
            'Title' => 'Pending',
            'Sort' => 10,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Process',
            'Title' => 'Process',
            'Sort' => 20,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Partially Paid',
            'Title' => 'Partially Paid',
            'Sort' => 30,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Fully Paid',
            'Title' => 'Fully Paid',
            'Sort' => 40,
            'Locale' => 'en_US'
        ),
		array(
            'Code' => 'Approved',
            'Title' => 'Approved',
            'Sort' => 50,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Voided',
            'Title' => 'Voided',
            'Sort' => 60,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Canceled',
            'Title' => 'Canceled',
            'Sort' => 70,
            'Locale' => 'en_US'
        ),
        array(
            'Code' => 'Pending',
            'Title' => '等待处理',
            'Sort' => 80,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Process',
            'Title' => '正在处理',
            'Sort' => 90,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Partially Paid',
            'Title' => '部分付款',
            'Sort' => 100,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Fully Paid',
            'Title' => '付全款',
            'Sort' => 110,
            'Locale' => 'zh_CN'
        ),
		array(
            'Code' => 'Approved',
            'Title' => '已批准',
            'Sort' => 120,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Voided',
            'Title' => '已废止',
            'Sort' => 130,
            'Locale' => 'zh_CN'
        ),
        array(
            'Code' => 'Canceled',
            'Title' => '已取消',
            'Sort' => 140,
            'Locale' => 'zh_CN'
        )
    );

    /**
     *
     * @param Code
     * @return Title
     */
    static function get_title_by_code($code, $locale = null) {
    	if(!$locale) $locale = i18n::get_locale();
        if($kyc = DepositSubmissionStatusList::get()->filter('Active', 1)->filter('Locale', $locale)->find('Code', $code)) {
            return $kyc->Title;
        }
		return $code;
    }
}
?>
