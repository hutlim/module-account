<?php

class BonusConvertProduct extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Bonus Convert E-Product";
    private static $plural_name = "E-Bonus Convert E-Product";
    
    private static $extensions = array("AccountConvert");

	static function create_statement($data, $memberid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        }
		
        return BonusConvertProduct::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_BonusConvertProduct');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_BonusConvertProduct' => array(
                'name' => _t('BonusConvertProduct.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('BonusConvertProduct.PERMISSIONS_CATEGORY', 'E-Bonus Convert E-Product')
            )
        );
    }
}
?>