<?php

class PurchaseAccountTransfer extends DataObject implements PermissionProvider {
    private static $singular_name = "E-Purchase Transfer";
    private static $plural_name = "E-Purchase Transfers";
    
    private static $extensions = array("AccountTransfer");

	static function create_statement($data, $memberid, $transferid){
		if(!$memberid) {
            throw new Exception("Empty memberid");
        } else if(!$transferid) {
            throw new Exception("Empty transferid");
        }
		
        return PurchaseAccountTransfer::create()
        ->castedUpdate($data)
        ->setField('MemberID', $memberid)
        ->setField('TransferID', $transferid)
        ->write();
    }

    function canView($member = false) {
        $extended = $this->extendedCan(__FUNCTION__, $member);
        if($extended !== null) {
            return $extended;
        }
        return Permission::check('VIEW_PurchaseAccountTransfer');
    }

    function canEdit($member = false) {
        return false;
    }

    function canDelete($member = false) {
        return false;
    }

    function canCreate($member = false) {
        return false;
    }

    public function providePermissions() {
        return array(
            'VIEW_PurchaseAccountTransfer' => array(
                'name' => _t('PurchaseAccountTransfer.PERMISSION_VIEW', 'Allow view access right'),
                'category' => _t('PurchaseAccountTransfer.PERMISSIONS_CATEGORY', 'E-Purchase Transfer')
            )
        );
    }
}
?>