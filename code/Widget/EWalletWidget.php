<?php
class EWalletWidget extends Widget {
	private static $db = array(
		'ShowPurchaseAccountBalance' => 'Boolean',
		'ShowCashAccountBalance' => 'Boolean',
		'ShowBonusAccountBalance' => 'Boolean',
		'ShowUpgradeAccountBalance' => 'Boolean',
		'ShowProductAccountBalance' => 'Boolean'
	);

	private static $title = null; //don't show a title for this widget by default
	private static $cmsTitle = "EWallet Widget";
	private static $description = "Show ewallet details";

	function fieldLabels($includerelations = true) {
		$labels = parent::fieldLabels($includerelations);

		$labels['ShowPurchaseAccountBalance'] = _t('EWalletWidget.SHOW_PURCHASE_BALANCE', 'Show E-Purchase Account Balance?');
		$labels['ShowCashAccountBalance'] = _t('EWalletWidget.SHOW_CASH_BALANCE', 'Show E-Cash Account Balance?');
		$labels['ShowBonusAccountBalance'] = _t('EWalletWidget.SHOW_BONUS_BALANCE', 'Show E-Bonus Account Balance?');
		$labels['ShowUpgradeAccountBalance'] = _t('EWalletWidget.SHOW_UPGRADE_BALANCE', 'Show E-Upgrade Account Balance?');
		$labels['ShowProductAccountBalance'] = _t('EWalletWidget.SHOW_PRODUCT_BALANCE', 'Show E-Product Account Balance?');
		return $labels;	
	}
	
	function getCMSFields() {
		$fields = parent::getCMSFields();
		$fields->push(CheckboxField::create("ShowPurchaseAccountBalance", $this->fieldLabel('ShowPurchaseAccountBalance')));
		$fields->push(CheckboxField::create("ShowCashAccountBalance", $this->fieldLabel('ShowCashAccountBalance')));
		$fields->push(CheckboxField::create("ShowBonusAccountBalance", $this->fieldLabel('ShowBonusAccountBalance')));
		$fields->push(CheckboxField::create("ShowUpgradeAccountBalance", $this->fieldLabel('ShowUpgradeAccountBalance')));
		$fields->push(CheckboxField::create("ShowProductAccountBalance", $this->fieldLabel('ShowProductAccountBalance')));
		return $fields;
	}
}