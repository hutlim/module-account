<?php

class BonusTransferCashStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Transfer E-Cash Statement Page";
    private static $plural_name = "E-Bonus Transfer E-Cash Statement Pages";
    private static $default_parent = 'AccountTransferPage';
    
    private static $db = array();

    private static $has_one = array();
}

class BonusTransferCashStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('BonusTransferCashStatementPage.TRANSFERRED_ON', 'Transferred On'))
        );

        $field_list = array(
            'Created' => _t('BonusTransferCashStatementPage.TRANSFERRED_ON', 'Transferred On'),
            'Reference' => _t('BonusTransferCashStatementPage.REFERENCE', 'Reference'),
            'Transfer.Username' => _t('BonusTransferCashStatementPage.TRANSFER_USERNAME', 'Transfer Username'),
            'Remark' => _t('BonusTransferCashStatementPage.REMARK', 'Remark'),
            'Rate' => array('title' => _t('BonusTransferCashStatementPage.RATE', 'Rate'), 'classes' => 'text-center'),
            'Amount' => array('title' => _t('BonusTransferCashStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'BonusAccountTransferCashAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>