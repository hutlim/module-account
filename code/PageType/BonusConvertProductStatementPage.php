<?php

class BonusConvertProductStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Convert E-Product Statement Page";
    private static $plural_name = "E-Bonus Convert E-Product Statement Pages";
    private static $default_parent = 'AccountConvertPage';
    
    private static $db = array();

    private static $has_one = array();
}

class BonusConvertProductStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('BonusConvertProductStatementPage.CONVERTED_ON', 'Converted On'))
        );

        $field_list = array(
            'Created' => _t('BonusConvertProductStatementPage.CONVERTED_ON', 'Converted On'),
            'Type' => _t('BonusConvertProductStatementPage.TYPE', 'Type'),
            'Reference' => _t('BonusConvertProductStatementPage.REFERENCE', 'Reference'),
            'Rate' => _t('BonusConvertProductStatementPage.RATE', 'Rate'),
            'Amount' => array('title' => _t('BonusConvertProductStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'BonusConvertProduct', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>