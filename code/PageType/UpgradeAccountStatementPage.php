<?php
class UpgradeAccountStatementPage extends MemberPage {
	private static $singular_name = "E-Upgrade Account Statement Page";
    private static $plural_name = "E-Upgrade Account Statement Pages";
    private static $default_parent = 'MyEwalletPage';

    private static $db = array();

    private static $has_one = array();

}

class UpgradeAccountStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            HtmlEditorField_Readonly::create('UpgradeAccountBalance', '', sprintf('<b>%s</b>', _t('UpgradeAccountStatementPage.CURRENT_BALANCE', 'Current Balance: {balance}', 'Display upgrade account balance', array('balance' => Account::get('UpgradeAccount', $this->CurrentMember()->ID)->obj('Balance')->Nice())))),
            DateRangeField::create('Date', _t('UpgradeAccountStatementPage.DATE', 'Date')),
            singleton('UpgradeAccountType')->getDropdownField('Type', _t('UpgradeAccountStatementPage.TYPE', 'Type'))->setEmptyString(_t('UpgradeAccountStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Date' => _t('UpgradeAccountStatementPage.DATE', 'Date'),
            'TypeByLang' => _t('UpgradeAccountStatementPage.TYPE', 'Type'),
            'Description' => _t('UpgradeAccountStatementPage.TRANSACTION_DESCRIPTION', 'Description'),
            'Reference' => _t('UpgradeAccountStatementPage.REFERENCE', 'Reference'),
            'Credit' => array('title' => _t('UpgradeAccountStatementPage.IN', 'In ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Debit' => array('title' => _t('UpgradeAccountStatementPage.OUT', 'Out ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'ForwardBalance' => array('title' => _t('UpgradeAccountStatementPage.BALANCE', 'Balance ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Date' => 'SS_Datetime->Nice',
            'Credit' => 'Double->Nice',
            'Debit' => 'Double->Nice',
            'ForwardBalance' => 'Double->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'UpgradeAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
