<?php

class CashTransferStatementPage extends MemberPage {
	private static $singular_name = "E-Cash Transfer Statement Page";
    private static $plural_name = "E-Cash Transfer Statement Pages";
    private static $default_parent = 'AccountTransferPage';
    
    private static $db = array();

    private static $has_one = array();
}

class CashTransferStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('CashTransferStatementPage.TRANSFERRED_ON', 'Transferred On'))
        );

        $field_list = array(
            'Created' => _t('CashTransferStatementPage.TRANSFERRED_ON', 'Transferred On'),
            'Reference' => _t('CashTransferStatementPage.REFERENCE', 'Reference'),
            'Transfer.Username' => _t('CashTransferStatementPage.TRANSFER_USERNAME', 'Transfer Username'),
            'Remark' => _t('CashTransferStatementPage.REMARK', 'Remark'),
            'Amount' => array('title' => _t('CashTransferStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'CashAccountTransfer', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>