<?php

class DepositStatementPage extends MemberPage {
	private static $singular_name = "E-Purchase Deposit Statement Page";
    private static $plural_name = "E-Purchase Deposit Statement Pages";
    private static $default_parent = 'MyEwalletPage';
    
    private static $db = array();

    private static $has_one = array();
}

class DepositStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form',
        'view_statement',
        'download_statement',
        'print_statement'
    );

    function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('DepositStatementPage.DEPOSITED', 'Deposited')),
            DropdownField::create('Status', _t('DepositStatementPage.STATUS', 'Status'), DepositSubmission::create()->obj('Status')->enumValues())->setEmptyString(_t('DepositStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Created' => _t('DepositStatementPage.DEPOSITED', 'Deposited'),
            'PaymentReceipt.Reference' => _t('DepositStatementPage.REFERENCE', 'Reference'),
            'StatusWithRemark' => _t('DepositStatementPage.STATUS', 'Status'),
            'Amount' => array('title' => _t('DepositStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Action' => array('title' => '', 'classes' => 'text-center')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'DepositSubmission', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }

    function view_statement() {
        $id = (int)$this->request->param('ID');
        if($deposit_submission = DepositSubmission::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id)){
	        $deposit_submission->PrintLink = $this->Link('print_statement/' . $id);
			if($deposit_submission->config()->allowed_pdf && class_exists('SS_DOMPDF')){
				$deposit_submission->PDFLink = $this->Link('download_statement/' . $id);
			}
	        return $deposit_submission->FullDetailHTML;
        }
		
		return $this->httpError('404');
    }

    function download_statement() {
    	$id = (int)$this->request->param('ID');
		$deposit_submission = DepositSubmission::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id);
		if($deposit_submission && $deposit_submission->config()->allowed_pdf && class_exists('SS_DOMPDF')){
			return $deposit_submission->ViewPDF();
		}
		
		return $this->httpError('404');
    }

    function print_statement() {
        $id = (int)$this->request->param('ID');
		if($deposit_submission = DepositSubmission::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id)){
        	return $deposit_submission->ViewPrint();
		}
		
		return $this->httpError('404');
    }
}
?>