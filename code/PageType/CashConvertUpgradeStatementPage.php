<?php

class CashConvertUpgradeStatementPage extends MemberPage {
	private static $singular_name = "E-Cash Convert E-Upgrade Statement Page";
    private static $plural_name = "E-Cash Convert E-Upgrade Statement Pages";
    private static $default_parent = 'AccountConvertPage';
    
    private static $db = array();

    private static $has_one = array();
}

class CashConvertUpgradeStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('CashConvertUpgradeStatementPage.CONVERTED_ON', 'Converted On'))
        );

        $field_list = array(
            'Created' => _t('CashConvertUpgradeStatementPage.CONVERTED_ON', 'Converted On'),
            'Type' => _t('CashConvertUpgradeStatementPage.TYPE', 'Type'),
            'Reference' => _t('CashConvertUpgradeStatementPage.REFERENCE', 'Reference'),
            'Rate' => _t('CashConvertUpgradeStatementPage.RATE', 'Rate'),
            'Amount' => array('title' => _t('CashConvertUpgradeStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'CashConvertUpgrade', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>