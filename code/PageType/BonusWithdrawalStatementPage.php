<?php
class BonusWithdrawalStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Withdrawal Statement Page";
    private static $plural_name = "E-Bonus Withdrawal Statement Pages";
    private static $default_parent = 'AccountWithdrawalPage';

    private static $db = array();

    private static $has_one = array();

}

class BonusWithdrawalStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'Form',
        'view_statement',
        'download_statement',
        'print_statement'
    );

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('BonusWithdrawalStatementPage.SUBMITTED', 'Submitted')),
            singleton('BonusAccountWithdrawal')->obj('Status')->formField(_t('BonusWithdrawalStatementPage.STATUS', 'Status'), 'Status')->setEmptyString(_t('BonusWithdrawalStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Created' => _t('BonusWithdrawalStatementPage.SUBMITTED', 'Submitted'),
            'PayoutMethod.Title' => _t('BonusWithdrawalStatementPage.METHOD', 'Method'),
            'PayoutMethod.Reference' => _t('BonusWithdrawalStatementPage.REFERENCE', 'Reference'),
            'StatusWithRemark' => _t('BonusWithdrawalStatementPage.STATUS', 'Status'),
            'Amount' => array('title' => _t('BonusWithdrawalStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Action' => array('title' => '', 'classes' => 'text-center')
        );
		
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'BonusAccountWithdrawal', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }

    function view_statement() {
        $id = (int)$this->request->param('ID');
        if($withdrawal_submission = BonusAccountWithdrawal::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id)){
        	$withdrawal_submission->PrintLink = $this->Link('print_statement/' . $id);
			if($withdrawal_submission->config()->allowed_pdf && class_exists('SS_DOMPDF')){
				$withdrawal_submission->PDFLink = $this->Link('download_statement/' . $id);
			}
        	return $withdrawal_submission->FullDetailHTML;
        }
		
		return $this->httpError('404');
    }

    function download_statement() {
		$id = (int)$this->request->param('ID');
		$withdrawal_submission = BonusAccountWithdrawal::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id);
        if($withdrawal_submission && $withdrawal_submission->config()->allowed_pdf && class_exists('SS_DOMPDF')){
			return $withdrawal_submission->ViewPDF();
        }
		
		return $this->httpError('404');
    }

    function print_statement() {
        $id = (int)$this->request->param('ID');
		if($withdrawal_submission = BonusAccountWithdrawal::get()->filter('MemberID', $this->CurrentMember()->ID)->byID($id)){
        	return $withdrawal_submission->ViewPrint();
		}
		
		return $this->httpError('404');
    }

}
