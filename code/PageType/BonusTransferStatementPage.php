<?php

class BonusTransferStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Transfer Statement Page";
    private static $plural_name = "E-Bonus Transfer Statement Pages";
    private static $default_parent = 'AccountTransferPage';
    
    private static $db = array();

    private static $has_one = array();
}

class BonusTransferStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('BonusTransferStatementPage.TRANSFERRED_ON', 'Transferred On'))
        );

        $field_list = array(
            'Created' => _t('BonusTransferStatementPage.TRANSFERRED_ON', 'Transferred On'),
            'Reference' => _t('BonusTransferStatementPage.REFERENCE', 'Reference'),
            'Transfer.Username' => _t('BonusTransferStatementPage.TRANSFER_USERNAME', 'Transfer Username'),
            'Remark' => _t('BonusTransferStatementPage.REMARK', 'Remark'),
            'Amount' => array('title' => _t('BonusTransferStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'BonusAccountTransfer', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>