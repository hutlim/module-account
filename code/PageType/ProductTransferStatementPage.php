<?php

class ProductTransferStatementPage extends MemberPage {
	private static $singular_name = "E-Product Transfer Statement Page";
    private static $plural_name = "E-Product Transfer Statement Pages";
    private static $default_parent = 'AccountTransferPage';
    
    private static $db = array();

    private static $has_one = array();
}

class ProductTransferStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('ProductTransferStatementPage.TRANSFERRED_ON', 'Transferred On'))
        );

        $field_list = array(
            'Created' => _t('ProductTransferStatementPage.TRANSFERRED_ON', 'Transferred On'),
            'Reference' => _t('ProductTransferStatementPage.REFERENCE', 'Reference'),
            'Transfer.Username' => _t('ProductTransferStatementPage.TRANSFER_USERNAME', 'Transfer Username'),
            'Remark' => _t('ProductTransferStatementPage.REMARK', 'Remark'),
            'Amount' => array('title' => _t('ProductTransferStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'ProductAccountTransfer', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>