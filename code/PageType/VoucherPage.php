<?php
class VoucherPage extends MemberOverviewPage {

    private static $db = array();

    private static $has_one = array();

}

class VoucherPage_Controller extends MemberOverviewPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'Form'
    );

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('VoucherPage.CREATED_ON', 'Created On')),
            singleton('Voucher')->obj('Status')->formField(_t('VoucherPage.STATUS', 'Status'), 'Status')->setEmptyString(_t('VoucherPage.ALL', 'All'))
        );

        $field_list = array(
            'Created' => _t('VoucherPage.CREATED_ON', 'Created On'),
            'Code' => _t('VoucherPage.CODE', 'Code'),
            'Amount' => array('title' => _t('VoucherPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Status' => _t('VoucherPage.STATUS', 'Status'),
            'UsedBy.Username' => _t('VoucherPage.USED_BY', 'Used By')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'Voucher', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
