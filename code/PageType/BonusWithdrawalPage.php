<?php
class BonusWithdrawalPage extends MemberPage {
	private static $singular_name = "E-Bonus Withdrawal Page";
    private static $plural_name = "E-Bonus Withdrawal Pages";
    private static $default_parent = 'AccountWithdrawalPage';

    private static $db = array();

    private static $has_one = array();

}

class BonusWithdrawalPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'Form'
    );

    public function init() {
        parent::init();
    }

    function Form() {
    	if(!Distributor::get_use_kyc()){
    		$fields = BonusAccountWithdrawal::create()->getWithdrawFormFields($this->CurrentMember()->ID);
	        $actions = FieldList::create(FormAction::create("doSave", _t('BonusWithdrawalPage.BUTTONSUBMIT', 'Submit')));
	        $validator = PayoutMethod_Validator::create('Amount');
		}
		else{
	    	if($this->CurrentMember()->KYCStatus == 'Approved'){
		        $fields = BonusAccountWithdrawal::create()->getWithdrawFormFields($this->CurrentMember()->ID);
		        $actions = FieldList::create(FormAction::create("doSave", _t('BonusWithdrawalPage.BUTTONSUBMIT', 'Submit')));
		        $validator = PayoutMethod_Validator::create('Amount');
			}
			else if($this->CurrentMember()->KYCStatus == 'Rejected'){
		        $fields = FieldList::create(
					LiteralField::create('KYCAlert', sprintf('<div class="alert alert-danger" role="alert"><p><span class="glyphicon glyphicon-warning-sign"></span> %s</p><p><a href="%s" class="alert-link">%s</a></p><p><a href="%s" class="alert-link">%s</a></p></div>', _t('BonusWithdrawalPage.KYC_REJECT', 'Your KYC verification has been rejected. Please check all your Bank Account Details and Bank Account Proof, Proof of Residence and Passport / Photo ID.'), $this->BankDetailsLink, _t('BonusWithdrawalPage.BUTTONCHECKBANKDETAILS', 'Click here to check Bank Account Details.'), $this->KYCVerificationLink, _t('BonusWithdrawalPage.BUTTONCHECKROOFDOCUMENT', 'Click here to check Proof of Document.')))
				);
				$actions = FieldList::create();
				$validator = null;
			}
			else if($this->CurrentMember()->KYCStatus == 'Process'){
		        $fields = FieldList::create(
					LiteralField::create('KYCAlert', sprintf('<div class="alert alert-info" role="alert"><p><span class="glyphicon glyphicon-info-sign"></span> %s</p><p><a href="%s" class="alert-link">%s</a></p></div>', _t('BonusWithdrawalPage.KYC_PROCESS', 'Your proof of document is on process verification. Please be patient for the approval of your KYC verification.'), $this->KYCVerificationLink, _t('BonusWithdrawalPage.BUTTONCHECKKYCSTATUS', 'Click here to check your KYC verification status.')))
				);
				$actions = FieldList::create();
				$validator = null;
			}
			else {
				$fields = FieldList::create(
					LiteralField::create('KYCAlert', sprintf('<div class="alert alert-danger" role="alert"><p><span class="glyphicon glyphicon-warning-sign"></span> %s</p><p><a href="%s" class="alert-link">%s</a></p><p><a href="%s" class="alert-link">%s</a></p></div>', _t('BonusWithdrawalPage.KYC_ALERT', 'You are not allowed to proceed withdrawal. You need to update all your Bank Account Details and upload Bank Account Proof, Proof of Residence and Passport / Photo ID.'), $this->BankDetailsLink, _t('BonusWithdrawalPage.BUTTONUPDATEBANKDETAILS', 'Click here to update Bank Account Details.'), $this->KYCVerificationLink, _t('BonusWithdrawalPage.BUTTONUPLOADPROOFDOCUMENT', 'Click here to upload Proof of Document.')))
				);
				$actions = FieldList::create();
				$validator = null;
			}
		}
        return Form::create($this, 'Form', $fields, $actions, $validator);
    }

    function doSave($data, $form) {
        try {
            DB::getConn()->transactionStart();
            $withdrawid = BonusAccountWithdrawal::create_withdrawal_submission($data, $this->CurrentMember()->ID);
            DB::getConn()->transactionEnd();
            return $this->redirect($this->BonusWithdrawalStatementLink);
        }
        catch(ValidationException $e) {
            DB::getConn()->transactionRollback();
            SS_Log::log(new Exception(print_r($e->getMessage(), true)), SS_Log::NOTICE);
            $form->sessionMessage($e->getMessage(), 'bad');
        }
        return $this->redirectBack();
    }
}
