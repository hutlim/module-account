<?php
class LoadDepositPage extends MemberPage {
	private static $singular_name = "E-Purchase Load Deposit Page";
    private static $plural_name = "E-Purchase Load Deposit Pages";
    private static $default_parent = 'MyEwalletPage';

    private static $db = array();

    private static $has_one = array();

}

class LoadDepositPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array(
        'Form',
        'view_statement',
        'download_statement',
        'print_statement'
    );

    public function init() {
        parent::init();
    }

    function Form() {
        return DepositForm::create($this, 'Form');
    }
}
