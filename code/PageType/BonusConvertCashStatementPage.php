<?php

class BonusConvertCashStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Convert E-Cash Statement Page";
    private static $plural_name = "E-Bonus Convert E-Cash Statement Pages";
    private static $default_parent = 'AccountConvertPage';
    
    private static $db = array();

    private static $has_one = array();
}

class BonusConvertCashStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('BonusConvertCashStatementPage.CONVERTED_ON', 'Converted On'))
        );

        $field_list = array(
            'Created' => _t('BonusConvertCashStatementPage.CONVERTED_ON', 'Converted On'),
            'Type' => _t('BonusConvertCashStatementPage.TYPE', 'Type'),
            'Reference' => _t('BonusConvertCashStatementPage.REFERENCE', 'Reference'),
            'Rate' => _t('BonusConvertCashStatementPage.RATE', 'Rate'),
            'Amount' => array('title' => _t('BonusConvertCashStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'BonusConvertCash', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>