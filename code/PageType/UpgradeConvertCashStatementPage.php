<?php

class UpgradeConvertCashStatementPage extends MemberPage {
	private static $singular_name = "E-Upgrade Convert E-Cash Statement Page";
    private static $plural_name = "E-Upgrade Convert E-Cash Statement Pages";
    private static $default_parent = 'AccountConvertPage';
    
    private static $db = array();

    private static $has_one = array();
}

class UpgradeConvertCashStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('UpgradeConvertCashStatementPage.CONVERTED_ON', 'Converted On'))
        );

        $field_list = array(
            'Created' => _t('UpgradeConvertCashStatementPage.CONVERTED_ON', 'Converted On'),
            'Type' => _t('UpgradeConvertCashStatementPage.TYPE', 'Type'),
            'Reference' => _t('UpgradeConvertCashStatementPage.REFERENCE', 'Reference'),
            'Rate' => _t('UpgradeConvertCashStatementPage.RATE', 'Rate'),
            'Amount' => array('title' => _t('UpgradeConvertCashStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'UpgradeConvertCash', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>