<?php
class ProductAccountStatementPage extends MemberPage {
	private static $singular_name = "E-Product Account Statement Page";
    private static $plural_name = "E-Product Account Statement Pages";
	
    private static $default_parent = 'MyEwalletPage';

    private static $db = array();

    private static $has_one = array();

}

class ProductAccountStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            HtmlEditorField_Readonly::create('ProductAccountBalance', '', sprintf('<b>%s</b>', _t('ProductAccountStatementPage.CURRENT_BALANCE', 'Current Balance: {balance}', 'Display E-Product account balance', array('balance' => Account::get('ProductAccount', $this->CurrentMember()->ID)->obj('Balance')->Nice())))),
            DateRangeField::create('Date', _t('ProductAccountStatementPage.DATE', 'Date')),
            singleton('ProductAccountType')->getDropdownField('Type', _t('ProductAccountStatementPage.TYPE', 'Type'))->setEmptyString(_t('ProductAccountStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Date' => _t('ProductAccountStatementPage.DATE', 'Date'),
            'TypeByLang' => _t('ProductAccountStatementPage.TYPE', 'Type'),
            'Description' => _t('ProductAccountStatementPage.TRANSACTION_DESCRIPTION', 'Description'),
            'Reference' => _t('ProductAccountStatementPage.REFERENCE', 'Reference'),
            'Credit' => array('title' => _t('ProductAccountStatementPage.IN', 'In ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Debit' => array('title' => _t('ProductAccountStatementPage.OUT', 'Out ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'ForwardBalance' => array('title' => _t('ProductAccountStatementPage.BALANCE', 'Balance ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Date' => 'SS_Datetime->Nice',
            'Credit' => 'Double->Nice',
            'Debit' => 'Double->Nice',
            'ForwardBalance' => 'Double->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'ProductAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
