<?php

class CashTransferPurchaseStatementPage extends MemberPage {
	private static $singular_name = "E-Cash Transfer E-Purchase Statement Page";
    private static $plural_name = "E-Cash Transfer E-Purchase Statement Pages";
    private static $default_parent = 'AccountTransferPage';
    
    private static $db = array();

    private static $has_one = array();
}

class CashTransferPurchaseStatementPage_Controller extends MemberPage_Controller {
    private static $allowed_actions = array(
        'Form'
    );

    function Form() {
        $fields = FieldList::create(
            DateRangeField::create('Created', _t('CashTransferPurchaseStatementPage.TRANSFERRED_ON', 'Transferred On'))
        );

        $field_list = array(
            'Created' => _t('CashTransferPurchaseStatementPage.TRANSFERRED_ON', 'Transferred On'),
            'Reference' => _t('CashTransferPurchaseStatementPage.REFERENCE', 'Reference'),
            'Transfer.Username' => _t('CashTransferPurchaseStatementPage.TRANSFER_USERNAME', 'Transfer Username'),
            'Remark' => _t('CashTransferPurchaseStatementPage.REMARK', 'Remark'),
            'Rate' => array('title' => _t('CashTransferPurchaseStatementPage.RATE', 'Rate'), 'classes' => 'text-center'),
            'Amount' => array('title' => _t('CashTransferPurchaseStatementPage.AMOUNT', 'Amount ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Created' => 'SS_Datetime->Nice',
            'Amount' => 'Double->Nice'
        );

        return DataListSearchForm::create($this, 'Form', 'CashAccountTransferPurchaseAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
?>