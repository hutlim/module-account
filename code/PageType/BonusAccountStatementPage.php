<?php
class BonusAccountStatementPage extends MemberPage {
	private static $singular_name = "E-Bonus Account Statement Page";
    private static $plural_name = "E-Bonus Account Statement Pages";
	
    private static $default_parent = 'MyEwalletPage';

    private static $db = array();

    private static $has_one = array();

}

class BonusAccountStatementPage_Controller extends MemberPage_Controller {

    /**
     * An array of actions that can be accessed via a request. Each array element
     * should be an action name, and the
     * permissions or conditions required to allow the user to access it.
     *
     * <code>
     * array (
     *     'action', // anyone can access this action
     *     'action' => true, // same as above
     *     'action' => 'ADMIN', // you must have ADMIN permissions to access this
     * action
     *     'action' => '->checkAction' // you can only access this action if
     * $this->checkAction() returns true
     * );
     * </code>
     *
     * @var array
     */
    private static $allowed_actions = array('Form');

    public function init() {
        parent::init();
    }

    function Form() {
        $fields = FieldList::create(
            HtmlEditorField_Readonly::create('BonusAccountBalance', '', sprintf('<b>%s</b>', _t('BonusAccountStatementPage.CURRENT_BALANCE', 'Current Balance: {balance}', 'Display E-Bonus account balance', array('balance' => Account::get('BonusAccount', $this->CurrentMember()->ID)->obj('Balance')->Nice())))),
            DateRangeField::create('Date', _t('BonusAccountStatementPage.DATE', 'Date')),
            singleton('BonusAccountType')->getDropdownField('Type', _t('BonusAccountStatementPage.TYPE', 'Type'))->setEmptyString(_t('BonusAccountStatementPage.ALL', 'All'))
        );

        $field_list = array(
            'Date' => _t('BonusAccountStatementPage.DATE', 'Date'),
            'TypeByLang' => _t('BonusAccountStatementPage.TYPE', 'Type'),
            'Description' => _t('BonusAccountStatementPage.TRANSACTION_DESCRIPTION', 'Description'),
            'Reference' => _t('BonusAccountStatementPage.REFERENCE', 'Reference'),
            'Credit' => array('title' => _t('BonusAccountStatementPage.IN', 'In ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'Debit' => array('title' => _t('BonusAccountStatementPage.OUT', 'Out ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right'),
            'ForwardBalance' => array('title' => _t('BonusAccountStatementPage.BALANCE', 'Balance ({currency})', '', array('currency' => SiteCurrencyConfig::current_site_currency())), 'classes' => 'text-right')
        );
        $casting_list = array(
            'Date' => 'SS_Datetime->Nice',
            'Credit' => 'Double->Nice',
            'Debit' => 'Double->Nice',
            'ForwardBalance' => 'Double->Nice'
        );
        
        return DataListSearchForm::create($this, 'Form', 'BonusAccount', array('MemberID' => $this->CurrentMember()->ID), $fields)->setDataFieldList($field_list)->setFieldCasting($casting_list);
    }
}
