<?php

class Account_MemberExtension extends DataExtension {
	private static $casting = array(
		'PurchaseAccountBalance' => 'Currency',
		'CashAccountBalance' => 'Currency',
        'BonusAccountBalance' => 'Currency',
        'UpgradeAccountBalance' => 'Currency',
        'ProductAccountBalance' => 'Currency'
    );
	
    function augmentWrite(&$manipulation) {
        $tables = array_keys($manipulation);
        $version_table = array();
        foreach($tables as $table) {
            $baseDataClass = ClassInfo::baseDataClass($table);

            $isRootClass = ($table == $baseDataClass);
            if($isRootClass && $manipulation[$table]['command'] == 'insert' && isset($manipulation[$table]['id']) && $manipulation[$table]['id']) {
            	DB::query("INSERT INTO PurchaseAccountBalance (MemberID) VALUES (".$manipulation[$table]['id'].")");
				DB::query("INSERT INTO CashAccountBalance (MemberID) VALUES (".$manipulation[$table]['id'].")");
                DB::query("INSERT INTO BonusAccountBalance (MemberID) VALUES (".$manipulation[$table]['id'].")");
				DB::query("INSERT INTO UpgradeAccountBalance (MemberID) VALUES (".$manipulation[$table]['id'].")");
				DB::query("INSERT INTO ProductAccountBalance (MemberID) VALUES (".$manipulation[$table]['id'].")");
            }
        }
    }
	
	function requireDefaultRecords() {
		$result = DB::query("SELECT * FROM Member WHERE ID NOT IN (SELECT MemberID FROM PurchaseAccountBalance)");
		foreach($result as $item){
			DB::query("INSERT INTO PurchaseAccountBalance (MemberID) VALUES (".$item['ID'].")");
		}
		
		$result = DB::query("SELECT * FROM Member WHERE ID NOT IN (SELECT MemberID FROM CashAccountBalance)");
		foreach($result as $item){
			DB::query("INSERT INTO CashAccountBalance (MemberID) VALUES (".$item['ID'].")");
		}
		
		$result = DB::query("SELECT * FROM Member WHERE ID NOT IN (SELECT MemberID FROM BonusAccountBalance)");
		foreach($result as $item){
			DB::query("INSERT INTO BonusAccountBalance (MemberID) VALUES (".$item['ID'].")");
		}
		
		$result = DB::query("SELECT * FROM Member WHERE ID NOT IN (SELECT MemberID FROM UpgradeAccountBalance)");
		foreach($result as $item){
			DB::query("INSERT INTO UpgradeAccountBalance (MemberID) VALUES (".$item['ID'].")");
		}
		
		$result = DB::query("SELECT * FROM Member WHERE ID NOT IN (SELECT MemberID FROM ProductAccountBalance)");
		foreach($result as $item){
			DB::query("INSERT INTO ProductAccountBalance (MemberID) VALUES (".$item['ID'].")");
		}
	}
	
	function getCashAccountBalance(){
		return Account::get('CashAccount', $this->owner->ID)->getBalance();
	}
	
	function getBonusAccountBalance(){
		return Account::get('BonusAccount', $this->owner->ID)->getBalance();
	}
	
	function getPurchaseAccountBalance(){
		return Account::get('PurchaseAccount', $this->owner->ID)->getBalance();
	}
	
	function getUpgradeAccountBalance(){
		return Account::get('UpgradeAccount', $this->owner->ID)->getBalance();
	}
	
	function getProductAccountBalance(){
		return Account::get('ProductAccount', $this->owner->ID)->getBalance();
	}
}
?>