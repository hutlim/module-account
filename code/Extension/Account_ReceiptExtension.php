<?php

class Account_ReceiptExtension extends DataExtension {
    function onAfterWrite() {
    	if($this->owner->isChanged('Status') && $this->owner->Status){
            if($deposit_submission = DepositSubmission::get()->find('PaymentReceiptID', $this->owner->ID)){
                DepositSubmission::update_status($deposit_submission->ID, $this->owner->Status, $this->owner->Remark);
            }
        }
    }

}
