<?php

class Account_ControllerExtension extends Extension {
	// Purchase Account
	function getPurchaseAccountStatement(){
        $page = PurchaseAccountStatementPage::get()->find('ClassName', 'PurchaseAccountStatementPage');
        if($page) return $page;
    }
    
    function getPurchaseAccountStatementLink() {
        $page = $this->getPurchaseAccountStatement();
        if($page) return $page->Link();
    }
	
	function getPurchaseDeposit(){
        $page = LoadDepositPage::get()->find('ClassName', 'LoadDepositPage');
        if($page) return $page;
    }
    
    function getPurchaseDepositLink() {
        $page = $this->getPurchaseDeposit();
        if($page) return $page->Link();
    }
	
    function getPurchaseDepositStatement(){
        $page = DepositStatementPage::get()->find('ClassName', 'DepositStatementPage');
        if($page) return $page;
    }
    
    function getPurchaseDepositStatementLink() {
        $page = $this->getPurchaseDepositStatement();
        if($page) return $page->Link();
    }
	
	function getPurchaseTransfer(){
        $page = PurchaseTransferPage::get()->find('ClassName', 'PurchaseTransferPage');
        if($page) return $page;
    }
    
    function getPurchaseTransferLink() {
        $page = $this->getPurchaseTransfer();
        if($page) return $page->Link();
    }
	
    function getPurchaseTransferStatement(){
        $page = PurchaseTransferStatementPage::get()->find('ClassName', 'PurchaseTransferStatementPage');
        if($page) return $page;
    }
    
    function getPurchaseTransferStatementLink() {
        $page = $this->getPurchaseTransferStatement();
        if($page) return $page->Link();
    }
	//
	
	// Cash Account
	function getCashAccountStatement(){
        $page = CashAccountStatementPage::get()->find('ClassName', 'CashAccountStatementPage');
        if($page) return $page;
    }
    
    function getCashAccountStatementLink() {
        $page = $this->getCashAccountStatement();
        if($page) return $page->Link();
    }
	
	function getCashConvertPurchase(){
        $page = CashConvertPurchasePage::get()->find('ClassName', 'CashConvertPurchasePage');
        if($page) return $page;
    }
    
    function getCashConvertPurchaseLink() {
        $page = $this->getCashConvertPurchase();
        if($page) return $page->Link();
    }
	
    function getCashConvertPurchaseStatement(){
        $page = CashConvertPurchaseStatementPage::get()->find('ClassName', 'CashConvertPurchaseStatementPage');
        if($page) return $page;
    }
    
    function getCashConvertPurchaseStatementLink() {
        $page = $this->getCashConvertPurchaseStatement();
        if($page) return $page->Link();
    }
	
	function getCashConvertUpgrade(){
        $page = CashConvertUpgradePage::get()->find('ClassName', 'CashConvertUpgradePage');
        if($page) return $page;
    }
    
    function getCashConvertUpgradeLink() {
        $page = $this->getCashConvertUpgrade();
        if($page) return $page->Link();
    }
	
    function getCashConvertUpgradeStatement(){
        $page = CashConvertUpgradeStatementPage::get()->find('ClassName', 'CashConvertUpgradeStatementPage');
        if($page) return $page;
    }
    
    function getCashConvertUpgradeStatementLink() {
        $page = $this->getCashConvertUpgradeStatement();
        if($page) return $page->Link();
    }
	
	function getCashTransfer(){
        $page = CashTransferPage::get()->find('ClassName', 'CashTransferPage');
        if($page) return $page;
    }
    
    function getCashTransferLink() {
        $page = $this->getCashTransfer();
        if($page) return $page->Link();
    }
	
    function getCashTransferStatement(){
        $page = CashTransferStatementPage::get()->find('ClassName', 'CashTransferStatementPage');
        if($page) return $page;
    }
    
    function getCashTransferStatementLink() {
        $page = $this->getCashTransferStatement();
        if($page) return $page->Link();
    }
	
	function getCashTransferPurchase(){
        $page = CashTransferPurchasePage::get()->find('ClassName', 'CashTransferPurchasePage');
        if($page) return $page;
    }
    
    function getCashTransferPurchaseLink() {
        $page = $this->getCashTransferPurchase();
        if($page) return $page->Link();
    }
	
    function getCashTransferPurchaseStatement(){
        $page = CashTransferPurchaseStatementPage::get()->find('ClassName', 'CashTransferPurchaseStatementPage');
        if($page) return $page;
    }
    
    function getCashTransferPurchaseStatementLink() {
        $page = $this->getCashTransferPurchaseStatement();
        if($page) return $page->Link();
    }
	
	function getCashWithdrawal(){
        $page = CashWithdrawalPage::get()->find('ClassName', 'CashWithdrawalPage');
        if($page) return $page;
    }
    
    function getCashWithdrawalLink() {
        $page = $this->getCashWithdrawal();
        if($page) return $page->Link();
    }
	
    function getCashWithdrawalStatement(){
        $page = CashWithdrawalStatementPage::get()->find('ClassName', 'CashWithdrawalStatementPage');
        if($page) return $page;
    }
    
    function getCashWithdrawalStatementLink() {
        $page = $this->getCashWithdrawalStatement();
        if($page) return $page->Link();
    }
	//
	
	// Bonus Account
	function getBonusAccountStatement(){
        $page = BonusAccountStatementPage::get()->find('ClassName', 'BonusAccountStatementPage');
        if($page) return $page;
    }
    
    function getBonusAccountStatementLink() {
        $page = $this->getBonusAccountStatement();
        if($page) return $page->Link();
    }
	
	function getBonusConvertCash(){
        $page = BonusConvertCashPage::get()->find('ClassName', 'BonusConvertCashPage');
        if($page) return $page;
    }
    
    function getBonusConvertCashLink() {
        $page = $this->getBonusConvertCash();
        if($page) return $page->Link();
    }
	
    function getBonusConvertCashStatement(){
        $page = BonusConvertCashStatementPage::get()->find('ClassName', 'BonusConvertCashStatementPage');
        if($page) return $page;
    }
    
    function getBonusConvertCashStatementLink() {
        $page = $this->getBonusConvertCashStatement();
        if($page) return $page->Link();
    }
	
	function getBonusConvertProduct(){
        $page = BonusConvertProductPage::get()->find('ClassName', 'BonusConvertProductPage');
        if($page) return $page;
    }
    
    function getBonusConvertProductLink() {
        $page = $this->getBonusConvertProduct();
        if($page) return $page->Link();
    }
	
    function getBonusConvertProductStatement(){
        $page = BonusConvertProductStatementPage::get()->find('ClassName', 'BonusConvertProductStatementPage');
        if($page) return $page;
    }
    
    function getBonusConvertProductStatementLink() {
        $page = $this->getBonusConvertProductStatement();
        if($page) return $page->Link();
    }
	
	function getBonusConvertPurchase(){
        $page = BonusConvertPurchasePage::get()->find('ClassName', 'BonusConvertPurchasePage');
        if($page) return $page;
    }
    
    function getBonusConvertPurchaseLink() {
        $page = $this->getBonusConvertPurchase();
        if($page) return $page->Link();
    }
	
    function getBonusConvertPurchaseStatement(){
        $page = BonusConvertPurchaseStatementPage::get()->find('ClassName', 'BonusConvertPurchaseStatementPage');
        if($page) return $page;
    }
    
    function getBonusConvertPurchaseStatementLink() {
        $page = $this->getBonusConvertPurchaseStatement();
        if($page) return $page->Link();
    }
	
	function getBonusConvertUpgrade(){
        $page = BonusConvertUpgradePage::get()->find('ClassName', 'BonusConvertUpgradePage');
        if($page) return $page;
    }
    
    function getBonusConvertUpgradeLink() {
        $page = $this->getBonusConvertUpgrade();
        if($page) return $page->Link();
    }
	
    function getBonusConvertUpgradeStatement(){
        $page = BonusConvertUpgradeStatementPage::get()->find('ClassName', 'BonusConvertUpgradeStatementPage');
        if($page) return $page;
    }
    
    function getBonusConvertUpgradeStatementLink() {
        $page = $this->getBonusConvertUpgradeStatement();
        if($page) return $page->Link();
    }
	
	function getBonusTransferCash(){
        $page = BonusTransferCashPage::get()->find('ClassName', 'BonusTransferCashPage');
        if($page) return $page;
    }
    
    function getBonusTransferCashLink() {
        $page = $this->getBonusTransferCash();
        if($page) return $page->Link();
    }
	
    function getBonusTransferCashStatement(){
        $page = BonusTransferCashStatementPage::get()->find('ClassName', 'BonusTransferCashStatementPage');
        if($page) return $page;
    }
    
    function getBonusTransferCashStatementLink() {
        $page = $this->getBonusTransferCashStatement();
        if($page) return $page->Link();
    }
	
	function getBonusTransfer(){
        $page = BonusTransferPage::get()->find('ClassName', 'BonusTransferPage');
        if($page) return $page;
    }
    
    function getBonusTransferLink() {
        $page = $this->getBonusTransfer();
        if($page) return $page->Link();
    }
	
    function getBonusTransferStatement(){
        $page = BonusTransferStatementPage::get()->find('ClassName', 'BonusTransferStatementPage');
        if($page) return $page;
    }
    
    function getBonusTransferStatementLink() {
        $page = $this->getBonusTransferStatement();
        if($page) return $page->Link();
    }
	
	function getBonusWithdrawal(){
        $page = BonusWithdrawalPage::get()->find('ClassName', 'BonusWithdrawalPage');
        if($page) return $page;
    }
    
    function getBonusWithdrawalLink() {
        $page = $this->getBonusWithdrawal();
        if($page) return $page->Link();
    }
	
    function getBonusWithdrawalStatement(){
        $page = BonusWithdrawalStatementPage::get()->find('ClassName', 'BonusWithdrawalStatementPage');
        if($page) return $page;
    }
    
    function getBonusWithdrawalStatementLink() {
        $page = $this->getBonusWithdrawalStatement();
        if($page) return $page->Link();
    }
	//
	
	// Product Account
	function getProductAccountStatement(){
        $page = ProductAccountStatementPage::get()->find('ClassName', 'ProductAccountStatementPage');
        if($page) return $page;
    }
    
    function getProductAccountStatementLink() {
        $page = $this->getProductAccountStatement();
        if($page) return $page->Link();
    }
	
	function getProductTransfer(){
        $page = ProductTransferPage::get()->find('ClassName', 'ProductTransferPage');
        if($page) return $page;
    }
    
    function getProductTransferLink() {
        $page = $this->getProductTransfer();
        if($page) return $page->Link();
    }
	
    function getProductTransferStatement(){
        $page = ProductTransferStatementPage::get()->find('ClassName', 'ProductTransferStatementPage');
        if($page) return $page;
    }
    
    function getProductTransferStatementLink() {
        $page = $this->getProductTransferStatement();
        if($page) return $page->Link();
    }
	//
	
	// Upgrade Account
    function getUpgradeAccountStatement(){
        $page = UpgradeAccountStatementPage::get()->find('ClassName', 'UpgradeAccountStatementPage');
        if($page) return $page;
    }
    
    function getUpgradeAccountStatementLink() {
        $page = $this->getUpgradeAccountStatement();
        if($page) return $page->Link();
    }
    
	function getUpgradeConvertCash(){
        $page = UpgradeConvertCashPage::get()->find('ClassName', 'UpgradeConvertCashPage');
        if($page) return $page;
    }
    
    function getUpgradeConvertCashLink() {
        $page = $this->getUpgradeConvertCash();
        if($page) return $page->Link();
    }
	
    function getUpgradeConvertCashStatement(){
        $page = UpgradeConvertCashStatementPage::get()->find('ClassName', 'UpgradeConvertCashStatementPage');
        if($page) return $page;
    }
    
    function getUpgradeConvertCashStatementLink() {
        $page = $this->getUpgradeConvertCashStatement();
        if($page) return $page->Link();
    }
}
