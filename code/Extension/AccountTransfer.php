<?php

Class AccountTransfer extends DataExtension {
	public static function get_extra_config($class, $extension, $args) {
        return array(
            'db' => array(
                'Amount' => 'Currency',
                'Rate' => 'Percentage',
                'Remark' => 'Varchar(250)',
        		'Reference' => 'Varchar'
            ),
            'has_one' => array(
                'Member' => 'Member',
        		'Transfer' => 'Member'
            ),
            'many_many' => array(
                'SendAccounts' => Config::inst()->get($extension, sprintf('%s.Send', $class)),
        		'ReceiveAccounts' => Config::inst()->get($extension, sprintf('%s.Receive', $class))
            ),
            'searchable_fields' => array(
                'Created' => array(
					'field' => 'DateField',
					'filter' => 'DateMatchFilter'
				),
		        'Member.Username',
		        'Member.FirstName',
		        'Member.Surname',
		        'Transfer.Username',
		        'Transfer.FirstName',
		        'Transfer.Surname',
		        'Reference',
		        'Rate',
		        'Amount' => array(
		            'filter' => 'GreaterThanOrEqualFilter'
		        )
            ),
            'summary_fields' => array(
                'Created.Nice',
		        'Member.Username',
		        'Member.Name',
		        'Transfer.Username',
		        'Transfer.Name',
		        'Reference',
		        'Rate',
		        'Amount'
            ),
            'default_sort' => 'Created DESC, ID DESC',
            'create_table_options' => array('MySQLDatabase' => 'ENGINE=InnoDB')
        );
    }
	
	/**
     * Generate reference for account transfer
     * @return str Returns the reference
     */
    static function reference_generator($account_class) {
        return str_pad(uniqid(rand()), 25, rand(), STR_PAD_LEFT);
    }
	
	function updateFieldLabels(&$labels) {
		$labels['Created'] = _t('AccountTransfer.DATE', 'Date');
		$labels['Created.Nice'] = _t('AccountTransfer.DATE', 'Date');
		$labels['Amount'] = _t('AccountTransfer.AMOUNT', 'Amount');
		$labels['Rate'] = _t('AccountTransfer.RATE', 'Rate');
		$labels['Rate.Nice'] = _t('AccountTransfer.RATE', 'Rate');
		$labels['Reference'] = _t('AccountTransfer.REFERENCE', 'Reference');
		$labels['Transfer.Username'] = _t('AccountTransfer.TRANSFER_USERNAME', 'Transfer Username');
		$labels['Transfer'] = _t('AccountTransfer.TRANSFER_NAME', 'Transfer Name');
		$labels['Transfer.Name'] = _t('AccountTransfer.TRANSFER_NAME', 'Transfer Name');
		$labels['Transfer.FirstName'] = _t('AccountTransfer.TRANSFER_FIRSTNAME', 'Transfer First Name');
		$labels['Transfer.Surname'] = _t('AccountTransfer.TRANSFER_SURNAME', 'Transfer Surname');
		$labels['Member.Username'] = _t('AccountTransfer.USERNAME', 'Username');
		$labels['Member'] = _t('AccountTransfer.NAME', 'Name');
		$labels['Member.Name'] = _t('AccountTransfer.NAME', 'Name');
		$labels['Member.FirstName'] = _t('AccountTransfer.FIRSTNAME', 'First Name');
		$labels['Member.Surname'] = _t('AccountTransfer.SURNAME', 'Surname');
	}
	
	function validate(ValidationResult $validationResult) {
		if($this->owner->SetUsername && !$this->owner->MemberID){
			$this->owner->MemberID = Distributor::get_id_by_username($this->owner->SetUsername);
		}
		
		if($this->owner->SetTransferUsername && !$this->owner->TransferID){
			$this->owner->TransferID = Distributor::get_id_by_username($this->owner->SetTransferUsername);
		}
		
		if(!$this->owner->MemberID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('AccountTransfer.INVALID_MEMBER_ID', 'Invalid member id'), 'INVALID_MEMBER_ID');
            $validationResult->combineAnd($subvalid);
        } else if(!$this->owner->TransferID) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('AccountTransfer.INVALID_TRANSFER_ID', 'Invalid transfer member id'), 'INVALID_TRANSFER_ID');
            $validationResult->combineAnd($subvalid);
        }
        
        if($this->owner->Rate < 0) {
            $subvalid = new ValidationResult();
            $subvalid->error(_t('AccountTransfer.INVALID_CONVERT_RATE', 'Invalid convert rate'), 'INVALID_CONVERT_RATE');
            $validationResult->combineAnd($subvalid);
        }
        
        if($this->owner->Amount <= 0){
			$subvalid = new ValidationResult();
            $subvalid->error(_t('AccountTransfer.INVALID_TRANSFER_AMOUNT', "Invalid transfer amount"), "INVALID_TRANSFER");
            $validationResult->combineAnd($subvalid);
		} else {
	        if(!$this->owner->exists()) {
	        	$account_class = $this->getSendAccountClass();
				if($account_class && ClassInfo::exists($account_class)){
		            $balance = Account::get($account_class, $this->owner->MemberID)->Balance;
		            if($this->owner->Amount > $balance){
		                $subvalid = new ValidationResult();
		                $subvalid->error(_t('AccountTransfer.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => DBField::create_field('Currency', $balance)->Nice())), "INSUFFICIENT_ACCOUNT_BALANCE");
		                $validationResult->combineAnd($subvalid);
		            }
				}
	        }
		}

        return $validationResult;
    }

	function updateCMSFields(FieldList $fields) {
        if($this->owner->exists()) {
            $fields->makeFieldReadonly('Amount');
            $fields->makeFieldReadonly('Reference');
            $fields->makeFieldReadonly('MemberID');
			$fields->makeFieldReadonly('TransferID');
			$fields->removeByName('Accounts');
        } else {
            $fields->insertBefore(UsernameField::create('SetUsername', _t('AccountTransfer.USERNAME', 'Username')), 'Amount');
			$fields->insertBefore(UsernameField::create('SetTransferUsername', _t('AccountTransfer.TRANSFER_USERNAME', 'Username')), 'SetUsername');
            $fields->push(HiddenField::create('AccountClass', 'AccountClass', Config::inst()->get($this->class, $this->owner->class)));
			$fields->removeByName('Reference');
            $fields->removeByName('TransferID');
            $fields->removeByName('MemberID');
        }
    }
    
    function getCMSValidator() {
        return AccountTransfer_Validator::create();
    }

	function getTransferFormFields($memberid){
		$account_class = $this->getSendAccountClass();
        $fields = FieldList::create(
            HiddenField::create('MemberID', 'MemberID', $memberid),
            HiddenField::create('AccountBalance', 'AccountBalance', Account::get($account_class, $memberid)->Balance),
            HtmlEditorField_Readonly::create('ShowAccountBalance', _t('AccountTransfer.CURRENT_BALANCE', 'Current Balance'), Account::get($account_class, $memberid)->obj('Balance')->Nice()),
            HtmlEditorField_Readonly::create('ShowType', _t('AccountTransfer.TYPE', 'Type'), $this->getType()),
            TeamMemberField::create('Transfer', _t('AccountTransfer.TRANSFER_USERNAME', 'Transfer Username'))->setMemberID(Distributor::currentUserID())->setIncludeOwnself(false)->setShowRank(false),
            TransferAmountField::create('Amount', _t('AccountTransfer.AMOUNT', 'Amount'))->setRate($this->getDefaultRate()),
            TextField::create('Remark', _t('AccountTransfer.REMARK', 'Remark')),
			SecurityPinField::create('AccountTransferSecurityPin', _t('AccountTransfer.SECURITY_PIN', 'Security Pin'))
        );
        
        $this->owner->extend('updateTransferFormFields', $fields);
        
        return $fields;
    }
    
    function onBeforeWrite(){
        if($this->owner->SetUsername){
            $this->owner->setField('MemberID', Distributor::get_id_by_username($this->owner->SetUsername));
        }
		
		if($this->owner->SetTransferUsername){
            $this->owner->setField('TransferID', Distributor::get_id_by_username($this->owner->SetTransferUsername));
        }

		if($this->owner->Reference == '') {
            $this->owner->setField('Reference', self::reference_generator($this->owner->class));
        }

		if($this->owner->Rate == 0 || $this->owner->Rate == '') {
            $this->owner->setField('Rate', $this->getDefaultRate());
        }
    }
    
    function onAfterWrite(){
        if($this->owner->Amount > 0){
        	$send_account_class = $this->getSendAccountClass();
            if(!$count = $this->owner->SendAccounts()->filter('Debit:GreaterThan', 0)->count()){
                $data = array(
                    'Type' => 'Transfer',
                    'Debit' => $this->owner->Amount,
                    'Reference' => $this->owner->Reference,
                    'Description' => sprintf('Account balance transfer to %s', $this->owner->Transfer()->Username)
                );
                $this->owner->SendAccounts()->add($send_account_class::create_statement($data, $this->owner->MemberID));
            }
			
			$receive_account_class = $this->getReceiveAccountClass();
			if(!$count = $this->owner->ReceiveAccounts()->filter('Credit:GreaterThan', 0)->count()){
                $data = array(
                    'Type' => 'Transfer',
                    'Credit' => $this->owner->Amount * $this->owner->Rate,
                    'Reference' => $this->owner->Reference,
                    'Description' => sprintf('Account balance transfer from %s', $this->owner->Member()->Username)
                );
                $this->owner->ReceiveAccounts()->add($receive_account_class::create_statement($data, $this->owner->TransferID));
            }
        }
    }

	function getDefaultRate(){
		return Config::inst()->get($this->class, sprintf('%s.Rate', $this->owner->class));
	}

	function getType(){
		$send_account = $this->getSendAccountClass();
		$receive_account = $this->getReceiveAccountClass();
		return _t('AccountTransfer.TYPE_DESCRIPTION', 'Transfer from {send_account} to {receive_account}', '', array('send_account' => singleton($send_account)->i18n_singular_name(), 'receive_account' => singleton($receive_account)->i18n_singular_name()));
	}
	
	function getSendAccountClass(){
		return Config::inst()->get($this->class, sprintf('%s.Send', $this->owner->class));
	}
	
	function getReceiveAccountClass(){
		return Config::inst()->get($this->class, sprintf('%s.Receive', $this->owner->class));
	}
}

class AccountTransfer_Validator extends RequiredFields {

    protected $customRequired = array(
        'SetUsername',
        'Amount'
    );

    /**
     * Constructor
     */
    public function __construct() {
        $required = func_get_args();
        if(isset($required[0]) && is_array($required[0])) {
            $required = $required[0];
        }
        $required = array_merge($required, $this->customRequired);

        parent::__construct($required);
    }

    /**
     * Check if the submitted member data is valid (server-side)
     *
     * Check if a member with that email doesn't already exist, or if it does
     * that it is this member.
     *
     * @param array $data Submitted data
     * @return bool Returns TRUE if the submitted data is valid, otherwise
     *              FALSE.
     */
    function php($data) {
        $valid = parent::php($data);

        $amount = isset($data['Amount']) ? $data['Amount'] : 0;
        $memberid = isset($data['SetUsername']) ? Distributor::get_id_by_username($data['SetUsername']) : 0;
        $account_class = isset($data['AccountClass']) ? $data['AccountClass'] : '';

        // if we are in a complex table field popup, use ctf[childID], else use
        // ID
        if(isset($_REQUEST['ctf']['childID'])) {
            $id = $_REQUEST['ctf']['childID'];
        } elseif(isset($_REQUEST['ID'])) {
            $id = $_REQUEST['ID'];
        } else {
            $id = null;
        }

        if($amount <= 0) {
            $this->validationError('Amount', _t('AccountTransfer.INVALID_TRANSFER_AMOUNT', 'Invalid transfer amount'), 'required');
            $valid = false;
        } else if($amount > 0 && $memberid) {
            $balance = Account::get($account_class, $memberid)->Balance;
            if($amount > $balance) {
                $this->validationError('Amount', _t('AccountTransfer.INSUFFICIENT_BALANCE', 'Insufficient account balance ({balance})', 'Display insufficient account balance', array('balance' => Account::get($account_class, $memberid)->obj('Balance')->Nice())), 'required');
            }
            $valid = false;
        }

        return $valid;
    }

}
?>