<?php
class CheckAccountBalance extends BuildTask {
    
    protected $title = 'Check Account Balance';
    
    protected $description = 'Check Account Balance';
	
	private static $check_account = array(
		'PurchaseAccount',
		'CashAccount',
		'BonusAccount',
		'UpgradeAccount',
		'ProductAccount'
	);
    
    function init() {
        parent::init();
        $canAccess = (Director::isDev() || Director::is_cli() || Permission::check("ADMIN"));
        if(!$canAccess) return Security::permissionFailure($this);
    }
    
    public function run($request)
    {
        set_time_limit(0);
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $start = $time;
        
		DB::alteration_message('Start check account balance', 'created');
		
		$error = false;
		$content = '<table border="1" style="background-color:;border-collapse:collapse;border:1px solid #000000;color:#000000;width:100%" cellpadding="10" cellspacing="0">';
		$content .= sprintf('<tr><td><b>%s</b></td><td><b>%s</b></td><td><b>%s</b></td></tr>', 'Username', 'Account', 'Balance');
		
		foreach(Config::inst()->get('CheckAccountBalance', 'check_account') as $account){
			$result = DB::query(sprintf("SELECT a.Username, a.ID AS MemberID, SUM(ROUND(b.Credit, 2) - ROUND(b.Debit, 2)) AS Account, (SELECT ROUND(Balance, 2) FROM %sBalance WHERE MemberID = a.ID) AS Balance FROM Member a INNER JOIN %s b on a.ID = b.MemberID WHERE a.IsDistributor = 1 GROUP BY b.MemberID HAVING Account != Balance", $account, $account));
			if($result->numRecords()){
				$error = true;
				$member = array();
				$content .= sprintf('<tr><td colspan="3"><b>%s</b></td></tr>', $account);
	            foreach($result as $data){
					$member[$data['MemberID']] = $data['Account'];
	            	$content .= sprintf('<tr><td>%s</td><td>%s</td><td>%s</td></tr>', $data['Username'], $data['Account'], $data['Balance']);
	            }

				if($request->requestVar('recalc')){
					foreach($member as $memberid => $balance){
						if($balance >= 0){
							$data = array();
							DB::query(sprintf("UPDATE %sBalance SET TotalCredit = 0, TotalDebit = 0, Balance = 0 WHERE MemberID = %d", $account, $memberid));
							$result = DB::query(sprintf("SELECT * FROM %s WHERE MemberID = %d ORDER BY Date, ID", $account, $memberid));
							foreach($result as $item){
								if(isset($data[$item['MemberID']])){
									$data[$item['MemberID']]['Credit'] += round($item['Credit'], 2);
									$data[$item['MemberID']]['Debit'] += round($item['Debit'], 2);
									$data[$item['MemberID']]['Balance'] += round($item['Credit'], 2) - round($item['Debit'], 2);
								}
								else{
									$data[$item['MemberID']]['Credit'] = round($item['Credit'], 2);
									$data[$item['MemberID']]['Debit'] = round($item['Debit'], 2);
									$data[$item['MemberID']]['Balance'] = round($item['Credit'], 2) - round($item['Debit'], 2);
								}
								DB::query(sprintf("UPDATE %s SET ForwardBalance = '%s' WHERE ID = '%s'", $account, $data[$item['MemberID']]['Balance'], $item['ID']));
							}
							foreach($data as $memberid => $val){
								DB::query(sprintf("UPDATE %sBalance SET TotalCredit = '%s', TotalDebit = '%s', Balance = '%s' WHERE MemberID = '%s'", $account, $val['Credit'], $val['Debit'], $val['Balance'], $memberid));
							}
						}
					}
				}
	        }
		}
		
		$content .= '</table>';
		
		if($error && $request->requestVar('notify')){
			$email = new Email(null, 'Hut.L <hut_25@yahoo.com>', 'Check Account Balance', $content);
			$email->send();
		}
		
		echo $content;
        
        $time = microtime();
        $time = explode(' ', $time);
        $time = $time[1] + $time[0];
        $finish = $time;
        $total_time = round(($finish - $start), 4);
        DB::alteration_message('Process Time - ' . $total_time . ' seconds', 'created');
    }
}

?>