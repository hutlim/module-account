<?php

class VoucherAdmin extends GeneralModelAdmin {
    private static $managed_models = array(
    	'Voucher'
    );

    private static $url_segment = 'voucher';
    private static $menu_title = 'Voucher';
    private static $menu_icon = 'account/images/voucher-icon.png';
	
	public $showImportForm = false;
    
    public function init() {
        parent::init();
    }
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
		if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        $listField = GridField::create(
            $this->sanitiseClassName($this->modelClass),
            false,
            $list,
            $fieldConfig = GridFieldConfig_RecordViewer::create($this->stat('page_length'))
                ->removeComponentsByType('GridFieldFilterHeader')
                ->addComponents(new VoucherGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
        );
		
		if(Permission::check(sprintf('CANCEL_%s', $this->modelClass))){
			$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
			$bulkButton->addBulkAction('cancel', _t('VoucherAdmin.CANCEL', 'Cancel'), 'GridFieldBulkVoucherHandler', array('icon' => 'cross-circle'));
		}

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
