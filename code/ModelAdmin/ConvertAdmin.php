<?php

class ConvertAdmin extends GeneralModelAdmin {
    private static $managed_models = array(
        'BonusConvertCash',
        'BonusConvertPurchase',
        'BonusConvertUpgrade',
        'BonusConvertProduct',
        'CashConvertPurchase',
        'CashConvertUpgrade'
    );

    private static $url_segment = 'balance-convert';
    private static $menu_title = 'Balance Convert';
    private static $menu_icon = 'account/images/convert-icon.png';
    
    public function init() {
        parent::init();
    }
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
		if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        $listField = GridField::create(
            $this->sanitiseClassName($this->modelClass),
            false,
            $list,
            $fieldConfig = GridFieldConfig_Base::create($this->stat('page_length'))
                ->removeComponentsByType('GridFieldFilterHeader')
                ->addComponents(new ConvertGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
        );
		
		if(Permission::check('ALLOW_Revert')){
			$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
			$bulkButton->addBulkAction('revert', _t('ConvertAdmin.REVERT', 'Revert'), 'GridFieldBulkConvertHandler', array('icon' => 'arrow-circle-135-left'));
		}

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
