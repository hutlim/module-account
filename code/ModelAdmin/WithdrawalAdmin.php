<?php

class WithdrawalAdmin extends GeneralModelAdmin {
    private static $managed_models = array(
        'CashAccountWithdrawal',
        'BonusAccountWithdrawal'
    );

    private static $url_segment = 'withdrawal';
    private static $menu_title = 'Withdrawal';
    private static $menu_icon = 'account/images/withdrawal-icon.png';
    
    public function init() {
        parent::init();
    }
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        $listField = GridField::create(
            $this->sanitiseClassName($this->modelClass),
            false,
            $list,
            $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                ->removeComponentsByType('GridFieldFilterHeader')
                ->removeComponentsByType('GridFieldDetailForm')
                ->removeComponentsByType('GridFieldAddNewButton')
                ->removeComponentsByType('GridFieldDeleteAction')
                ->addComponents(new WithdrawalGridFieldDetailForm(), new WithdrawalGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
        );
		
		if(Permission::check(sprintf('APPROVE_%s', $this->modelClass)) || Permission::check(sprintf('REJECT_%s', $this->modelClass))){
			$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
			if(Permission::check(sprintf('APPROVE_%s', $this->modelClass))){
				$bulkButton->addBulkAction('approve', _t('WithdrawalAdmin.APPROVE', 'Approve'), 'GridFieldBulkWithdrawalHandler');
			}
			
			if(Permission::check(sprintf('REJECT_%s', $this->modelClass))){
				$bulkButton->addBulkAction('reject', _t('WithdrawalAdmin.REJECT', 'Reject'), 'GridFieldBulkWithdrawalHandler', array('icon' => 'cross-circle'));
			}
		}

        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
            $listField->getConfig()->getComponentByType('WithdrawalGridFieldDetailForm')->setValidator($detailValidator);
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
