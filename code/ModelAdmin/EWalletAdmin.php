<?php

class EWalletAdmin extends GeneralModelAdmin {
    private static $managed_models = array(
    	'PurchaseAccountAdjustment',
        'CashAccountAdjustment', 
        'BonusAccountAdjustment',
        'UpgradeAccountAdjustment',
        'ProductAccountAdjustment',
        'DepositSubmission'
    );

    private static $url_segment = 'ewallet';
    private static $menu_title = 'EWallet';
    private static $menu_icon = 'account/images/ewallet-icon.png';
	
	public $showImportForm = false;
    
    public function init() {
        parent::init();
    }
    
    public function getEditForm($id = null, $fields = null) {
        $list = $this->getList();
        if(ClassInfo::exists('GridFieldExportToExcelButton')){
        	$exportButton = new GridFieldExportToExcelButton('buttons-after-left');
		}
		else{
			$exportButton = new GridFieldExportButton('buttons-after-left');
		}
        $exportButton->setExportColumns($this->getExportFields());
        
        if(preg_match("/deposit/i", $this->modelClass)){
            Requirements::javascript('account/javascript/DepositAction.js');
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldFilterHeader')
                    ->removeComponentsByType('GridFieldDetailForm')
                    ->removeComponentsByType('GridFieldAddNewButton')
                    ->removeComponentsByType('GridFieldDeleteAction')
                    ->addComponents(new DepositGridFieldDetailForm(), new DepositGridFieldDataColumns(), new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
			
			if(Permission::check('APPROVE_DepositSubmission')){
				$fieldConfig->addComponent($bulkButton = new GridFieldBulkAction(), 'GridFieldSortableHeader');
				$bulkButton->addBulkAction('approve', _t('EWalletAdmin.APPROVE', 'Approve'), 'GridFieldBulkDepositHandler');
			}
        }
		else if(preg_match("/adjustment/i", $this->modelClass)){
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldFilterHeader')
					->removeComponentsByType('GridFieldEditButton')
                    ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
        }
        else{
            $listField = GridField::create(
                $this->sanitiseClassName($this->modelClass),
                false,
                $list,
                $fieldConfig = GridFieldConfig_RecordEditor::create($this->stat('page_length'))
                    ->removeComponentsByType('GridFieldFilterHeader')
                    ->addComponents(new GridFieldButtonRow('after'), new GridFieldPrintButton('buttons-after-left'), $exportButton)
            );
        }

        // Validation
        if(singleton($this->modelClass)->hasMethod('getCMSValidator')) {
            $detailValidator = singleton($this->modelClass)->getCMSValidator();
			if(preg_match("/deposit/i", $this->modelClass)){
            	$listField->getConfig()->getComponentByType('DepositGridFieldDetailForm')->setValidator($detailValidator);
			}
			else{
				$listField->getConfig()->getComponentByType('GridFieldDetailForm')->setValidator($detailValidator);
			}
        }

        $form = CMSForm::create( 
            $this,
            'EditForm',
            new FieldList($listField),
            new FieldList()
        )->setHTMLID('Form_EditForm');
		$form->setResponseNegotiator($this->getResponseNegotiator());
        $form->addExtraClass('cms-edit-form cms-panel-padded center');
        $form->setTemplate($this->getTemplatesWithSuffix('_EditForm'));
        $editFormAction = Controller::join_links($this->Link($this->sanitiseClassName($this->modelClass)), 'EditForm');
        $form->setFormAction($editFormAction);
        $form->setAttribute('data-pjax-fragment', 'CurrentForm');

        $this->extend('updateEditForm', $form);
        
        return $form;
    }
}
