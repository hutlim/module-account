(function($) {
	$('select[rel~=payoutset]').each(function(index) {
		var payoutsetfield = $(this), payoutfields = payoutsetfield.parents('form').find('.composite.payoutfields');
		var payout = payoutsetfield.val();
		payoutfields.filter(function() {
		   return !$(this).hasClass(payout) || payout == '';
		}).addClass('hidden');
		payoutfields.filter(function() {
		   return $(this).hasClass(payout);
		}).removeClass('hidden');
	});
   
	$('select[rel~=payoutset]').live('change', function() {
		var payoutsetfield = $(this), payoutset = payoutsetfield.parents('form').find('.field.payoutset'), payoutfields = payoutset.parents('form').find('.composite.payoutfields');
		payoutset.find('span.message').remove();
		payoutfields.find('span.message').remove();
		var payout = this.value;
		payoutfields.filter(function() {
		   return !$(this).hasClass(payout) || payout == '';
		}).addClass('hidden');
		payoutfields.filter(function() {
		   return $(this).hasClass(payout);
		}).removeClass('hidden');
	});
}(jQuery));