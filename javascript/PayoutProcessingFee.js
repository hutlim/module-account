(function($) {
	var widthdrawal_field = $(':text[rel~=withdrawalamount], select[rel~=withdrawalamount]'), amount = widthdrawal_field.val(), payoutfields = widthdrawal_field.parents('form').find('.composite.payoutfields');
	payoutfields.each(function(index) {
		var processing_fee = $(this).find('input[rel~=processing_fee]').val();
		var processing_fee_percentage = $(this).find('input[rel~=processing_fee_percentage]').val();
		if(processing_fee_percentage > 0 && amount * processing_fee_percentage > processing_fee){
			processing_fee = amount * processing_fee_percentage;
		}
		
		var show_processing_fee = $(this).find('input[rel~=show_processing_fee]');
		show_processing_fee.val(Inputmask.format(processing_fee, {alias: 'decimal', digits: 2, digitsOptional: false, placeholder: '0', 'prefix': show_processing_fee.data('prefix')}));
	});
	
	widthdrawal_field.live("change", function(e) {
		e.preventDefault();
		var amount = $(this).val();
		payoutfields.each(function(index) {
			var processing_fee = $(this).find('input[rel~=processing_fee]').val();
			var processing_fee_percentage = $(this).find('input[rel~=processing_fee_percentage]').val();
			if(processing_fee_percentage > 0 && amount * processing_fee_percentage > processing_fee){
				processing_fee = amount * processing_fee_percentage;
			}
			
			var show_processing_fee = $(this).find('input[rel~=show_processing_fee]');
			show_processing_fee.val(Inputmask.format(processing_fee, {alias: 'decimal', digits: 2, digitsOptional: false, placeholder: '0', 'prefix': show_processing_fee.data('prefix')}));
		});
	});
}(jQuery));