(function($) {
	$(':text[rel~=convert-amount], select[rel~=convert-amount]').live("change", function() {
		var field = $(this);
		var convert_container = field.parents('form');
		var rate = convert_container.find(':text[rel~=convert-rate]').val();
		if(field.val() == ''){
			convert_container.find(':text[rel~=convert-converted]').val('');
			return false;
		}
		
		convert_container.find(':text[rel~=convert-converted]').val(Inputmask.format(field.val() * rate, {alias: 'decimal', digits: 2, digitsOptional: false, placeholder: '0'}));
	});
})(jQuery);
