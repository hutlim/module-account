(function($) {
	$('select[rel~=purchase-percentage]').live("change", function() {
		var field = $(this);
		var payment_container = field.parents('div.paymentfields');
		var amount_field = payment_container.find('input:hidden[rel~=purchase-amount]');
		var payment_field = payment_container.find('input:hidden[rel~=payment-amount]');
		if(field.val() == ''){
			amount_field.val(0);
			return false;
		}

		amount_field.val(field.val().replace('_', '.') * payment_field.val());
	});

	$('select[rel~=cash-percentage]').live("change", function() {
		var field = $(this);
		var payment_container = field.parents('div.paymentfields');
		var amount_field = payment_container.find('input:hidden[rel~=cash-amount]');
		var payment_field = payment_container.find('input:hidden[rel~=payment-amount]');
		if(field.val() == ''){
			amount_field.val(0);
			return false;
		}

		amount_field.val(field.val().replace('_', '.') * payment_field.val());
	});

	$('select[rel~=bonus-percentage]').live("change", function() {
		var field = $(this);
		var payment_container = field.parents('div.paymentfields');
		var amount_field = payment_container.find('input:hidden[rel~=bonus-amount]');
		var payment_field = payment_container.find('input:hidden[rel~=payment-amount]');
		if(field.val() == ''){
			amount_field.val(0);
			return false;
		}

		amount_field.val(field.val().replace('_', '.') * payment_field.val());
	});

	$('select[rel~=upgrade-percentage]').live("change", function() {
		var field = $(this);
		var payment_container = field.parents('div.paymentfields');
		var amount_field = payment_container.find('input:hidden[rel~=upgrade-amount]');
		var payment_field = payment_container.find('input:hidden[rel~=payment-amount]');
		if(field.val() == ''){
			amount_field.val(0);
			return false;
		}

		amount_field.val(field.val().replace('_', '.') * payment_field.val());
	});

	$('select[rel~=product-percentage]').live("change", function() {
		var field = $(this);
		var payment_container = field.parents('div.paymentfields');
		var amount_field = payment_container.find('input:hidden[rel~=product-amount]');
		var payment_field = payment_container.find('input:hidden[rel~=payment-amount]');
		if(field.val() == ''){
			amount_field.val(0);
			return false;
		}

		amount_field.val(field.val().replace('_', '.') * payment_field.val());
	});
})(jQuery);
