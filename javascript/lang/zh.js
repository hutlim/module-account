if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'WithdrawalAction.APPROVE_CONFIRMATION' : '你确定要批准此余额兑现？',
		'WithdrawalAction.REJECT_CONFIRMATION' : '你确定要拒绝此余额兑现？',
		'WithdrawalAction.REMARK' : '备注',
		'WithdrawalAction.REMARK_REQUIRED' : '备注为必填',
		'WithdrawalAction.APPROVE_WITHDRAWAL' : '批准此余额兑现',
		'WithdrawalAction.REJECT_WITHDRAWAL' : '拒绝此余额兑现',
		'WithdrawalAction.APPROVE_SUCCESS' : '余额兑现已批准成功',
		'WithdrawalAction.REJECT_SUCCESS' : '余额兑现已拒绝成功',
		'DepositAction.APPROVE_CONFIRMATION' : '你确定要批准此余额充值？',
		'DepositAction.REMARK' : '备注',
		'DepositAction.APPROVE_DEPOSIT' : '批准此余额充值',
		'DepositAction.APPROVE_SUCCESS' : '余额充值已批准成功'
	});
}