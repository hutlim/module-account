if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'WithdrawalAction.APPROVE_CONFIRMATION' : 'Are you sure you want to approve this withdrawal request?',
		'WithdrawalAction.REJECT_CONFIRMATION' : 'Are you sure you want to reject this withdrawal request?',
		'WithdrawalAction.REMARK' : 'Remark',
		'WithdrawalAction.REMARK_REQUIRED' : 'Remark is required',
		'WithdrawalAction.APPROVE_WITHDRAWAL' : 'Approve Withdrawal',
		'WithdrawalAction.REJECT_WITHDRAWAL' : 'Reject Withdrawal',
		'DepositAction.APPROVE_CONFIRMATION' : 'Are you sure you want to approve this deposit request?',
		'WithdrawalAction.APPROVE_SUCCESS' : 'Withdrawal request has been approved successfully',
		'WithdrawalAction.REJECT_SUCCESS' : 'Withdrawal request has been rejected successfully',
		'DepositAction.REMARK' : 'Remark',
		'DepositAction.APPROVE_DEPOSIT' : 'Approve Deposit',
		'DepositAction.APPROVE_SUCCESS' : 'Deposit request has been approved successfully'
	});
}