(function($){
	/*"use strict";*/
	$.entwine('ss', function($) {
		$('.ss-gridfield .col-buttons .action.gridfield-button-approve-deposit').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				if (confirm(ss.i18n._t('DepositAction.APPROVE_CONFIRMATION', 'Are you sure you want to approve this deposit request?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Remark">' + ss.i18n._t('DepositAction.REMARK', 'Remark') + '</label><input id="Remark" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('DepositAction.APPROVE_DEPOSIT', 'Approve Deposit')] = function() {
		        		var remark = $('#modal').find('#Remark').val();

		          		$(this).dialog("close");

						// If the button is disabled, do nothing.
						if (self.button('option', 'disabled')) {
							e.preventDefault();
							return;
						}
						
						self.getGridField().reload(
							{data: [{name: self.attr('name'), value: self.val()}, {name: 'Remark', value: remark}]},
							function(){
								statusMessage(ss.i18n._t('DepositAction.APPROVE_SUCCESS', 'Deposit request has been approved successfully'), 'good');
							}
						);

						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
		
		$('#action_doApproveDeposit').entwine({
			onclick: function(e) {
				var self = this;
				$('#modal').remove();
				$('#Remark').remove();
				if (confirm(ss.i18n._t('DepositAction.APPROVE_CONFIRMATION', 'Are you sure you want to approve this deposit request?'))) {
					var content = '<div id="modal"><div class="field text"><label class="left" for="Remark">' + ss.i18n._t('DepositAction.REMARK', 'Remark') + '</label><input id="Remark" class="text" type="text" /></div></div>';
					var actions = {};
					actions[ss.i18n._t('DepositAction.APPROVE_DEPOSIT', 'Approve Deposit')] = function() {
		        		var remark = $('#modal').find('#Remark').val();
		        		self.parents('form').append('<input type="hidden" id="Remark" name="Remark" value="' + remark + '" />');
		          		$(this).dialog("close");
		          		
		          		if(!self.is(':disabled')) {
							self.parents('form').trigger('submit', [self]);
						}
						e.preventDefault();
		        	};
					$('body').append(content);
					$('#modal').dialog({
						resizable : false,
						modal : true,
						autoOpen : true,
						width: 500,
				      	buttons: actions
					});
				} else {
					e.preventDefault();
					return false;
				}
			}
		});
	});

}(jQuery));