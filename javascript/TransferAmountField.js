(function($) {
	$(':text[rel~=transfer-amount], select[rel~=transfer-amount]').live("change", function() {
		var field = $(this);
		var transfer_container = field.parents('form');
		var rate = transfer_container.find(':text[rel~=transfer-rate]').val();
		if(field.val() == ''){
			transfer_container.find(':text[rel~=transfer-converted]').val('');
			return false;
		}
		
		transfer_container.find(':text[rel~=transfer-converted]').val(Inputmask.format(field.val() * rate, {alias: 'decimal', digits: 2, digitsOptional: false, placeholder: '0'}));
	});
})(jQuery);
